
    create table AES_EDIT_SPEC (
        ID int8 not null,
        CASE_NO varchar(255) not null,
        STATE int4 not null,
        EDIT_SPEC_SCHEMA_ID int8 not null,
        primary key (ID)
    );

    create table AES_EDIT_SPEC_ITEM (
        ID int8 not null,
        ERROR varchar(255),
        QUESTION varchar(255) not null,
        RESPONSE varchar(255) not null,
        EDIT_SPEC_ID int8 not null,
        primary key (ID)
    );

    create table AES_EDIT_SPEC_SCHM (
        ID int8 not null,
        CASE_NO varchar(255) not null,
        primary key (ID)
    );

    create table AES_EDIT_SPEC_SCHM_ITEM (
        ID int8 not null,
        QUESTION varchar(255) not null,
        RULES varchar(255) not null,
        EDIT_SPEC_SCHEMA_ID int8 not null,
        primary key (ID)
    );

    create table AES_QSTN (
        ID int8 not null,
        FIELD_NO varchar(255) not null,
        STATEMENT varchar(255) not null,
        primary key (ID)
    );

    create table CNG_ACTR (
        ID int8 not null,
        ACTOR_TYPE int4 not null,
        ADDRESS1 varchar(255),
        ADDRESS2 varchar(255),
        ADDRESS3 varchar(255),
        CODE varchar(255) not null,
        EMAIL varchar(255) not null,
        FAX varchar(255),
        IDENTITY_NO varchar(255) not null,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        MOBILE varchar(255),
        NAME varchar(255) not null,
        PHONE varchar(255),
        POSTCODE varchar(255),
        primary key (ID)
    );

    create table CNG_CNFG (
        ID int8 not null,
        DESCRIPTION varchar(255),
        CONFIG_KEY varchar(255) not null,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        CONFIG_VALUE varchar(255),
        CONFIG_VALUE_BYTEA bytea,
        CONFIG_VALUE_DOUBLE float8,
        CONFIG_VALUE_LONG int8,
        primary key (ID)
    );

    create table CNG_GROP (
        ID int8 not null,
        primary key (ID)
    );

    create table CNG_GROP_MMBR (
        ID int8 not null,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        GROUP_ID int8,
        PRINCIPAL_ID int8,
        primary key (GROUP_ID, PRINCIPAL_ID)
    );

    create table CNG_MODL (
        ID int8 not null,
        CANONICAL_CODE varchar(255) not null,
        CODE varchar(255) not null,
        DESCRIPTION varchar(255),
        ENABLED boolean,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        ORDINAL int4 not null,
        primary key (ID)
    );

    create table CNG_PCPL (
        ID int8 not null,
        ENABLED boolean not null,
        LOCKED boolean not null,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        NAME varchar(255) not null,
        PRINCIPAL_TYPE int4,
        primary key (ID)
    );

    create table CNG_PCPL_ROLE (
        ID int8 not null,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        ROLE_TYPE int4,
        PRINCIPAL_ID int8,
        primary key (ID)
    );

    create table CNG_RFRN_NO (
        ID int8 not null,
        CODE varchar(255) not null,
        CURRENT_VALUE int4,
        DESCRIPTION varchar(255) not null,
        INCREMENT_VALUE int4,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        PREFIX varchar(255),
        REFERENCE_FORMAT varchar(255),
        SEQUENCE_FORMAT varchar(255),
        primary key (ID)
    );

    create table CNG_SMDL (
        ID int8 not null,
        CODE varchar(255),
        DESCRIPTION varchar(255),
        ENABLED boolean,
        C_TS timestamp,
        C_ID int8,
        D_TS timestamp,
        D_ID int8,
        M_TS timestamp,
        M_ID int8,
        M_ST int4,
        ORDINAL int4,
        MODULE_ID int8,
        primary key (ID)
    );

    create table CNG_STAF (
        ID int8 not null,
        primary key (ID)
    );

    create table CNG_USER (
        EMAIL varchar(255) not null,
        PASSWORD varchar(255),
        REAL_NAME varchar(255) not null,
        ID int8 not null,
        ACTOR_ID int8,
        primary key (ID)
    );

    create table NEWSS_USR (
        USER_ID varchar(255) not null,
        PASSWORD varchar(255),
        USER_NAME varchar(255),
        primary key (USER_ID)
    );

    alter table AES_EDIT_SPEC 
        add constraint FKC82EA1402A86F9C7 
        foreign key (EDIT_SPEC_SCHEMA_ID) 
        references AES_EDIT_SPEC_SCHM;

    alter table AES_EDIT_SPEC_ITEM 
        add constraint FK536152B261E7B8A6 
        foreign key (EDIT_SPEC_ID) 
        references AES_EDIT_SPEC;

    alter table AES_EDIT_SPEC_SCHM_ITEM 
        add constraint FK4728C57E2A86F9C7 
        foreign key (EDIT_SPEC_SCHEMA_ID) 
        references AES_EDIT_SPEC_SCHM;

    alter table AES_QSTN 
        add constraint uc_AES_QSTN_1 unique (FIELD_NO);

    alter table AES_QSTN 
        add constraint uc_AES_QSTN_2 unique (STATEMENT);

    alter table CNG_ACTR 
        add constraint uc_CNG_ACTR_1 unique (CODE);

    alter table CNG_GROP 
        add constraint FKCC991D2FDD05C3EA 
        foreign key (ID) 
        references CNG_PCPL;

    alter table CNG_GROP_MMBR 
        add constraint FKFD23AB008195FBFB 
        foreign key (GROUP_ID) 
        references CNG_GROP;

    alter table CNG_GROP_MMBR 
        add constraint FKFD23AB001347A19B 
        foreign key (PRINCIPAL_ID) 
        references CNG_PCPL;

    alter table CNG_MODL 
        add constraint uc_CNG_MODL_1 unique (CANONICAL_CODE);

    alter table CNG_MODL 
        add constraint uc_CNG_MODL_2 unique (CODE);

    alter table CNG_PCPL 
        add constraint uc_CNG_PCPL_1 unique (NAME);

    alter table CNG_PCPL_ROLE 
        add constraint FK4B6F18431347A19B 
        foreign key (PRINCIPAL_ID) 
        references CNG_PCPL;

    alter table CNG_RFRN_NO 
        add constraint uc_CNG_RFRN_NO_1 unique (CODE);

    alter table CNG_SMDL 
        add constraint FKCC9E7D85F184E728 
        foreign key (MODULE_ID) 
        references CNG_MODL;

    alter table CNG_STAF 
        add constraint FKCC9E9769D1F99AB1 
        foreign key (ID) 
        references CNG_ACTR;

    alter table CNG_USER 
        add constraint uc_CNG_USER_1 unique (EMAIL);

    alter table CNG_USER 
        add constraint FKCC9F7CEE6F98153B 
        foreign key (ACTOR_ID) 
        references CNG_ACTR;

    alter table CNG_USER 
        add constraint FKCC9F7CEEDD05C3EA 
        foreign key (ID) 
        references CNG_PCPL;

    create sequence SQ_AES_EDIT_SPEC;

    create sequence SQ_AES_EDIT_SPEC_ITEM;

    create sequence SQ_AES_EDIT_SPEC_SCHM;

    create sequence SQ_AES_EDIT_SPEC_SCHM_ITEM;

    create sequence SQ_AES_QSTN;

    create sequence SQ_CNG_ACTR;

    create sequence SQ_CNG_CNFG;

    create sequence SQ_CNG_GROP_MMBR;

    create sequence SQ_CNG_MODL;

    create sequence SQ_CNG_PCPL;

    create sequence SQ_CNG_PCPL_ROLE;

    create sequence SQ_CNG_RFRN_NO;

    create sequence SQ_CNG_SMDL;
