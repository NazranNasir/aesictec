import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LoginService} from '../services/login/login.service';
import {Router} from '@angular/router';
import {LoginAction} from "../services/auth/auth.reducer";
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  users: any;

  constructor(private loginService: LoginService,
              private store: Store<any>,
              private router: Router) {
  }

  ngOnInit() {
    // this.loginService.get().subscribe(data=> {
    //   this.users = data;
    //   console.log(this.users);
    // });
  }

  onSubmit(loginForm: NgForm) {

    console.log(1);
    this.store.dispatch(new LoginAction(loginForm.value.realName, loginForm.value.password));
    // console.log(loginForm.valid);
      if(loginForm.valid)
        this.router.navigate(['/aes']);
    
  }

}
