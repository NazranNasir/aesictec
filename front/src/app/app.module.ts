import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Type} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {CovalentHttpModule} from '@covalent/http';

import {LoginService} from './services/login/login.service';
import {StoreModule} from "@ngrx/store";
import {AuthModule} from "./services/auth/auth.module";
import {AuthenticationService} from "./services/auth/authentization.service";
import {AuthorizationService} from "./services/auth/authorization.service";
import {LocalStorageService} from "./services/auth/local-storage.service";
import {TOKEN_NAME} from "./services/auth/auth.constant";
import {AuthConfig, AuthHttp} from "angular2-jwt";
import {Http} from "@angular/http";
import {RequestInterceptor} from "./services/auth/request.interceptor";
import {EffectsModule} from "@ngrx/effects";


const httpInterceptorProviders: Type<any>[] = [
  RequestInterceptor,
];

export function authHttpServiceFactory(http: Http) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: 'Bearer',
    tokenName: TOKEN_NAME,
    globalHeaders: [{'Content-Type': 'application/json'}],
    noJwtError: false,
    noTokenScheme: true,
    tokenGetter: (() => localStorage.getItem(TOKEN_NAME))
  }), http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AuthModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    HttpClientModule,
    CovalentHttpModule.forRoot({
      interceptors: [{
        interceptor: RequestInterceptor, paths: ['**'],
      }],
    }),

  ],
  providers: [
    {provide: AuthHttp, useFactory: authHttpServiceFactory, deps: [Http]},
    LoginService,
    AuthenticationService,
    AuthorizationService,
    LocalStorageService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
