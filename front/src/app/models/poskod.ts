export class Poskod {
   id: number;
   location: string;
   postcode: string;
   post_Office: string;
   state: string;
   state_code: string;
}
