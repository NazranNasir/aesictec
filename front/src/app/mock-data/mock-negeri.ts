import { Negeri } from './../models/negeri';

export const NEGERI: Negeri[] = [
    { id: 1, code_no: '01', name: 'Johor' },
    { id: 2, code_no: '02', name: 'Kedah' },
    { id: 3, code_no: '03', name: 'Kelantan' },
    { id: 4, code_no: '04', name: 'Melaka' },
    { id: 5, code_no: '05', name: 'Negeri Sembilan' },
    { id: 6, code_no: '06', name: 'Pahang' },
    { id: 7, code_no: '07', name: 'Pulau Pinang' },
    { id: 8, code_no: '08', name: 'Perak' },
    { id: 9, code_no: '09', name: 'Perlis' },
    { id: 10, code_no: '10', name: 'Selangor' },
    { id: 11, code_no: '11', name: 'Terengganu' },
    { id: 12, code_no: '12', name: 'Sabah' },
    { id: 13, code_no: '13', name: 'Sarawak' },
    { id: 14, code_no: '14', name: 'Wilayah Persekutuan (Kuala Lumpur)' },
    { id: 15, code_no: '15', name: 'Wilayah Persekutuan (Labuan)' },
    { id: 16, code_no: '16', name: 'Wilayah Persekutuan (Putrajaya)' }
];