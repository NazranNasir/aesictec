import { NoSiri } from './../models/no-siri';

export const NOSIRI: NoSiri[] = [
    { id: 1, code_no: 101101000000 },
    { id: 2, code_no: 102101000000 },
    { id: 3, code_no: 103101000000 },
    { id: 4, code_no: 104101000000 },
    { id: 5, code_no: 105101000000 },
    { id: 6, code_no: 106101000000 },
    { id: 7, code_no: 107101000000 },
    { id: 8, code_no: 108101000000 },
    { id: 9, code_no: 109101000000 },
    { id: 10, code_no: 110101000000 }
];