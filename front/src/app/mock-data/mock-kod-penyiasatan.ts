import { KodPenyiasatan } from './../models/kod-penyiasatan';

export const KODPENYIASATAN: KodPenyiasatan[] = [
    { id: 1, code_no: 411, name: 'ICTeC' },
    { id: 3, code_no: 310, name: 'AKTIVITI WAYANG GAMBAR, VIDEO & PROGRAM TELEVISYEN, RAKAMAN BUNYI DAN AKTIVITI PENYIARAN' },
    { id: 4, code_no: 319, name: 'PERKHIDMATAN TELEKOMUNIKASI' },
    { id: 5, code_no: 320, name: 'PERKHIDMATAN KOMPUTER DAN MAKLUMAT' },
    { id: 6, code_no: 330, name: 'AKTIVITI PENERBITAN' },
    { id: 7, code_no: 100, name: 'PERTANIAN' },
    { id: 8, code_no: 205, name: 'PEMBUATAN' },
    { id: 9, code_no: 311, name: 'PENTADBIRAN & KHIDMAT SOKONGAN' },
    { id: 10, code_no: 332, name: 'PERKHIDMATAN LAIN' },
    { id: 11, code_no: null, name: 'TEST BANK SOALAN' },
];
