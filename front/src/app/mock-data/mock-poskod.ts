import { Poskod } from './../models/poskod';

export const POSKOD: Poskod[] =
[
  { id: 1,
    location: "Pusat Pentadbiran Kerajaan Negeri Johor",
    postcode: "79000",
    post_Office: "Nusajaya",
    state: "Johor",
    state_code: "01"
  },

  { id: 2,
    location: "Kampung Melele",
    postcode: "06100",
    post_Office: "Kodiang",
    state: "Kedah",
    state_code: "02"
  },

  { id: 3,
    location: "Pondok Haji Ahmad",
    postcode: "16080",
    post_Office: "Tumpat",
    state: "Kelantan",
    state_code: "03"
  },

  { id: 4,
    location: "Pangsapuri Bukit Beruang",
    postcode: "75450",
    post_Office: "Ayer Keroh",
    state: "Melaka",
    state_code: "04"
  },

  { id: 5,
    location: "Senawang",
    postcode: "70450",
    post_Office: "Seremban",
    state: "Negeri Sembilan",
    state_code: "05"
  },

  { id: 6,
    location: "Medan Gopeng",
    postcode: "31350",
    post_Office: "Ipoh",
    state: "Perak",
    state_code: "08"
  },








];
