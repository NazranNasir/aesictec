import { PejabatOperasi } from './../models/pejabat-operasi';

export const PEJABATOPERASI: PejabatOperasi[] = [
    { id: 1, code_no: 101101 },
    { id: 2, code_no: 102101 },
    { id: 3, code_no: 103101 },
    { id: 4, code_no: 104101 },
    { id: 5, code_no: 105101 },
    { id: 6, code_no: 106101 },
    { id: 7, code_no: 107101 },
    { id: 8, code_no: 108101 },
    { id: 9, code_no: 109101 },
    { id: 10, code_no: 110101 }
];