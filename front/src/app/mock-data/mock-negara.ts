import { Negara } from './../models/negara';

export const NEGARA: Negara[] = 
[
	{
		id: 1,
		name: "AFGHANISTAN",
		code_no: "004"
	},
	{
		id: 2,
		name: "ALBANIA",
		code_no: "008"
	},
	{
		id: 3,
		name: "ANTARCTICA",
		code_no: "010"
	},
	{
		id: 4,
		name: "ALGERIA",
		code_no: "012"
	},
	{
		id: 5,
		name: "AMERICAN SAMOA",
		code_no: "016"
	},
	{
		id: 6,
		name: "ANDORRA",
		code_no: "020"
	},
	{
		id: 7,
		name: "ANGOLA",
		code_no: "024"
	},
	{
		id: 8,
		name: "ANTIGUA AND BARBUDA",
		code_no: "028"
	},
	{
		id: 9,
		name: "AZERBAIJAN",
		code_no: "031"
	},
	{
		id: 10,
		name: "ARGENTINA",
		code_no: "032"
	},
	{
		id: 11,
		name: "AUSTRALIA",
		code_no: "036"
	},
	{
		id: 12,
		name: "AUSTRIA",
		code_no: "040"
	},
	{
		id: 13,
		name: "BAHAMAS",
		code_no: "044"
	},
	{
		id: 14,
		name: "BAHRAIN",
		code_no: "048"
	},
	{
		id: 15,
		name: "BANGLADESH",
		code_no: "050"
	},
	{
		id: 16,
		name: "ARMENIA",
		code_no: "051"
	},
	{
		id: 17,
		name: "BARBADOS",
		code_no: "052"
	},
	{
		id: 18,
		name: "BELGIUM",
		code_no: "056"
	},
	{
		id: 19,
		name: "BERMUDA",
		code_no: "060"
	},
	{
		id: 20,
		name: "BHUTAN",
		code_no: "064"
	},
	{
		id: 21,
		name: "BOLIVIA",
		code_no: "068"
	},
	{
		id: 22,
		name: "BOSNIA AND HERZEGOWINA",
		code_no: "070"
	},
	{
		id: 23,
		name: "BOTSWANA",
		code_no: "072"
	},
	{
		id: 24,
		name: "BOUVET ISLAND",
		code_no: "074"
	},
	{
		id: 25,
		name: "BRAZIL",
		code_no: "076"
	},
	{
		id: 26,
		name: "BELIZE",
		code_no: "084"
	},
	{
		id: 27,
		name: "BRITISH INDIAN OCEAN TERRITORY",
		code_no: "086"
	},
	{
		id: 28,
		name: "SOLOMON ISLANDS",
		code_no: "090"
	},
	{
		id: 29,
		name: "VIRGIN ISLANDS (BRITISH)",
		code_no: "092"
	},
	{
		id: 30,
		name: "BRUNEI DARUSSALAM",
		code_no: "096"
	},
	{
		id: 31,
		name: "BULGARIA",
		code_no: "100"
	},
	{
		id: 32,
		name: "MYANMAR",
		code_no: "104"
	},
	{
		id: 33,
		name: "BURUNDI",
		code_no: "108"
	},
	{
		id: 34,
		name: "BELARUS",
		code_no: "112"
	},
	{
		id: 35,
		name: "CAMBODIA",
		code_no: "116"
	},
	{
		id: 36,
		name: "CAMEROON",
		code_no: "120"
	},
	{
		id: 37,
		name: "CANADA",
		code_no: "124"
	},
	{
		id: 38,
		name: "CAPE VERDE",
		code_no: "132"
	},
	{
		id: 39,
		name: "CAYMAN ISLANDS",
		code_no: "136"
	},
	{
		id: 40,
		name: "CENTRAL AFRICAN REPUBLIC",
		code_no: "140"
	},
	{
		id: 41,
		name: "SRI LANKA",
		code_no: "144"
	},
	{
		id: 42,
		name: "CHAD",
		code_no: "148"
	},
	{
		id: 43,
		name: "CHILE",
		code_no: "152"
	},
	{
		id: 44,
		name: "CHINA",
		code_no: "156"
	},
	{
		id: 45,
		name: "TAIWAN",
		code_no: "158"
	},
	{
		id: 46,
		name: "CHRISTMAS ISLAND",
		code_no: "162"
	},
	{
		id: 47,
		name: "COCOS (KEELING) ISLANDS",
		code_no: "166"
	},
	{
		id: 48,
		name: "COLOMBIA",
		code_no: "170"
	},
	{
		id: 49,
		name: "COMOROS",
		code_no: "174"
	},
	{
		id: 50,
		name: "MAYOTTE",
		code_no: "175"
	},
	{
		id: 51,
		name: "CONGO",
		code_no: " Republic of"
	},
	{
		id: 52,
		name: "CONGO",
		code_no: " Democratic Republic of (was Zaire)"
	},
	{
		id: 53,
		name: "COOK ISLANDS",
		code_no: "184"
	},
	{
		id: 54,
		name: "COSTA RICA",
		code_no: "188"
	},
	{
		id: 55,
		name: "CROATIA (local name: Hrvatska)",
		code_no: "191"
	},
	{
		id: 56,
		name: "CUBA",
		code_no: "192"
	},
	{
		id: 57,
		name: "CYPRUS",
		code_no: "196"
	},
	{
		id: 58,
		name: "CZECH REPUBLIC",
		code_no: "203"
	},
	{
		id: 59,
		name: "BENIN",
		code_no: "204"
	},
	{
		id: 60,
		name: "DENMARK",
		code_no: "208"
	},
	{
		id: 61,
		name: "DOMINICA",
		code_no: "212"
	},
	{
		id: 62,
		name: "DOMINICAN REPUBLIC",
		code_no: "214"
	},
	{
		id: 63,
		name: "ECUADOR",
		code_no: "218"
	},
	{
		id: 64,
		name: "EL SALVADOR",
		code_no: "222"
	},
	{
		id: 65,
		name: "EQUATORIAL GUINEA",
		code_no: "226"
	},
	{
		id: 66,
		name: "ETHIOPIA",
		code_no: "231"
	},
	{
		id: 67,
		name: "ERITREA",
		code_no: "232"
	},
	{
		id: 68,
		name: "ESTONIA",
		code_no: "233"
	},
	{
		id: 69,
		name: "FAROE ISLANDS",
		code_no: "234"
	},
	{
		id: 70,
		name: "FALKLAND ISLANDS (MALVINAS)",
		code_no: "238"
	},
	{
		id: 71,
		name: "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS",
		code_no: "239"
	},
	{
		id: 72,
		name: "FIJI",
		code_no: "242"
	},
	{
		id: 73,
		name: "FINLAND",
		code_no: "246"
	},
	{
		id: 74,
		name: "AALAND ISLANDS",
		code_no: "248"
	},
	{
		id: 75,
		name: "FRANCE",
		code_no: "250"
	},
	{
		id: 76,
		name: "FRENCH GUIANA",
		code_no: "254"
	},
	{
		id: 77,
		name: "FRENCH POLYNESIA",
		code_no: "258"
	},
	{
		id: 78,
		name: "FRENCH SOUTHERN TERRITORIES",
		code_no: "260"
	},
	{
		id: 79,
		name: "DJIBOUTI",
		code_no: "262"
	},
	{
		id: 80,
		name: "GABON",
		code_no: "266"
	},
	{
		id: 81,
		name: "GEORGIA",
		code_no: "268"
	},
	{
		id: 82,
		name: "GAMBIA",
		code_no: "270"
	},
	{
		id: 83,
		name: "PALESTINIAN TERRITORY",
		code_no: " Occupied"
	},
	{
		id: 84,
		name: "GERMANY",
		code_no: "276"
	},
	{
		id: 85,
		name: "GHANA",
		code_no: "288"
	},
	{
		id: 86,
		name: "GIBRALTAR",
		code_no: "292"
	},
	{
		id: 87,
		name: "KIRIBATI",
		code_no: "296"
	},
	{
		id: 88,
		name: "GREECE",
		code_no: "300"
	},
	{
		id: 89,
		name: "GREENLAND",
		code_no: "304"
	},
	{
		id: 90,
		name: "GRENADA",
		code_no: "308"
	},
	{
		id: 91,
		name: "GUADELOUPE",
		code_no: "312"
	},
	{
		id: 92,
		name: "GUAM",
		code_no: "316"
	},
	{
		id: 93,
		name: "GUATEMALA",
		code_no: "320"
	},
	{
		id: 94,
		name: "GUINEA",
		code_no: "324"
	},
	{
		id: 95,
		name: "GUYANA",
		code_no: "328"
	},
	{
		id: 96,
		name: "HAITI",
		code_no: "332"
	},
	{
		id: 97,
		name: "HEARD AND MC DONALD ISLANDS",
		code_no: "334"
	},
	{
		id: 98,
		name: "VATICAN CITY STATE (HOLY SEE)",
		code_no: "336"
	},
	{
		id: 99,
		name: "HONDURAS",
		code_no: "340"
	},
	{
		id:100,
		name: "HONG KONG",
		code_no: "344"
	},
	{
		id:101,
		name: "HUNGARY",
		code_no: "348"
	},
	{
		id:102,
		name: "ICELAND",
		code_no: "352"
	},
	{
		id:103,
		name: "INDIA",
		code_no: "356"
	},
	{
		id:104,
		name: "INDONESIA",
		code_no: "360"
	},
	{
		id:105,
		name: "IRAN (ISLAMIC REPUBLIC OF)",
		code_no: "364"
	},
	{
		id:106,
		name: "IRAQ",
		code_no: "368"
	},
	{
		id:107,
		name: "IRELAND",
		code_no: "372"
	},
	{
		id:108,
		name: "ISRAEL",
		code_no: "376"
	},
	{
		id:109,
		name: "ITALY",
		code_no: "380"
	},
	{
		id:110,
		name: "COTE D'IVOIRE",
		code_no: "384"
	},
	{
		id:111,
		name: "JAMAICA",
		code_no: "388"
	},
	{
		id:112,
		name: "JAPAN",
		code_no: "392"
	},
	{
		id:113,
		name: "KAZAKHSTAN",
		code_no: "398"
	},
	{
		id:114,
		name: "JORDAN",
		code_no: "400"
	},
	{
		id:115,
		name: "KENYA",
		code_no: "404"
	},
	{
		id:116,
		name: "KOREA",
		code_no: " DEMOCRATIC PEOPLE'S REPUBLIC OF"
	},
	{
		id:117,
		name: "KOREA",
		code_no: " REPUBLIC OF"
	},
	{
		id:118,
		name: "KUWAIT",
		code_no: "414"
	},
	{
		id:119,
		name: "KYRGYZSTAN",
		code_no: "417"
	},
	{
		id:120,
		name: "LAO PEOPLE'S DEMOCRATIC REPUBLIC",
		code_no: "418"
	},
	{
		id:121,
		name: "LEBANON",
		code_no: "422"
	},
	{
		id:122,
		name: "LESOTHO",
		code_no: "426"
	},
	{
		id:123,
		name: "LATVIA",
		code_no: "428"
	},
	{
		id:124,
		name: "LIBERIA",
		code_no: "430"
	},
	{
		id:125,
		name: "LIBYAN ARAB JAMAHIRIYA",
		code_no: "434"
	},
	{
		id:126,
		name: "LIECHTENSTEIN",
		code_no: "438"
	},
	{
		id:127,
		name: "LITHUANIA",
		code_no: "440"
	},
	{
		id:128,
		name: "LUXEMBOURG",
		code_no: "442"
	},
	{
		id:129,
		name: "MACAU",
		code_no: "446"
	},
	{
		id:130,
		name: "MADAGASCAR",
		code_no: "450"
	},
	{
		id:131,
		name: "MALAWI",
		code_no: "454"
	},
	{
		id:132,
		name: "MALDIVES",
		code_no: "462"
	},
	{
		id:133,
		name: "MALI",
		code_no: "466"
	},
	{
		id:134,
		name: "MALTA",
		code_no: "470"
	},
	{
		id:135,
		name: "MARTINIQUE",
		code_no: "474"
	},
	{
		id:136,
		name: "MAURITANIA",
		code_no: "478"
	},
	{
		id:137,
		name: "MAURITIUS",
		code_no: "480"
	},
	{
		id:138,
		name: "MEXICO",
		code_no: "484"
	},
	{
		id:139,
		name: "MONACO",
		code_no: "492"
	},
	{
		id:140,
		name: "MONGOLIA",
		code_no: "496"
	},
	{
		id:141,
		name: "MOLDOVA",
		code_no: " REPUBLIC OF"
	},
	{
		id:142,
		name: "MONTSERRAT",
		code_no: "500"
	},
	{
		id:143,
		name: "MOROCCO",
		code_no: "504"
	},
	{
		id:144,
		name: "MOZAMBIQUE",
		code_no: "508"
	},
	{
		id:145,
		name: "OMAN",
		code_no: "512"
	},
	{
		id:146,
		name: "NAMIBIA",
		code_no: "516"
	},
	{
		id:147,
		name: "NAURU",
		code_no: "520"
	},
	{
		id:148,
		name: "NEPAL",
		code_no: "524"
	},
	{
		id:149,
		name: "NETHERLANDS",
		code_no: "528"
	},
	{
		id:150,
		name: "NETHERLANDS ANTILLES",
		code_no: "530"
	},
	{
		id:151,
		name: "ARUBA",
		code_no: "533"
	},
	{
		id:152,
		name: "NEW CALEDONIA",
		code_no: "540"
	},
	{
		id:153,
		name: "VANUATU",
		code_no: "548"
	},
	{
		id:154,
		name: "NEW ZEALAND",
		code_no: "554"
	},
	{
		id:155,
		name: "NICARAGUA",
		code_no: "558"
	},
	{
		id:156,
		name: "NIGER",
		code_no: "562"
	},
	{
		id:157,
		name: "NIGERIA",
		code_no: "566"
	},
	{
		id:158,
		name: "NIUE",
		code_no: "570"
	},
	{
		id:159,
		name: "NORFOLK ISLAND",
		code_no: "574"
	},
	{
		id:160,
		name: "NORWAY",
		code_no: "578"
	},
	{
		id:161,
		name: "NORTHERN MARIANA ISLANDS",
		code_no: "580"
	},
	{
		id:162,
		name: "UNITED STATES MINOR OUTLYING ISLANDS",
		code_no: "581"
	},
	{
		id:163,
		name: "MICRONESIA",
		code_no: " FEDERATED STATES OF"
	},
	{
		id:164,
		name: "MARSHALL ISLANDS",
		code_no: "584"
	},
	{
		id:165,
		name: "PALAU",
		code_no: "585"
	},
	{
		id:166,
		name: "PAKISTAN",
		code_no: "586"
	},
	{
		id:167,
		name: "PANAMA",
		code_no: "591"
	},
	{
		id:168,
		name: "PAPUA NEW GUINEA",
		code_no: "598"
	},
	{
		id:169,
		name: "PARAGUAY",
		code_no: "600"
	},
	{
		id:170,
		name: "PERU",
		code_no: "604"
	},
	{
		id:171,
		name: "PHILIPPINES",
		code_no: "608"
	},
	{
		id:172,
		name: "PITCAIRN",
		code_no: "612"
	},
	{
		id:173,
		name: "POLAND",
		code_no: "616"
	},
	{
		id:174,
		name: "PORTUGAL",
		code_no: "620"
	},
	{
		id:175,
		name: "GUINEA-BISSAU",
		code_no: "624"
	},
	{
		id:176,
		name: "TIMOR-LESTE",
		code_no: "626"
	},
	{
		id:177,
		name: "PUERTO RICO",
		code_no: "630"
	},
	{
		id:178,
		name: "QATAR",
		code_no: "634"
	},
	{
		id:179,
		name: "REUNION",
		code_no: "638"
	},
	{
		id:180,
		name: "ROMANIA",
		code_no: "642"
	},
	{
		id:181,
		name: "RUSSIAN FEDERATION",
		code_no: "643"
	},
	{
		id:182,
		name: "RWANDA",
		code_no: "646"
	},
	{
		id:183,
		name: "SAINT HELENA",
		code_no: "654"
	},
	{
		id:184,
		name: "SAINT KITTS AND NEVIS",
		code_no: "659"
	},
	{
		id:185,
		name: "ANGUILLA",
		code_no: "660"
	},
	{
		id:186,
		name: "SAINT LUCIA",
		code_no: "662"
	},
	{
		id:187,
		name: "SAINT PIERRE AND MIQUELON",
		code_no: "666"
	},
	{
		id:188,
		name: "SAINT VINCENT AND THE GRENADINES",
		code_no: "670"
	},
	{
		id:189,
		name: "SAN MARINO",
		code_no: "674"
	},
	{
		id:190,
		name: "SAO TOME AND PRINCIPE",
		code_no: "678"
	},
	{
		id:191,
		name: "SAUDI ARABIA",
		code_no: "682"
	},
	{
		id:192,
		name: "SENEGAL",
		code_no: "686"
	},
	{
		id:193,
		name: "SEYCHELLES",
		code_no: "690"
	},
	{
		id:194,
		name: "SIERRA LEONE",
		code_no: "694"
	},
	{
		id:195,
		name: "SINGAPORE",
		code_no: "702"
	},
	{
		id:196,
		name: "SLOVAKIA",
		code_no: "703"
	},
	{
		id:197,
		name: "VIET NAM",
		code_no: "704"
	},
	{
		id:198,
		name: "SLOVENIA",
		code_no: "705"
	},
	{
		id:199,
		name: "SOMALIA",
		code_no: "706"
	},
	{
		id:200,
		name: "SOUTH AFRICA",
		code_no: "710"
	},
	{
		id:201,
		name: "ZIMBABWE",
		code_no: "716"
	},
	{
		id:202,
		name: "SPAIN",
		code_no: "724"
	},
	{
		id:203,
		name: "WESTERN SAHARA",
		code_no: "732"
	},
	{
		id:204,
		name: "SUDAN",
		code_no: "736"
	},
	{
		id:205,
		name: "SURINAME",
		code_no: "740"
	},
	{
		id:206,
		name: "SVALBARD AND JAN MAYEN ISLANDS",
		code_no: "744"
	},
	{
		id:207,
		name: "SWAZILAND",
		code_no: "748"
	},
	{
		id:208,
		name: "SWEDEN",
		code_no: "752"
	},
	{
		id:209,
		name: "SWITZERLAND",
		code_no: "756"
	},
	{
		id:210,
		name: "SYRIAN ARAB REPUBLIC",
		code_no: "760"
	},
	{
		id:211,
		name: "TAJIKISTAN",
		code_no: "762"
	},
	{
		id:212,
		name: "THAILAND",
		code_no: "764"
	},
	{
		id:213,
		name: "TOGO",
		code_no: "768"
	},
	{
		id:214,
		name: "TOKELAU",
		code_no: "772"
	},
	{
		id:215,
		name: "TONGA",
		code_no: "776"
	},
	{
		id:216,
		name: "TRINIDAD AND TOBAGO",
		code_no: "780"
	},
	{
		id:217,
		name: "UNITED ARAB EMIRATES",
		code_no: "784"
	},
	{
		id:218,
		name: "TUNISIA",
		code_no: "788"
	},
	{
		id:219,
		name: "TURKEY",
		code_no: "792"
	},
	{
		id:220,
		name: "TURKMENISTAN",
		code_no: "795"
	},
	{
		id:221,
		name: "TURKS AND CAICOS ISLANDS",
		code_no: "796"
	},
	{
		id:222,
		name: "TUVALU",
		code_no: "798"
	},
	{
		id:223,
		name: "UGANDA",
		code_no: "800"
	},
	{
		id:224,
		name: "UKRAINE",
		code_no: "804"
	},
	{
		id:225,
		name: "MACEDONIA",
		code_no: " THE FORMER YUGOSLAV REPUBLIC OF"
	},
	{
		id:226,
		name: "EGYPT",
		code_no: "818"
	},
	{
		id:227,
		name: "UNITED KINGDOM",
		code_no: "826"
	},
	{
		id:228,
		name: "TANZANIA",
		code_no: " UNITED REPUBLIC OF"
	},
	{
		id:229,
		name: "UNITED STATES",
		code_no: "840"
	},
	{
		id:230,
		name: "VIRGIN ISLANDS (U.S.)",
		code_no: "850"
	},
	{
		id:231,
		name: "BURKINA FASO",
		code_no: "854"
	},
	{
		id:232,
		name: "URUGUAY",
		code_no: "858"
	},
	{
		id:233,
		name: "UZBEKISTAN",
		code_no: "860"
	},
	{
		id:234,
		name: "VENEZUELA",
		code_no: "862"
	},
	{
		id:235,
		name: "WALLIS AND FUTUNA ISLANDS",
		code_no: "876"
	},
	{
		id:236,
		name: "SAMOA",
		code_no: "882"
	},
	{
		id:237,
		name: "YEMEN",
		code_no: "887"
	},
	{
		id:238,
		name: "SERBIA AND MONTENEGRO",
		code_no: "891"
	},
	{
		id:239,
		name: "ZAMBIA",
		code_no: "894"
	},
	{
		id:240,
		name: "OTHERS COUNTRY",
		code_no: "999"
	}
]