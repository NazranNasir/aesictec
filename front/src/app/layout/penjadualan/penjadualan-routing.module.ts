import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PenjadualanComponent } from './penjadualan.component';

const routes: Routes = [
  {
    path: '',
    component: PenjadualanComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PenjadualanRoutingModule { }
