import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PenjadualanRoutingModule } from './penjadualan-routing.module';
import { PageHeaderModule } from './../../shared';

import { PenjadualanComponent } from './penjadualan.component';
import { TableJadualComponent } from './components/table-jadual/table-jadual.component';
import { ParameterJadualComponent } from './components/parameter-jadual/parameter-jadual.component';

@NgModule({
  imports: [
    CommonModule,
    PenjadualanRoutingModule,
    PageHeaderModule
  ],
  declarations: [PenjadualanComponent, TableJadualComponent, ParameterJadualComponent]
})
export class PenjadualanModule { }
