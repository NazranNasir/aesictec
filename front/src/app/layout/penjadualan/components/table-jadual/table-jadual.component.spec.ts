import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableJadualComponent } from './table-jadual.component';

describe('TableJadualComponent', () => {
  let component: TableJadualComponent;
  let fixture: ComponentFixture<TableJadualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableJadualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableJadualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
