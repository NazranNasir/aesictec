import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParameterJadualComponent } from './parameter-jadual.component';

describe('ParameterJadualComponent', () => {
  let component: ParameterJadualComponent;
  let fixture: ComponentFixture<ParameterJadualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParameterJadualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterJadualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
