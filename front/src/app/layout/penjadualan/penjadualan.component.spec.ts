import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenjadualanComponent } from './penjadualan.component';

describe('PenjadualanComponent', () => {
  let component: PenjadualanComponent;
  let fixture: ComponentFixture<PenjadualanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenjadualanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenjadualanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
