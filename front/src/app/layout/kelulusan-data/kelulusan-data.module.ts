import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KelulusanDataRoutingModule } from './kelulusan-data-routing.module';
import { PageHeaderModule } from './../../shared';

import { KelulusanDataComponent } from './kelulusan-data.component';

@NgModule({
  imports: [
    CommonModule,
    KelulusanDataRoutingModule,
    PageHeaderModule
  ],
  declarations: [KelulusanDataComponent]
})
export class KelulusanDataModule { }
