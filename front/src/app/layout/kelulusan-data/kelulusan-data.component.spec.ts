import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KelulusanDataComponent } from './kelulusan-data.component';

describe('KelulusanDataComponent', () => {
  let component: KelulusanDataComponent;
  let fixture: ComponentFixture<KelulusanDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KelulusanDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KelulusanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
