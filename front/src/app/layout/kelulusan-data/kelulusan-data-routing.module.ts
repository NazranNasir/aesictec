import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KelulusanDataComponent } from './kelulusan-data.component';

const routes: Routes = [
  {
    path: '',
    component: KelulusanDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KelulusanDataRoutingModule { }
