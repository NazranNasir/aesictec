import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SemakanDataRoutingModule } from './semakan-data-routing.module';
import { PageHeaderModule } from './../../shared';

import { SemakanDataComponent } from './semakan-data.component';

@NgModule({
  imports: [
    CommonModule,
    SemakanDataRoutingModule,
    PageHeaderModule
  ],
  declarations: [SemakanDataComponent]
})
export class SemakanDataModule { }
