import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SemakanDataComponent } from './semakan-data.component';

const routes: Routes = [
  {
    path: '',
    component: SemakanDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SemakanDataRoutingModule { }
