import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemakanDataComponent } from './semakan-data.component';

describe('SemakanDataComponent', () => {
  let component: SemakanDataComponent;
  let fixture: ComponentFixture<SemakanDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemakanDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemakanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
