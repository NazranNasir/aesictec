import { Component, OnInit } from '@angular/core';
import { KODPENYIASATAN } from './../../mock-data/mock-kod-penyiasatan';

@Component({
  selector: 'app-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.css']
})
export class ImportComponent implements OnInit {
  kodPenyiasatan = KODPENYIASATAN;
  constructor() { }

  ngOnInit() {
  }

}
