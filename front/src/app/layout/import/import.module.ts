import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImportRoutingModule } from './import-routing.module';
import { PageHeaderModule } from './../../shared';

import { ImportComponent } from './import.component';

@NgModule({
  imports: [
    CommonModule,
    ImportRoutingModule,
    PageHeaderModule
  ],
  declarations: [ImportComponent]
})

export class ImportModule { }
