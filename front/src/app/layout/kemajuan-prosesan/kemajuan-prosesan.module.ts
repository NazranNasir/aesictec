import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KemajuanProsesanRoutingModule } from './kemajuan-prosesan-routing.module';
import { PageHeaderModule } from './../../shared';

import { KemajuanProsesanComponent } from './kemajuan-prosesan.component';

@NgModule({
  imports: [
    CommonModule,
    KemajuanProsesanRoutingModule,
    PageHeaderModule
  ],
  declarations: [KemajuanProsesanComponent]
})
export class KemajuanProsesanModule { }
