import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KemajuanProsesanComponent } from './kemajuan-prosesan.component';

describe('KemajuanProsesanComponent', () => {
  let component: KemajuanProsesanComponent;
  let fixture: ComponentFixture<KemajuanProsesanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KemajuanProsesanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KemajuanProsesanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
