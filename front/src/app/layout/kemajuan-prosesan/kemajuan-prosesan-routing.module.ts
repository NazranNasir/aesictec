import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KemajuanProsesanComponent } from './kemajuan-prosesan.component';

const routes: Routes = [
  {
    path: '',
    component: KemajuanProsesanComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KemajuanProsesanRoutingModule { }
