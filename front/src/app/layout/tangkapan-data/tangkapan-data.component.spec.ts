import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TangkapanDataComponent } from './tangkapan-data.component';

describe('TangkapanDataComponent', () => {
  let component: TangkapanDataComponent;
  let fixture: ComponentFixture<TangkapanDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TangkapanDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TangkapanDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
