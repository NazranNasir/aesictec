import { Component, OnInit, Input } from '@angular/core';
import { NEGERI } from './../../../../mock-data/mock-negeri';
import { PEJABATOPERASI } from './../../../../mock-data/mock-pejabat-operasi';
import { KodPenyiasatan } from './../../../../models/kod-penyiasatan';
import { KodPenyiasatanService } from './../../../../services/kod-penyiasatan.service';
import { NoSiri } from './../../../../models/no-siri';
import { NoSiriService } from './../../../../services/no-siri.service';
import { TahunRujukan } from './../../../../models/tahun-rujukan';
import { TahunRujukanService } from './../../../../services/tahun-rujukan.service';

@Component({
  selector: 'app-icr',
  templateUrl: './icr.component.html',
  styleUrls: ['./icr.component.css']
})
export class IcrComponent implements OnInit {
  @Input() title: string;
  negeri = NEGERI;
  pejabatOperasi = PEJABATOPERASI;
  kodPenyiasatan: KodPenyiasatan[];
  noSiri: NoSiri[];
  tahunRujukan: TahunRujukan[];
  selectedKP: number;
  selectedNoSiri: number;
  selectedTahun: number;
  statusSemakan: boolean;
  statusImport: boolean;
  constructor(
    private kodPenyiasatanService: KodPenyiasatanService,
    private noSiriService: NoSiriService,
    private tahunRujukanService: TahunRujukanService
  ) { }

  ngOnInit() {
    this.kodPenyiasatanService.getAllKP().subscribe(kodPenyiasatan => this.kodPenyiasatan = kodPenyiasatan);
    this.noSiriService.getAllNOSIRI().subscribe(noSiri => this.noSiri = noSiri);
    this.tahunRujukanService.getALLTAHUNRUJUKAN().subscribe(tahunRujukan => this.tahunRujukan = tahunRujukan);
    this.statusSemakan = false;
    this.statusImport = false;
  }

  onSemakImport() {
    this.statusImport = true;
  }

  onSemakFail() {
    this.statusSemakan = true;
  }

  onChange(e, type): void {
    if(type == 'kod-penyiasatan')
      this.selectedKP = e.target.value;
    else if(type == 'no-siri')
      this.selectedNoSiri = e.target.value;
    else if(type == 'tahun-rujukan')
      this.selectedTahun = e.target.value;
  }

}
