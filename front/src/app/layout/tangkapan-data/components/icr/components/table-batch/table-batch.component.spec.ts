import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableBatchComponent } from './table-batch.component';

describe('TableBatchComponent', () => {
  let component: TableBatchComponent;
  let fixture: ComponentFixture<TableBatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableBatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableBatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
