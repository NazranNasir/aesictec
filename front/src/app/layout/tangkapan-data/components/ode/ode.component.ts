
import { Component, OnInit, Input } from '@angular/core';
import { NEGERI } from './../../../../mock-data/mock-negeri';
import { PEJABATOPERASI } from './../../../../mock-data/mock-pejabat-operasi';
import { KodPenyiasatan } from './../../../../models/kod-penyiasatan';
import { KodPenyiasatanService } from './../../../../services/kod-penyiasatan.service';
import { NoSiri } from './../../../../models/no-siri';
import { NoSiriService } from './../../../../services/no-siri.service';
import { TahunRujukan } from './../../../../models/tahun-rujukan';
import { TahunRujukanService } from './../../../../services/tahun-rujukan.service';

@Component({
  selector: 'app-ode',
  templateUrl: './ode.component.html',
  styleUrls: ['./ode.component.css']
})
export class OdeComponent implements OnInit {
  @Input() title: string;
  negeri = NEGERI;
  pejabatOperasi = PEJABATOPERASI;
  kodPenyiasatan: KodPenyiasatan[];
  noSiri: NoSiri[];
  tahunRujukan: TahunRujukan[];
  selectedKP: number;
  selectedNoSiri: number;
  selectedTahun: number;
  selectedBorang: number;
  statusSemakan: boolean;
  isShort: boolean;
  btnSemak: boolean;
  constructor(
    private kodPenyiasatanService: KodPenyiasatanService,
    private noSiriService: NoSiriService,
    private tahunRujukanService: TahunRujukanService
  ) { }

  ngOnInit() {
    this.kodPenyiasatanService.getAllKP().subscribe(kodPenyiasatan => this.kodPenyiasatan = kodPenyiasatan);
    this.noSiriService.getAllNOSIRI().subscribe(noSiri => this.noSiri = noSiri);
    this.tahunRujukanService.getALLTAHUNRUJUKAN().subscribe(tahunRujukan => this.tahunRujukan = tahunRujukan);
    this.statusSemakan = false;
    this.btnSemak = false;
    this.isShort = false;
  }
  onSemak(kp) {
    this.statusSemakan = true;
    if(kp == 7 || kp == 8 || kp == 9 || kp == 10)
      this.isShort = true;
    else
      this.isShort = false;
  }
  onPilihBorang(e): void {
    if(e.target.value == "SBU")
      this.selectedBorang = 1;
    else if(e.target.value == "BBU")
      this.selectedBorang = 2;
  }
  onChange(e, type): void {
    this.statusSemakan = false;
    if(type == 'kod-penyiasatan')
      this.selectedKP = e.target.value;
    // else if(type == 'no-siri'){
    //   this.selectedNoSiri = e.target.value;
    //   console.log(this.selectedNoSiri);
    // }

    else if(type == 'tahun-rujukan')
      this.selectedTahun = e.target.value;
  }

  onKeyupNoSiri(e):void {
    if(e.target.value.length > 0)
    {
      this.btnSemak = true;
    }
    else
    {
      this.btnSemak = false;
    }
  }

}
