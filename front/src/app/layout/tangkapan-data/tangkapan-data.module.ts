import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TangkapanDataRoutingModule } from './tangkapan-data-routing.module';
import { PageHeaderModule } from './../../shared';

import { TangkapanDataComponent } from './tangkapan-data.component';
import { OdeComponent } from './components/ode/ode.component';
import { IcrComponent } from './components/icr/icr.component';
import { TableBatchComponent } from './components/icr/components/table-batch/table-batch.component';

@NgModule({
  imports: [
    CommonModule,
    TangkapanDataRoutingModule,
    PageHeaderModule
  ],
  declarations: [TangkapanDataComponent, OdeComponent, IcrComponent, TableBatchComponent]
})
export class TangkapanDataModule { }
