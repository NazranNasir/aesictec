import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TangkapanDataComponent } from './tangkapan-data.component';
import { OdeComponent } from './components/ode/ode.component';
import { IcrComponent } from './components/icr/icr.component';

const routes: Routes = [
  {
    path: '',
    component: TangkapanDataComponent,
    children: [
      {
        path: 'ode',
        component: OdeComponent
      },
      {
        path: 'import-icr',
        component: IcrComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TangkapanDataRoutingModule { }
