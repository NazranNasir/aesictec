import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditSpekComponent } from './edit-spek.component';

const routes: Routes = [
  {
    path: '',
    component: EditSpekComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditSpekRoutingModule { }
