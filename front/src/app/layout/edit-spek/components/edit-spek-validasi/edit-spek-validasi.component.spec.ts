import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpekValidasiComponent } from './edit-spek-validasi.component';

describe('EditSpekValidasiComponent', () => {
  let component: EditSpekValidasiComponent;
  let fixture: ComponentFixture<EditSpekValidasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpekValidasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpekValidasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
