import { Component, OnInit, Input } from '@angular/core';
import { KodPenyiasatan } from './../../../../models/kod-penyiasatan';
import { KodPenyiasatanService } from './../../../../services/kod-penyiasatan.service';

@Component({
  selector: 'app-edit-spek-validasi',
  templateUrl: './edit-spek-validasi.component.html',
  styleUrls: ['./edit-spek-validasi.component.css']
})
export class EditSpekValidasiComponent implements OnInit {
  @Input() id: number;
  kodPenyiasatan: KodPenyiasatan;

  constructor(private kodPenyiasatanService: KodPenyiasatanService) { }

  ngOnInit() {
    this.kodPenyiasatanService.getKP(this.id).subscribe(kodPenyiasatan => this.kodPenyiasatan = kodPenyiasatan);
  }

}
