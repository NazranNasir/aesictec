import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpekHeaderComponent } from './edit-spek-header.component';

describe('EditSpekHeaderComponent', () => {
  let component: EditSpekHeaderComponent;
  let fixture: ComponentFixture<EditSpekHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpekHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpekHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
