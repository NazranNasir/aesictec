import { Component, OnInit, Input } from '@angular/core';
import { KodPenyiasatan } from './../../../../models/kod-penyiasatan';
import { KodPenyiasatanService } from './../../../../services/kod-penyiasatan.service';

@Component({
  selector: 'app-edit-spek-header',
  templateUrl: './edit-spek-header.component.html',
  styleUrls: ['./edit-spek-header.component.css']
})
export class EditSpekHeaderComponent implements OnInit {
  @Input() name: string;
  @Input() code_no: number;
  @Input() no_siri: number;
  @Input() tahun_rujukan: number;
  @Input() id: number;
  ralatCount: number;
  kodPenyiasatan: KodPenyiasatan;

  constructor(private kodPenyiasatanService: KodPenyiasatanService) { }

  ngOnInit() {
    this.kodPenyiasatanService.getKP(this.id).subscribe(kodPenyiasatan => this.kodPenyiasatan = kodPenyiasatan);
    if(this.id == 1)
      this.ralatCount = 70;
    else if(this.id == 2)
      this.ralatCount = 0;
    else if(this.id == 3)
      this.ralatCount = 600;
    else if(this.id == 4)
      this.ralatCount = 600;
    else if(this.id == 5)
      this.ralatCount = 600;
    else if(this.id == 6)
      this.ralatCount = 600;
  }

}
