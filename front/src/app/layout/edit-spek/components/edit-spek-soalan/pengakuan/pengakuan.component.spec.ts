import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PengakuanComponent } from './pengakuan.component';

describe('PengakuanComponent', () => {
  let component: PengakuanComponent;
  let fixture: ComponentFixture<PengakuanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PengakuanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PengakuanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
