import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpekSoalanComponent } from './edit-spek-soalan.component';

describe('EditSpekSoalanComponent', () => {
  let component: EditSpekSoalanComponent;
  let fixture: ComponentFixture<EditSpekSoalanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpekSoalanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpekSoalanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
