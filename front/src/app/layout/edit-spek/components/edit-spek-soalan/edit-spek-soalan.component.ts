import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-edit-spek-soalan',
  templateUrl: './edit-spek-soalan.component.html',
  styleUrls: ['./edit-spek-soalan.component.scss']
})
export class EditSpekSoalanComponent implements OnInit {
  // currentOrientation = "horizontal";
  // currentJustify = "fill";
  @Input() id: number;
  @Input() borang: number;
  active_ictec: string;
  active_umum: string;
  active_mk: string;
  active_test: string;
  constructor() { }
  ngOnInit() {
    if(this.id == 1)
    {
      this.active_ictec = 'active';
      this.active_test = '';
      this.active_umum = '';
      this.active_mk = '';
    }
    else if(this.id == 11)
    {
      this.active_test = 'active';
      this.active_ictec = '';
      this.active_umum = '';
      this.active_mk = '';
    }
    else if ((this.id == 7 || this.id == 8 || this.id == 9 || this.id == 10) && this.borang == 1)
    {
      this.active_ictec = '';
      this.active_test = '';
      this.active_umum = 'active';
      this.active_mk = '';
    }
    else if (this.id == 3 || this.id == 4 || this.id == 5 || this.id == 6)
    {
      this.active_ictec = '';
      this.active_test = '';
      this.active_umum = '';
      this.active_mk = 'active';
    }
  }

}
