import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100621Component } from './f3100621.component';

describe('F3100621Component', () => {
  let component: F3100621Component;
  let fixture: ComponentFixture<F3100621Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100621Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100621Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
