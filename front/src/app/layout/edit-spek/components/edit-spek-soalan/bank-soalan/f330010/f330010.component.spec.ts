import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330010Component } from './f330010.component';

describe('F330010Component', () => {
  let component: F330010Component;
  let fixture: ComponentFixture<F330010Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330010Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330010Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
