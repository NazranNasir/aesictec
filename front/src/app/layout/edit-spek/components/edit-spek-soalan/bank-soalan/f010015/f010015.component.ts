import { Component, OnInit, Input } from '@angular/core';
import {Poskod} from '../../../../../../models/poskod';
import {PoskodService} from '../../../../../../services/poskod.service';

@Component({
  selector: 'app-f010015',
  templateUrl: './f010015.component.html',
  styleUrls: ['./f010015.component.css', '../bank-soalan.component.css']
})
export class F010015Component implements OnInit {
  poskod : Poskod[];
  @Input() num: string;
  constructor(
    private poskodService: PoskodService
  ) { }

  ngOnInit() {
    this.poskodService.getALLPOSKOD().subscribe(poskod => this.poskod = poskod);
  }
}
