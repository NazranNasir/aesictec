import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310004Component } from './f310004.component';

describe('F310004Component', () => {
  let component: F310004Component;
  let fixture: ComponentFixture<F310004Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310004Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
