import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F090016Component } from './f090016.component';

describe('F090016Component', () => {
  let component: F090016Component;
  let fixture: ComponentFixture<F090016Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F090016Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F090016Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
