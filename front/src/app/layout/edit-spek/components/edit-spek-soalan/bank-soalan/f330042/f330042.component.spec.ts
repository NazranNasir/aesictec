import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330042Component } from './f330042.component';

describe('F330042Component', () => {
  let component: F330042Component;
  let fixture: ComponentFixture<F330042Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330042Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330042Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
