import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F340001Component } from './f340001.component';

describe('F340001Component', () => {
  let component: F340001Component;
  let fixture: ComponentFixture<F340001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F340001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F340001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
