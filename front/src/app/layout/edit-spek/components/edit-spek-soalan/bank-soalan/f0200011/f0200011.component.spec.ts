import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F0200011Component } from './f0200011.component';

describe('F0200011Component', () => {
  let component: F0200011Component;
  let fixture: ComponentFixture<F0200011Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F0200011Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F0200011Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
