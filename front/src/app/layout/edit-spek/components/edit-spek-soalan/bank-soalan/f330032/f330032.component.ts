import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330032',
  templateUrl: './f330032.component.html',
  styleUrls: ['./f330032.component.css','../bank-soalan.component.css']
})
export class F330032Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330032', text: '(a) 2014' },
    { id:2, fnum:'330033', text: '(b) 2015' },
    { id:3, fnum:'330034', text: '(c) 2016' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
