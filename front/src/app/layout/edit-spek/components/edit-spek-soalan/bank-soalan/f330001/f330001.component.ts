import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330001',
  templateUrl: './f330001.component.html',
  styleUrls: ['./f330001.component.css', '../bank-soalan.component.css']
})
export class F330001Component implements OnInit {
  @Input() num: string;

  array: Array<{id: number, fnum:string, text: string}> = [
{ id:1, fnum:'01', text: '(a) Tempatan (cth. Amalan Peningkatan Kualiti(APK), Pensijilan Jenama Malaysia,Skim Organik Malaysia (SOM) dsb.)' },
{ id:2, fnum:'02', text: '(b) Antarabangsa' },
{ id:3, fnum:'03', text: '(c) Halal' },
{ id:4, fnum:'04', text: '(d) Tiada' },
];




  arr13_6: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330032', text: '(a) 2014' },
    { id:2, fnum:'330033', text: '(b) 2015' },
    { id:3, fnum:'330034', text: '(c) 2016' }
  ];
  arr13_8: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330036', text: '(a) Kos buruh' },
    { id:2, fnum:'330037', text: '(b) Kos operasi' },
    { id:3, fnum:'330038', text: '(c) Kos berulang lain' }
  ];
  arr13_9: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330039', text: '(a) Tanah, bangunan & struktur lain' },
    { id:2, fnum:'330040', text: '(b) Kenderaan, loji, perisian, jentera & peralatan' },
    { id:3, fnum:'330041', text: '(c) Lain-lain' }
  ];
  arr13_10: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330042', text: '(a) Dana sendiri' },
    { id:2, fnum:'330043', text: '(b) Syarikat Swasta' },
    { id:3, fnum:'330044', text: '(c) Kerajaan' },
    { id:4, fnum:'330045', text: '(d) Institusi Pengajian Tinggi' },
    { id:5, fnum:'330046', text: '(e) Dana Luar Negara' },
    { id:6, fnum:'330047', text: '(f) Dana Lain' }
  ];
  arr13_11: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330048', text: '(a) Penyelidikan asas' },
    { id:2, fnum:'330049', text: '(b) Penyelidikan gunaan' },
    { id:3, fnum:'330050', text: '(c) Penyelidikan eksperimen' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
