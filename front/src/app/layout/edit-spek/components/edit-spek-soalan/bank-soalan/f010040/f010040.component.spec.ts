import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010040Component } from './f010040.component';

describe('F010040Component', () => {
  let component: F010040Component;
  let fixture: ComponentFixture<F010040Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010040Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010040Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
