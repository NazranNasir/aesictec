import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010034Component } from './f010034.component';

describe('F010034Component', () => {
  let component: F010034Component;
  let fixture: ComponentFixture<F010034Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010034Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010034Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
