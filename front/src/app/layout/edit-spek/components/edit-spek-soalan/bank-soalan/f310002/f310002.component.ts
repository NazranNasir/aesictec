import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310002',
  templateUrl: './f310002.component.html',
  styleUrls: ['./f310002.component.css', '../bank-soalan.component.css']
})
export class F310002Component implements OnInit {
  @Input() num: string;
  @Input() text: string;
  constructor() { }

  ngOnInit() {
  }

}
