import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330010',
  templateUrl: './f330010.component.html',
  styleUrls: ['./f330010.component.css', '../bank-soalan.component.css']
})
export class F330010Component implements OnInit {
  @Input() num: string;

  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'10', text: '(a) Agensi kerajaan' },
    { id:2, fnum:'11', text: '(b) Institusi Pengajian Tinggi' },
    { id:3, fnum:'12', text: '(c) Swasta' },
    { id:4, fnum:'13', text: '(d) Tiada' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
