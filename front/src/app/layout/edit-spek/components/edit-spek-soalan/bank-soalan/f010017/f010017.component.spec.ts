import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010017Component } from './f010017.component';

describe('F010017Component', () => {
  let component: F010017Component;
  let fixture: ComponentFixture<F010017Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010017Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010017Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
