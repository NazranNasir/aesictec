import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010004Component } from './f010004.component';

describe('F010004Component', () => {
  let component: F010004Component;
  let fixture: ComponentFixture<F010004Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010004Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010004Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
