import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F340003Component } from './f340003.component';

describe('F340003Component', () => {
  let component: F340003Component;
  let fixture: ComponentFixture<F340003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F340003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F340003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
