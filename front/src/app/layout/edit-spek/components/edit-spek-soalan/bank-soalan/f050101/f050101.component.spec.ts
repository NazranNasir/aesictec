import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F050101Component } from './f050101.component';

describe('F050101Component', () => {
  let component: F050101Component;
  let fixture: ComponentFixture<F050101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F050101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F050101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
