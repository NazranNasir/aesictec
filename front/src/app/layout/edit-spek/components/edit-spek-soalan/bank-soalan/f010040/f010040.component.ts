import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f010040',
  templateUrl: './f010040.component.html',
  styleUrls: ['./f010040.component.css','../bank-soalan.component.css']
})
export class F010040Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, text: string}> = [
    { id:1, text: 'Bebas' },
    { id:2, text: 'Ibu Pejabat' },
    { id:3, text: 'Cawangan / Pejabat Operasi' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
