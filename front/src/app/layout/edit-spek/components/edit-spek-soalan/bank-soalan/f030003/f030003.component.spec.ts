import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F030003Component } from './f030003.component';

describe('F030003Component', () => {
  let component: F030003Component;
  let fixture: ComponentFixture<F030003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F030003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F030003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
