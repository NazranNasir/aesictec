import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F080001Component } from './f080001.component';

describe('F080001Component', () => {
  let component: F080001Component;
  let fixture: ComponentFixture<F080001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F080001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F080001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
