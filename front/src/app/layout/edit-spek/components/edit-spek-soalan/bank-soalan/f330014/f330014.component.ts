import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330014',
  templateUrl: './f330014.component.html',
  styleUrls: ['./f330014.component.css', '../bank-soalan.component.css']
})
export class F330014Component implements OnInit {
  @Input() num: string;

  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'14', text: '(a) Inovasi produk' },
    { id:2, fnum:'15', text: '(b) Inovasi proses' },
    { id:3, fnum:'16', text: '(c) Inovasi organisasi' },
    { id:4, fnum:'17', text: '(d) Inovasi pemasaran' },
    { id:5, fnum:'18', text: '(e) Tiada' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
