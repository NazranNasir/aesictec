import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310005',
  templateUrl: './f310005.component.html',
  styleUrls: ['./f310005.component.css', '../bank-soalan.component.css']
})
export class F310005Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'05', text: '(a) Intranet dalam perniagaan anda' },
    { id:2, fnum:'06', text: '(b) Extranet di antara perniagaan anda dan organisasi lain (termasuk perniagaan yang berkenaan)' },
    { id:3, fnum:'07', text: '(c) Rangkaian kawasan setempat' },
    { id:4, fnum:'08', text: '(d) Rangkaian kawasan setempat tanpa wayar' },
    { id:5, fnum:'09', text: '(e) Rangkaian kawasan luas' },
    { id:6, fnum:'10', text: '(f) Tiada di atas' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
