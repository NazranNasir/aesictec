import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310044Component } from './f310044.component';

describe('F310044Component', () => {
  let component: F310044Component;
  let fixture: ComponentFixture<F310044Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310044Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310044Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
