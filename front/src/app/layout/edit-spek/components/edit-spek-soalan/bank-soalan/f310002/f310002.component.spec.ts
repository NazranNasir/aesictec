import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310002Component } from './f310002.component';

describe('F310002Component', () => {
  let component: F310002Component;
  let fixture: ComponentFixture<F310002Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310002Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
