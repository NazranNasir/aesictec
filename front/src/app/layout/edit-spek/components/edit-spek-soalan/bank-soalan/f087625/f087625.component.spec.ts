import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F087625Component } from './f087625.component';

describe('F087625Component', () => {
  let component: F087625Component;
  let fixture: ComponentFixture<F087625Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F087625Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F087625Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
