import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F0100361Component } from './f0100361.component';

describe('F0100361Component', () => {
  let component: F0100361Component;
  let fixture: ComponentFixture<F0100361Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F0100361Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F0100361Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
