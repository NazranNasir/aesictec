import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330039Component } from './f330039.component';

describe('F330039Component', () => {
  let component: F330039Component;
  let fixture: ComponentFixture<F330039Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330039Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330039Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
