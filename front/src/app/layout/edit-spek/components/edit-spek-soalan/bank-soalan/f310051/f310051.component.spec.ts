import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310051Component } from './f310051.component';

describe('F310051Component', () => {
  let component: F310051Component;
  let fixture: ComponentFixture<F310051Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310051Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310051Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
