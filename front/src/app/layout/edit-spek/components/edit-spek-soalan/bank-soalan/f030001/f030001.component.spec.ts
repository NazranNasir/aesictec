import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F030001Component } from './f030001.component';

describe('F030001Component', () => {
  let component: F030001Component;
  let fixture: ComponentFixture<F030001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F030001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F030001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
