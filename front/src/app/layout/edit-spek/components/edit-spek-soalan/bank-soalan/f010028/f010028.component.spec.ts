import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010028Component } from './f010028.component';

describe('F010028Component', () => {
  let component: F010028Component;
  let fixture: ComponentFixture<F010028Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010028Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010028Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
