import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F061401Component } from './f061401.component';

describe('F061401Component', () => {
  let component: F061401Component;
  let fixture: ComponentFixture<F061401Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F061401Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F061401Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
