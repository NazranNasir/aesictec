import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f070101',
  templateUrl: './f070101.component.html',
  styleUrls: ['./f070101.component.css', '../bank-soalan.component.css']
})
export class F070101Component implements OnInit {
  @Input() num1: string;
  @Input() num2: string;
  @Input() num3: string;
  @Input() num4: string;
  @Input() num5: string;
  @Input() num6: string;
  @Input() num7: string;
  constructor() { }

  ngOnInit() {
  }

}
