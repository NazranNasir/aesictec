import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F051001Component } from './f051001.component';

describe('F051001Component', () => {
  let component: F051001Component;
  let fixture: ComponentFixture<F051001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F051001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F051001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
