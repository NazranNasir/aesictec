import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F040101Component } from './f040101.component';

describe('F040101Component', () => {
  let component: F040101Component;
  let fixture: ComponentFixture<F040101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F040101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F040101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
