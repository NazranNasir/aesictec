import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100031Component } from './f3100031.component';

describe('F3100031Component', () => {
  let component: F3100031Component;
  let fixture: ComponentFixture<F3100031Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100031Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100031Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
