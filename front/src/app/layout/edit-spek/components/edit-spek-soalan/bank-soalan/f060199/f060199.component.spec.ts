import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F060199Component } from './f060199.component';

describe('F060199Component', () => {
  let component: F060199Component;
  let fixture: ComponentFixture<F060199Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F060199Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F060199Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
