import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F0100351Component } from './f0100351.component';

describe('F0100351Component', () => {
  let component: F0100351Component;
  let fixture: ComponentFixture<F0100351Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F0100351Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F0100351Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
