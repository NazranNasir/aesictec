import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F070101Component } from './f070101.component';

describe('F070101Component', () => {
  let component: F070101Component;
  let fixture: ComponentFixture<F070101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F070101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F070101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
