import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F151001Component } from './f151001.component';

describe('F151001Component', () => {
  let component: F151001Component;
  let fixture: ComponentFixture<F151001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F151001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F151001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
