import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F080023Component } from './f080023.component';

describe('F080023Component', () => {
  let component: F080023Component;
  let fixture: ComponentFixture<F080023Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F080023Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F080023Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
