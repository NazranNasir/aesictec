import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F030013Component } from './f030013.component';

describe('F030013Component', () => {
  let component: F030013Component;
  let fixture: ComponentFixture<F030013Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F030013Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F030013Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
