import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010015Component } from './f010015.component';

describe('F010015Component', () => {
  let component: F010015Component;
  let fixture: ComponentFixture<F010015Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010015Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010015Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
