import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310054Component } from './f310054.component';

describe('F310054Component', () => {
  let component: F310054Component;
  let fixture: ComponentFixture<F310054Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310054Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310054Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
