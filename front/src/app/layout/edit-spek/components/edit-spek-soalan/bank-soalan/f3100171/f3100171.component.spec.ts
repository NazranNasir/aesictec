import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100171Component } from './f3100171.component';

describe('F3100171Component', () => {
  let component: F3100171Component;
  let fixture: ComponentFixture<F3100171Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100171Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100171Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
