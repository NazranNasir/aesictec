import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f010028',
  templateUrl: './f010028.component.html',
  styleUrls: ['./f010028.component.css', '../bank-soalan.component.css']
})
export class F010028Component implements OnInit {
  @Input() num: string;
  @Input() text: string;

  constructor() { }

  ngOnInit() {
  }

}
