import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310037',
  templateUrl: './f310037.component.html',
  styleUrls: ['./f310037.component.css', '../bank-soalan.component.css']
})
export class F310037Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'37', text: '(a) Media sosial (cth: Facebook, Instagram dan blog)' },
    { id:2, fnum:'38', text: '(b) Laman web kepunyaan sendiri' },
    { id:3, fnum:'39', text: '(c) Pasaran e-Dagang atas talian (cth: Lazada, Zalora , 11street, Alibaba)' },
    { id:4, fnum:'40', text: '(d) Rangkaian persendirian yang ditetapkan (cth: extranet, EDI)' },
    { id:5, fnum:'41', text: '(e) Aplikasi mudah alih (cth: Uber app, Lazada app, Dah makan app)' },
    { id:6, fnum:'42', text: '(f) Perkhidmatan pembayaran elektronik (cth: Perbankan internet ( Maybank2U, Interbank Giro), Kad kredit, Kad debit, Pembayaran atas talian (iPay88, Mol Pay, Paypal dll.))' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
