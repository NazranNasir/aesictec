import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F160001Component } from './f160001.component';

describe('F160001Component', () => {
  let component: F160001Component;
  let fixture: ComponentFixture<F160001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F160001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F160001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
