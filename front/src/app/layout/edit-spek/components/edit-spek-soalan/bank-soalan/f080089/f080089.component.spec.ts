import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F080089Component } from './f080089.component';

describe('F080089Component', () => {
  let component: F080089Component;
  let fixture: ComponentFixture<F080089Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F080089Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F080089Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
