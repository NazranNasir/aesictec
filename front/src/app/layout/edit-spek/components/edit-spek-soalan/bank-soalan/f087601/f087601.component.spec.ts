import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F087601Component } from './f087601.component';

describe('F087601Component', () => {
  let component: F087601Component;
  let fixture: ComponentFixture<F087601Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F087601Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F087601Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
