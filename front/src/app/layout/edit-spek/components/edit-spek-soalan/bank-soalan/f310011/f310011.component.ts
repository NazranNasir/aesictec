import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310011',
  templateUrl: './f310011.component.html',
  styleUrls: ['./f310011.component.css', '../bank-soalan.component.css']
})
export class F310011Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'11', text: '(a) Jalur sempit' },
    { id:2, fnum:'12', text: '(b) Jalur lebar tetap' },
    { id:3, fnum:'13', text: '(c) Jalur lebar mudah alih (Boleh pilih lebih daripada satu)' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
