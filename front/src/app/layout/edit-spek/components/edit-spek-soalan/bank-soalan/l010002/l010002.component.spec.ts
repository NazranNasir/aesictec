import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { L010002Component } from './l010002.component';

describe('L010002Component', () => {
  let component: L010002Component;
  let fixture: ComponentFixture<L010002Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ L010002Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(L010002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
