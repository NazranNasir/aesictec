import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310054',
  templateUrl: './f310054.component.html',
  styleUrls: ['./f310054.component.css', '../bank-soalan.component.css']
})
export class F310054Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310054', text: '(a) Tempatan' },
    { id:2, fnum:'310055', text: '(b) Antarabangsa' }
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
