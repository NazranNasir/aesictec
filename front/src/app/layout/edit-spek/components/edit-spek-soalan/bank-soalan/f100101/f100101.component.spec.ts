import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F100101Component } from './f100101.component';

describe('F100101Component', () => {
  let component: F100101Component;
  let fixture: ComponentFixture<F100101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F100101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F100101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
