import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100351Component } from './f3100351.component';

describe('F3100351Component', () => {
  let component: F3100351Component;
  let fixture: ComponentFixture<F3100351Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100351Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100351Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
