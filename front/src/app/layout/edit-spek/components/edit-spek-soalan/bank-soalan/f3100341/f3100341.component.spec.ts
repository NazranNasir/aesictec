import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100341Component } from './f3100341.component';

describe('F3100341Component', () => {
  let component: F3100341Component;
  let fixture: ComponentFixture<F3100341Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100341Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100341Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
