import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330001Component } from './f330001.component';

describe('F330001Component', () => {
  let component: F330001Component;
  let fixture: ComponentFixture<F330001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
