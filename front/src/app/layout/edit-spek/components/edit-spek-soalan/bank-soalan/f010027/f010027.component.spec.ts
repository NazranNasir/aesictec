import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010027Component } from './f010027.component';

describe('F010027Component', () => {
  let component: F010027Component;
  let fixture: ComponentFixture<F010027Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010027Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010027Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
