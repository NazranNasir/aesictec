import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330048',
  templateUrl: './f330048.component.html',
  styleUrls: ['./f330048.component.css', '../bank-soalan.component.css']
})
export class F330048Component implements OnInit {
  @Input() num: string;

  arr13_11: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330048', text: '(a) Penyelidikan asas' },
    { id:2, fnum:'330049', text: '(b) Penyelidikan gunaan' },
    { id:3, fnum:'330050', text: '(c) Penyelidikan eksperimen' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
