import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F0100161Component } from './f0100161.component';

describe('F0100161Component', () => {
  let component: F0100161Component;
  let fixture: ComponentFixture<F0100161Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F0100161Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F0100161Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
