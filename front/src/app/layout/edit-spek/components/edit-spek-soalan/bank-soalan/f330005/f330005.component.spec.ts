import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330005Component } from './f330005.component';

describe('F330005Component', () => {
  let component: F330005Component;
  let fixture: ComponentFixture<F330005Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330005Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330005Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
