import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F340002Component } from './f340002.component';

describe('F340002Component', () => {
  let component: F340002Component;
  let fixture: ComponentFixture<F340002Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F340002Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F340002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
