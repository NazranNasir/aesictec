import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100431Component } from './f3100431.component';

describe('F3100431Component', () => {
  let component: F3100431Component;
  let fixture: ComponentFixture<F3100431Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100431Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100431Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
