import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330032Component } from './f330032.component';

describe('F330032Component', () => {
  let component: F330032Component;
  let fixture: ComponentFixture<F330032Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330032Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330032Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
