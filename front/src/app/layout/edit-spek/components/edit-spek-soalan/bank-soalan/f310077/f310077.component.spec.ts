import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310077Component } from './f310077.component';

describe('F310077Component', () => {
  let component: F310077Component;
  let fixture: ComponentFixture<F310077Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310077Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310077Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
