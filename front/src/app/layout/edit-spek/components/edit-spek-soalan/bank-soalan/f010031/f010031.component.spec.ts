import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010031Component } from './f010031.component';

describe('F010031Component', () => {
  let component: F010031Component;
  let fixture: ComponentFixture<F010031Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010031Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010031Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
