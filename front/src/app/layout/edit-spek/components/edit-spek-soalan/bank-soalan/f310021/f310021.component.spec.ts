import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310021Component } from './f310021.component';

describe('F310021Component', () => {
  let component: F310021Component;
  let fixture: ComponentFixture<F310021Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310021Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310021Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
