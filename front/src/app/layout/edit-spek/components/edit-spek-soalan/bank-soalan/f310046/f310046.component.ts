import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310046',
  templateUrl: './f310046.component.html',
  styleUrls: ['./f310046.component.css', '../bank-soalan.component.css']
})
export class F310046Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string, isOther: boolean}> = [
    { id:1, fnum:'310046', text: '(a) Melalui kemudahan pesanan dalam talian di laman web pembekal', isOther: false },
    { id:2, fnum:'310047', text: '(b) Melalui laman web lain (cth.: pasaran dalam talian, platform e-Dagang, tapak ejen dll.)', isOther: false },
    { id:3, fnum:'310048', text: '(c) Rangkaian persendirian yang ditetapkan (cth.: extranet, EDI dll.)', isOther: false },
    { id:4, fnum:'310049', text: '(d) Lain-lain (sila nyatakan)', isOther: true }
  ];
  constructor() { }

  ngOnInit() {
  }

}
