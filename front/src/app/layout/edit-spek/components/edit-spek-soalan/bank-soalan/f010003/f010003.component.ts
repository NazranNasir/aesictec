import {Component, Input, OnInit} from '@angular/core';
import { TahunRujukan } from './../../../../../../models/tahun-rujukan';
import { TahunRujukanService } from './../../../../../../services/tahun-rujukan.service';

@Component({
  selector: 'app-f010003',
  templateUrl: './f010003.component.html',
  styleUrls: ['./f010003.component.css', '../bank-soalan.component.css']
})
export class F010003Component implements OnInit {

  @Input() num: string;

  tahunRujukan : TahunRujukan[];
  constructor(
    private tahunRujukanService: TahunRujukanService
  ) { }

  ngOnInit() {
    this.tahunRujukanService.getALLTAHUNRUJUKAN().subscribe(tahunRujukan => this.tahunRujukan = tahunRujukan);
  }

}
