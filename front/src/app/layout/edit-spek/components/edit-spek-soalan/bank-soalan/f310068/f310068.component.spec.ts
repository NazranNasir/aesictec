import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310068Component } from './f310068.component';

describe('F310068Component', () => {
  let component: F310068Component;
  let fixture: ComponentFixture<F310068Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310068Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310068Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
