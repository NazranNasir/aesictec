import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-f330039',
  templateUrl: './f330039.component.html',
  styleUrls: ['./f330039.component.css','../bank-soalan.component.css']
})
export class F330039Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330039', text: '(a) Tanah, bangunan & struktur lain' },
    { id:2, fnum:'330040', text: '(b) Kenderaan, loji, perisian, jentera & peralatan' },
    { id:3, fnum:'330041', text: '(c) Lain-lain' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
