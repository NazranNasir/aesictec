import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310063Component } from './f310063.component';

describe('F310063Component', () => {
  let component: F310063Component;
  let fixture: ComponentFixture<F310063Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310063Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310063Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
