import { Component, OnInit, Input } from '@angular/core';
import { Negara } from './../../../../../../models/negara';
import { NegaraService } from './../../../../../../services/negara.service';

@Component({
  selector: 'app-f310056',
  templateUrl: './f310056.component.html',
  styleUrls: ['./f310056.component.css', '../bank-soalan.component.css']
})
export class F310056Component implements OnInit {
  negara : Negara[];
  @Input() num: string;
  array: Array<{id: number, fnum:string}> = [
    { id:1, fnum:'310056' },
    { id:2, fnum:'310057' },
    { id:3, fnum:'310058' }
  ];
  array2: Array<{id: number, fnum:string}> = [
    { id:1, fnum:'310059' },
    { id:2, fnum:'310060' },
    { id:3, fnum:'310061' }
  ];
  constructor(
    private negaraService: NegaraService
  ) { }

  ngOnInit() {
    this.negaraService.getAllNegara().subscribe(negara => this.negara = negara);
  }

}
