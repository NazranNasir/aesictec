import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F0200021Component } from './f0200021.component';

describe('F0200021Component', () => {
  let component: F0200021Component;
  let fixture: ComponentFixture<F0200021Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F0200021Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F0200021Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
