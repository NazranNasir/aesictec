import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F030012Component } from './f030012.component';

describe('F030012Component', () => {
  let component: F030012Component;
  let fixture: ComponentFixture<F030012Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F030012Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F030012Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
