import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010014Component } from './f010014.component';

describe('F010014Component', () => {
  let component: F010014Component;
  let fixture: ComponentFixture<F010014Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010014Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010014Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
