import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310056Component } from './f310056.component';

describe('F310056Component', () => {
  let component: F310056Component;
  let fixture: ComponentFixture<F310056Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310056Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310056Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
