import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100761Component } from './f3100761.component';

describe('F3100761Component', () => {
  let component: F3100761Component;
  let fixture: ComponentFixture<F3100761Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100761Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100761Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
