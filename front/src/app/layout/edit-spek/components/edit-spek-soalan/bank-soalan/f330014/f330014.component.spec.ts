import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330014Component } from './f330014.component';

describe('F330014Component', () => {
  let component: F330014Component;
  let fixture: ComponentFixture<F330014Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330014Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330014Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
