import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330048Component } from './f330048.component';

describe('F330048Component', () => {
  let component: F330048Component;
  let fixture: ComponentFixture<F330048Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330048Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330048Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
