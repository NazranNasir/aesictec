import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310037Component } from './f310037.component';

describe('F310037Component', () => {
  let component: F310037Component;
  let fixture: ComponentFixture<F310037Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310037Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310037Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
