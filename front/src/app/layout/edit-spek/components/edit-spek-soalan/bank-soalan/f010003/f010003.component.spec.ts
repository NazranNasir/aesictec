import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010003Component } from './f010003.component';

describe('F010003Component', () => {
  let component: F010003Component;
  let fixture: ComponentFixture<F010003Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010003Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010003Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
