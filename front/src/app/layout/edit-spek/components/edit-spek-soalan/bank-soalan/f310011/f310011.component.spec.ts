import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310011Component } from './f310011.component';

describe('F310011Component', () => {
  let component: F310011Component;
  let fixture: ComponentFixture<F310011Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310011Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310011Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
