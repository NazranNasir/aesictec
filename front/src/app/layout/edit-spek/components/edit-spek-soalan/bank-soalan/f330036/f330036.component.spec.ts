import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330036Component } from './f330036.component';

describe('F330036Component', () => {
  let component: F330036Component;
  let fixture: ComponentFixture<F330036Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330036Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330036Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
