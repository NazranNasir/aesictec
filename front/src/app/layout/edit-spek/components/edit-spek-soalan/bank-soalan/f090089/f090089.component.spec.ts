import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F090089Component } from './f090089.component';

describe('F090089Component', () => {
  let component: F090089Component;
  let fixture: ComponentFixture<F090089Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F090089Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F090089Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
