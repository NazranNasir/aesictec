import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330019',
  templateUrl: './f330019.component.html',
  styleUrls: ['./f330019.component.css','../bank-soalan.component.css']
})
export class F330019Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, fnum2:string, text: string, hasTextInput: boolean}> = [
    { id:1, fnum:'19', fnum2:'26', text: '(a) Dana sendiri', hasTextInput: true },
    { id:2, fnum:'20', fnum2:'27', text: '(b) Syarikat Swasta', hasTextInput: true },
    { id:3, fnum:'21', fnum2:'28', text: '(c) Kerajaan', hasTextInput: true },
    { id:4, fnum:'22', fnum2:'29', text: '(d) Institusi Pengajian Tinggi', hasTextInput: true },
    { id:5, fnum:'23', fnum2:'30', text: '(e) Dana Luar Negara', hasTextInput: true },
    { id:6, fnum:'24', fnum2:'31', text: '(f) Dana Lain', hasTextInput: true },
    { id:7, fnum:'25', fnum2:'', text: '(g) Tiada', hasTextInput: false }
  ];
  constructor() { }

  ngOnInit() {
  }

}
