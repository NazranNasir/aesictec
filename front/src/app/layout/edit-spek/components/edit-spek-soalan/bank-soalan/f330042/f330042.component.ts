import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-f330042',
  templateUrl: './f330042.component.html',
  styleUrls: ['./f330042.component.css','../bank-soalan.component.css']
})
export class F330042Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330042', text: '(a) Dana sendiri' },
    { id:2, fnum:'330043', text: '(b) Syarikat swasta' },
    { id:3, fnum:'330044', text: '(c) Kerajaan' },
    { id:4, fnum:'330045', text: '(c) Institusi Pengajian Tinggi' },
    { id:5, fnum:'330046', text: '(c) Dana luar negara' },
    { id:6, fnum:'330047', text: '(c) Dana lain' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
