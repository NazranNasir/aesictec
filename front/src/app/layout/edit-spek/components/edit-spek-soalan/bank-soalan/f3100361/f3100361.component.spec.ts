import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100361Component } from './f3100361.component';

describe('F3100361Component', () => {
  let component: F3100361Component;
  let fixture: ComponentFixture<F3100361Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100361Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100361Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
