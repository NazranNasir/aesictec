import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010029Component } from './f010029.component';

describe('F010029Component', () => {
  let component: F010029Component;
  let fixture: ComponentFixture<F010029Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010029Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010029Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
