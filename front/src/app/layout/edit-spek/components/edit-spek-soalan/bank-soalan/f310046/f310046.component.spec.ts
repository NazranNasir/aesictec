import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310046Component } from './f310046.component';

describe('F310046Component', () => {
  let component: F310046Component;
  let fixture: ComponentFixture<F310046Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310046Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310046Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
