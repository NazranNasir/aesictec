import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310051',
  templateUrl: './f310051.component.html',
  styleUrls: ['./f310051.component.css', '../bank-soalan.component.css']
})
export class F310051Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, num: string, text: string, text2: string}> = [
    { id:1, fnum:'310051', num: '(a)', text: 'Perniagaan lain', text2: '<strong>B2B</strong> Perniagaan kepada Perniagaan' },
    { id:2, fnum:'310052', num: '(b)',  text: 'Perniagaan individu', text2: '<strong>B2C</strong> Perniagaan kepada Pengguna' },
    { id:3, fnum:'310053', num: '(c)', text: 'Kerajaan dan organisasi bukan perniagaan lain', text2: '<strong>B2G</strong> Perniagaan kepada Kerajaan'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
