import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankSoalanComponent } from './bank-soalan.component';

describe('BankSoalanComponent', () => {
  let component: BankSoalanComponent;
  let fixture: ComponentFixture<BankSoalanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankSoalanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankSoalanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
