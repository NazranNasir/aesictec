import { Component, OnInit } from '@angular/core';
import { Negara } from './../../../../../../models/negara';
import { NegaraService } from './../../../../../../services/negara.service';

@Component({
  selector: 'app-f030012',
  templateUrl: './f030012.component.html',
  styleUrls: ['./f030012.component.css', '../bank-soalan.component.css']
})
export class F030012Component implements OnInit {
  negara : Negara[];
  constructor(
    private negaraService: NegaraService
  ) { }

  ngOnInit() {
    this.negaraService.getAllNegara().subscribe(negara => this.negara = negara);
  }

}
