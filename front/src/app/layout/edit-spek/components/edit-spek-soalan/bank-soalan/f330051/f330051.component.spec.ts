import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330051Component } from './f330051.component';

describe('F330051Component', () => {
  let component: F330051Component;
  let fixture: ComponentFixture<F330051Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330051Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330051Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
