import {Component, OnInit, Input} from '@angular/core';


@Component({
  selector: 'app-f0100361',
  templateUrl: './f0100361.component.html',
  styleUrls: ['./f0100361.component.css','../bank-soalan.component.css']
})
export class F0100361Component implements OnInit {
  @Input() num: string;
  @Input() num2: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'37', text: '(i) Pembekalan Berkadar Standard' },
    { id:2, fnum:'38', text: '(ii) Pembekalan Berkadar Sifar' },
    { id:3, fnum:'39', text: '(iii) Pembekalan Berkadar Dikecualikan' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
