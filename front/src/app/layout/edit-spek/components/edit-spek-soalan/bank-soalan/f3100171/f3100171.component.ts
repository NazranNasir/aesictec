import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f3100171',
  templateUrl: './f3100171.component.html',
  styleUrls: ['./f3100171.component.css', '../bank-soalan.component.css']
})
export class F3100171Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'18', text: '(a) Laman web kepunyaan sendiri' },
    { id:2, fnum:'19', text: '(b) Laman web di entiti lain' },
    { id:3, fnum:'20', text: '(c) Media sosial (cth. Facebook, Twitter, YouTube)' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
