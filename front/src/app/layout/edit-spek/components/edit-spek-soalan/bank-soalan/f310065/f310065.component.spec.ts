import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310065Component } from './f310065.component';

describe('F310065Component', () => {
  let component: F310065Component;
  let fixture: ComponentFixture<F310065Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310065Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310065Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
