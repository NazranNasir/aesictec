import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f030001',
  templateUrl: './f030001.component.html',
  styleUrls: ['./f030001.component.css', '../bank-soalan.component.css']
})
export class F030001Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum: string, text: string}> = [
    { id:1, fnum: '030001', text:'(a) Modal berbayar *' },
    { id:2, fnum: '030002', text:'(b) Rizab' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
