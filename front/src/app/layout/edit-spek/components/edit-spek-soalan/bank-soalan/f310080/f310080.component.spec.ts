import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310080Component } from './f310080.component';

describe('F310080Component', () => {
  let component: F310080Component;
  let fixture: ComponentFixture<F310080Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310080Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310080Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
