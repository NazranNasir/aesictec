import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F090001Component } from './f090001.component';

describe('F090001Component', () => {
  let component: F090001Component;
  let fixture: ComponentFixture<F090001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F090001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F090001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
