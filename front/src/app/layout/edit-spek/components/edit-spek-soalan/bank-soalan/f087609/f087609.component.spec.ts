import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F087609Component } from './f087609.component';

describe('F087609Component', () => {
  let component: F087609Component;
  let fixture: ComponentFixture<F087609Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F087609Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F087609Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
