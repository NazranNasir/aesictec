import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F010002Component } from './f010002.component';

describe('F010002Component', () => {
  let component: F010002Component;
  let fixture: ComponentFixture<F010002Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F010002Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F010002Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
