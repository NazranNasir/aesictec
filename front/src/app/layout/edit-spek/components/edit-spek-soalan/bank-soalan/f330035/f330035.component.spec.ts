import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330035Component } from './f330035.component';

describe('F330035Component', () => {
  let component: F330035Component;
  let fixture: ComponentFixture<F330035Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330035Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330035Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
