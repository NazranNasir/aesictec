import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F330019Component } from './f330019.component';

describe('F330019Component', () => {
  let component: F330019Component;
  let fixture: ComponentFixture<F330019Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F330019Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F330019Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
