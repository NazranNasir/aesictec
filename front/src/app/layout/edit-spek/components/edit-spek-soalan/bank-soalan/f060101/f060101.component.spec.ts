import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F060101Component } from './f060101.component';

describe('F060101Component', () => {
  let component: F060101Component;
  let fixture: ComponentFixture<F060101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F060101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F060101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
