import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F052001Component } from './f052001.component';

describe('F052001Component', () => {
  let component: F052001Component;
  let fixture: ComponentFixture<F052001Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F052001Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F052001Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
