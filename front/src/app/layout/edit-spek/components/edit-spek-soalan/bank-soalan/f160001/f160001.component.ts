import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-f160001',
  templateUrl: './f160001.component.html',
  styleUrls: ['./f160001.component.css', '../bank-soalan.component.css']
})
export class F160001Component implements OnInit {
  @Input() num: string;
  @Input() fnum: string;
  constructor() { }

  ngOnInit() {
  }

}
