import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310068',
  templateUrl: './f310068.component.html',
  styleUrls: ['./f310068.component.css', '../bank-soalan.component.css']
})
export class F310068Component implements OnInit {
  @Input() num: string;
  @Input() num2: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310068', text: '(a) Tempatan' },
    { id:2, fnum:'310069', text: '(b) Antarabangsa' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
