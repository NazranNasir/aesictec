import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310070Component } from './f310070.component';

describe('F310070Component', () => {
  let component: F310070Component;
  let fixture: ComponentFixture<F310070Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310070Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310070Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
