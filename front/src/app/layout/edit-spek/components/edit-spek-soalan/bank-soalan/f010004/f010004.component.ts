import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f010004',
  templateUrl: './f010004.component.html',
  styleUrls: ['./f010004.component.css', '../bank-soalan.component.css']
})
export class F010004Component implements OnInit {
  @Input() num: string;

  dari;
  hingga;

  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'06', text: '(a) Operasi bermusim' },
    { id:2, fnum:'07', text: '(b) Perniagaan baru' },
    { id:3, fnum:'08', text: '(c) Pertukaran hak milik' },
    { id:4, fnum:'09', text: '(d) Perubahan tahun fiskal' },
    { id:5, fnum:'10', text: '(e) Operasi diberhentikan' },
    { id:6, fnum:'11', text: '(f) Tidak aktif untuk sementara' },
    { id:7, fnum:'12', text: '(g) Lain-lain - sila nyatakan' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
