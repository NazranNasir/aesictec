import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310065',
  templateUrl: './f310065.component.html',
  styleUrls: ['./f310065.component.css', '../bank-soalan.component.css']
})
export class F310065Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, num: string, text: string, text2: string}> = [
    { id:1, fnum:'310065', num: '(a)', text: 'Perniagaan lain', text2: '<strong>B2B</strong> Perniagaan kepada Perniagaan' },
    { id:2, fnum:'310066', num: '(b)',  text: 'Perniagaan individu', text2: '<strong>B2C</strong> Perniagaan kepada Pengguna' },
    { id:3, fnum:'310067', num: '(c)', text: 'Kerajaan dan organisasi bukan perniagaan lain', text2: '<strong>B2G</strong> Perniagaan kepada Kerajaan'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
