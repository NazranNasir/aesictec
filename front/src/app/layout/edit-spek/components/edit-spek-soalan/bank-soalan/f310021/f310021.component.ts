import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-f310021',
  templateUrl: './f310021.component.html',
  styleUrls: ['./f310021.component.css', '../bank-soalan.component.css']
})
export class F310021Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'21', text: '(a) Menghantar atau menerima e-mel' },
    { id:2, fnum:'22', text: '(b) Telefon melalui internet (cth: Skype, Facetime)' },
    { id:3, fnum:'23', text: '(c) Menghantar informasi atau pesanan dengan segera' },
    { id:4, fnum:'24', text: '(d) Mendapatkan maklumat berkenaan barangan atau perkhidmatan' },
    { id:5, fnum:'25', text: '(e) Mendapatkan maklumat dari organisasi kerajaan' },
    { id:6, fnum:'26', text: '(f) Berhubung dengan organisasi kerajaan (termasuk muat turun / permohonan borang, pembayaran secara online)' },
    { id:7, fnum:'27', text: '(g) Perbankan melalui internet' },
    { id:8, fnum:'28', text: '(h) Mengakses perkhidmatan kewangan yang lain (cth. pembelian insurans)' },
    { id:9, fnum:'29', text: '(i) Penyediaan perkhidmatan pelanggan' },
    { id:10, fnum:'30', text: '(j) Penghantaran produk secara atas talian (merujuk kepada produk yang dihantar melalui internet dalam bentuk digital contoh: laporan, perisian, permainan komputer dan perkhidmatan lain atas talian seperti perkhidmatan berkaitan komputer atau perkhidmatan maklumat)' },
    { id:11, fnum:'31', text: '(k) Pemberitahuan jawatan kosong dalaman atau luaran' },
    { id:12, fnum:'32', text: '(l) Latihan untuk kakitangan (aplikasi e-pembelajaran yang didapati atas intranet atau daripada laman web)' },
    { id:13, fnum:'33', text: '(m) Lain-lain' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
