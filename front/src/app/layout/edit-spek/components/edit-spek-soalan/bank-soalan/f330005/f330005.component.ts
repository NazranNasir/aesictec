import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-f330005',
  templateUrl: './f330005.component.html',
  styleUrls: ['./f330005.component.css', '../bank-soalan.component.css']
})
export class F330005Component implements OnInit {
  @Input() num: string;

  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'05', text: '(a) Paten' },
    { id:2, fnum:'06', text: '(b) Cap dagangan (termasuk : jenama yang didaftarkan / dilindungi)' },
    { id:3, fnum:'07', text: '(c) Hakcipta' },
    { id:4, fnum:'08', text: '(d) Lain-lain (cth. Reka bentuk perindustrian, Petunjuk geografi, Reka bentuk susun atur litar bersepadu)' },
    { id:5, fnum:'09', text: '(e) Tiada' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
