import {Component, Input, OnInit} from '@angular/core';
import {PoskodService} from '../../../../../../services/poskod.service';
import {Poskod} from '../../../../../../models/poskod';


@Component({
  selector: 'app-f010014',
  templateUrl: './f010014.component.html',
  styleUrls: ['./f010014.component.css','../bank-soalan.component.css']
})
export class F010014Component implements OnInit {
  poskod : Poskod[];

  @Input() num: string;
  constructor( private poskodService: PoskodService) { }

  ngOnInit() {this.poskodService.getALLPOSKOD().subscribe(poskod => this.poskod = poskod);
  }

}
