import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F3100011Component } from './f3100011.component';

describe('F3100011Component', () => {
  let component: F3100011Component;
  let fixture: ComponentFixture<F3100011Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F3100011Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F3100011Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
