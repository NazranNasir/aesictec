import {Component, Input, OnInit} from '@angular/core';


@Component({
  selector: 'app-f330036',
  templateUrl: './f330036.component.html',
  styleUrls: ['./f330036.component.css','../bank-soalan.component.css']
})
export class F330036Component implements OnInit {
  @Input() num: string;
  array: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330036', text: '(a) Kos buruh' },
    { id:2, fnum:'330037', text: '(b) Kos operasi' },
    { id:3, fnum:'330038', text: '(c) Kos berulang lain' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
