import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F320101Component } from './f320101.component';

describe('F320101Component', () => {
  let component: F320101Component;
  let fixture: ComponentFixture<F320101Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F320101Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F320101Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
