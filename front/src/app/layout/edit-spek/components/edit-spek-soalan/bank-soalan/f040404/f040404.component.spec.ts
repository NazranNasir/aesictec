import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F040404Component } from './f040404.component';

describe('F040404Component', () => {
  let component: F040404Component;
  let fixture: ComponentFixture<F040404Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F040404Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F040404Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
