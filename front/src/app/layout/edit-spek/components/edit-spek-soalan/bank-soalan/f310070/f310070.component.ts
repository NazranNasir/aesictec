import { Component, OnInit, Input } from '@angular/core';
import { Negara } from './../../../../../../models/negara';
import { NegaraService } from './../../../../../../services/negara.service';

@Component({
  selector: 'app-f310070',
  templateUrl: './f310070.component.html',
  styleUrls: ['./f310070.component.css', '../bank-soalan.component.css']
})
export class F310070Component implements OnInit {
  negara : Negara[];
  @Input() num: string;
  array: Array<{id: number, fnum:string}> = [
    { id:1, fnum:'310070' },
    { id:2, fnum:'310071' },
    { id:3, fnum:'310072' }
  ];
  array2: Array<{id: number, fnum:string}> = [
    { id:1, fnum:'310073' },
    { id:2, fnum:'310074' },
    { id:3, fnum:'310075' }
  ];
  constructor(
    private negaraService: NegaraService
  ) { }

  ngOnInit() {
    this.negaraService.getAllNegara().subscribe(negara => this.negara = negara);
  }

}
