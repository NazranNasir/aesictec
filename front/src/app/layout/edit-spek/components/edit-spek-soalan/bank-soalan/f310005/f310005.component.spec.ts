import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F310005Component } from './f310005.component';

describe('F310005Component', () => {
  let component: F310005Component;
  let fixture: ComponentFixture<F310005Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F310005Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F310005Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
