import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { F087615Component } from './f087615.component';

describe('F087615Component', () => {
  let component: F087615Component;
  let fixture: ComponentFixture<F087615Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ F087615Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(F087615Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
