import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanLimaComponent } from './umum-soalan-lima.component';

describe('UmumSoalanLimaComponent', () => {
  let component: UmumSoalanLimaComponent;
  let fixture: ComponentFixture<UmumSoalanLimaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanLimaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanLimaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
