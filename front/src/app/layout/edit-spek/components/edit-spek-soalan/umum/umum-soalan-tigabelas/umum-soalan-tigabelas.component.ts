import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-umum-soalan-tigabelas',
  templateUrl: './umum-soalan-tigabelas.component.html',
  styleUrls: ['./umum-soalan-tigabelas.component.css']
})
export class UmumSoalanTigabelasComponent implements OnInit {
  @Input() num: string;

  arr13_1: Array<{id: number, fnum:string, num: string, text: string}> = [
    { id:1, fnum:'330048', num: '(a)', text: 'Penyelidikan asas'},
    { id:2, fnum:'330049', num: '(b)', text: 'Perniagaan individu'},
    { id:3, fnum:'330050', num: '(c)', text: 'Kerajaan dan organisasi bukan perniagaan lain'}
    ];

  arr13_2: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330005', text: '(a) Paten' },
    { id:2, fnum:'330006', text: '(b) Cap dagangan (termasuk : jenama yang didaftarkan / dilindungi)' },
    { id:3, fnum:'330007', text: '(c) Hakcipta' },
    { id:4, fnum:'330008', text: '(d) Lain-lain (cth. Reka bentuk perindustrian, Petunjuk geografi, Reka bentuk susun atur litar bersepadu)' },
    { id:5, fnum:'330009', text: '(e) Tiada' }
  ];
  arr13_3: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330010', text: '(a) Agensi kerajaan' },
    { id:2, fnum:'330011', text: '(b) Institusi Pengajian Tinggi' },
    { id:3, fnum:'330012', text: '(c) Swasta' },
    { id:4, fnum:'330013', text: '(d) Tiada' }
  ];
  arr13_4: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330014', text: '(a) Inovasi produk' },
    { id:2, fnum:'330015', text: '(b) Inovasi proses' },
    { id:3, fnum:'330016', text: '(c) Inovasi organisasi' },
    { id:4, fnum:'330017', text: '(d) Inovasi pemasaran' },
    { id:5, fnum:'330018', text: '(e) Tiada' }
  ];
  arr13_5: Array<{id: number, fnum:string, fnum2:string, text: string, hasTextInput: boolean}> = [
    { id:1, fnum:'19', fnum2:'26', text: '(a) Dana sendiri', hasTextInput: true },
    { id:2, fnum:'20', fnum2:'27', text: '(b) Syarikat Swasta', hasTextInput: true },
    { id:3, fnum:'21', fnum2:'28', text: '(c) Kerajaan', hasTextInput: true },
    { id:4, fnum:'22', fnum2:'29', text: '(d) Institusi Pengajian Tinggi', hasTextInput: true },
    { id:5, fnum:'23', fnum2:'30', text: '(e) Dana Luar Negara', hasTextInput: true },
    { id:6, fnum:'24', fnum2:'31', text: '(f) Dana Lain', hasTextInput: true },
    { id:7, fnum:'25', fnum2:'', text: '(g) Tiada', hasTextInput: false }
  ];
  arr13_6: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330032', text: '(a) 2014' },
    { id:2, fnum:'330033', text: '(b) 2015' },
    { id:3, fnum:'330034', text: '(c) 2016' }
  ];
  arr13_8: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330036', text: '(a) Kos buruh' },
    { id:2, fnum:'330037', text: '(b) Kos operasi' },
    { id:3, fnum:'330038', text: '(c) Kos berulang lain' }
  ];
  arr13_9: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330039', text: '(a) Tanah, bangunan & struktur lain' },
    { id:2, fnum:'330040', text: '(b) Kenderaan, loji, perisian, jentera & peralatan' },
    { id:3, fnum:'330041', text: '(c) Lain-lain' }
  ];
  arr13_10: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'330042', text: '(a) Dana sendiri' },
    { id:2, fnum:'330043', text: '(b) Syarikat Swasta' },
    { id:3, fnum:'330044', text: '(c) Kerajaan' },
    { id:4, fnum:'330045', text: '(d) Institusi Pengajian Tinggi' },
    { id:5, fnum:'330046', text: '(e) Dana Luar Negara' },
    { id:6, fnum:'330047', text: '(f) Dana Lain' }
  ];


  constructor() { }

  ngOnInit() {
  }

}
