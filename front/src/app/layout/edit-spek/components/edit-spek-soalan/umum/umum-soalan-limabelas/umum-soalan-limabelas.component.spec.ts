import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanLimabelasComponent } from './umum-soalan-limabelas.component';

describe('UmumSoalanLimabelasComponent', () => {
  let component: UmumSoalanLimabelasComponent;
  let fixture: ComponentFixture<UmumSoalanLimabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanLimabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanLimabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
