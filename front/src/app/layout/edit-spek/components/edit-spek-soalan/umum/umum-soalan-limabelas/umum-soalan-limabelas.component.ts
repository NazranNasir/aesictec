import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-umum-soalan-limabelas',
  templateUrl: './umum-soalan-limabelas.component.html',
  styleUrls: ['./umum-soalan-limabelas.component.css', '../../../../edit-spek.component.css']
})
export class UmumSoalanLimabelasComponent implements OnInit {
  @Input() num: string;
  @Input() fnum: string;

  constructor() { }

  ngOnInit() {
  }

}
