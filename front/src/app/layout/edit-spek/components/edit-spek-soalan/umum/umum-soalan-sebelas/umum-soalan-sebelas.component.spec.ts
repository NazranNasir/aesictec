import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanSebelasComponent } from './umum-soalan-sebelas.component';

describe('UmumSoalanSebelasComponent', () => {
  let component: UmumSoalanSebelasComponent;
  let fixture: ComponentFixture<UmumSoalanSebelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanSebelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanSebelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
