import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanSepuluhComponent } from './umum-soalan-sepuluh.component';

describe('UmumSoalanSepuluhComponent', () => {
  let component: UmumSoalanSepuluhComponent;
  let fixture: ComponentFixture<UmumSoalanSepuluhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanSepuluhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanSepuluhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
