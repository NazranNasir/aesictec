import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanEnamComponent } from './umum-soalan-enam.component';

describe('UmumSoalanEnamComponent', () => {
  let component: UmumSoalanEnamComponent;
  let fixture: ComponentFixture<UmumSoalanEnamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanEnamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanEnamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
