import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanEnambelasComponent } from './umum-soalan-enambelas.component';

describe('UmumSoalanEnambelasComponent', () => {
  let component: UmumSoalanEnambelasComponent;
  let fixture: ComponentFixture<UmumSoalanEnambelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanEnambelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanEnambelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
