import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanTigaComponent } from './umum-soalan-tiga.component';

describe('UmumSoalanTigaComponent', () => {
  let component: UmumSoalanTigaComponent;
  let fixture: ComponentFixture<UmumSoalanTigaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanTigaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanTigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
