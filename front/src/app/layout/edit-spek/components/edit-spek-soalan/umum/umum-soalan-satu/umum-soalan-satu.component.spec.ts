import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanSatuComponent } from './umum-soalan-satu.component';

describe('UmumSoalanSatuComponent', () => {
  let component: UmumSoalanSatuComponent;
  let fixture: ComponentFixture<UmumSoalanSatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanSatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanSatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
