import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanLapanComponent } from './umum-soalan-lapan.component';

describe('UmumSoalanLapanComponent', () => {
  let component: UmumSoalanLapanComponent;
  let fixture: ComponentFixture<UmumSoalanLapanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanLapanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanLapanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
