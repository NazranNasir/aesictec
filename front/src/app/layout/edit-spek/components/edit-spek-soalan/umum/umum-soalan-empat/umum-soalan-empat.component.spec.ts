import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanEmpatComponent } from './umum-soalan-empat.component';

describe('UmumSoalanEmpatComponent', () => {
  let component: UmumSoalanEmpatComponent;
  let fixture: ComponentFixture<UmumSoalanEmpatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanEmpatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanEmpatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
