import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanSembilanComponent } from './umum-soalan-sembilan.component';

describe('UmumSoalanSembilanComponent', () => {
  let component: UmumSoalanSembilanComponent;
  let fixture: ComponentFixture<UmumSoalanSembilanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanSembilanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanSembilanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
