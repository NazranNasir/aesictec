import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanEmpatbelasComponent } from './umum-soalan-empatbelas.component';

describe('UmumSoalanEmpatbelasComponent', () => {
  let component: UmumSoalanEmpatbelasComponent;
  let fixture: ComponentFixture<UmumSoalanEmpatbelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanEmpatbelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanEmpatbelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
