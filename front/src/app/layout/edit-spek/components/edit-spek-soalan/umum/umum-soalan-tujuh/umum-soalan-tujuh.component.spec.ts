import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanTujuhComponent } from './umum-soalan-tujuh.component';

describe('UmumSoalanTujuhComponent', () => {
  let component: UmumSoalanTujuhComponent;
  let fixture: ComponentFixture<UmumSoalanTujuhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanTujuhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanTujuhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
