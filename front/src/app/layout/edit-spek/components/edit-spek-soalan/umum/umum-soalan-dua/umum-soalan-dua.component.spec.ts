import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanDuaComponent } from './umum-soalan-dua.component';

describe('UmumSoalanDuaComponent', () => {
  let component: UmumSoalanDuaComponent;
  let fixture: ComponentFixture<UmumSoalanDuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanDuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanDuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
