import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanTigabelasComponent } from './umum-soalan-tigabelas.component';

describe('UmumSoalanTigabelasComponent', () => {
  let component: UmumSoalanTigabelasComponent;
  let fixture: ComponentFixture<UmumSoalanTigabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanTigabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanTigabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
