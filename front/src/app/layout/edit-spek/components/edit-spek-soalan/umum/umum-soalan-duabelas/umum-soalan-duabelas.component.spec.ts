import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmumSoalanDuabelasComponent } from './umum-soalan-duabelas.component';

describe('UmumSoalanDuabelasComponent', () => {
  let component: UmumSoalanDuabelasComponent;
  let fixture: ComponentFixture<UmumSoalanDuabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmumSoalanDuabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmumSoalanDuabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
