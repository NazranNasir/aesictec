import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateFnumToFroComponent } from './date-fnum-to-fro.component';

describe('DateFnumToFroComponent', () => {
  let component: DateFnumToFroComponent;
  let fixture: ComponentFixture<DateFnumToFroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateFnumToFroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateFnumToFroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
