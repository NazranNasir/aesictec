import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumOnlyInputComponent } from './num-only-input.component';

describe('NumOnlyInputComponent', () => {
  let component: NumOnlyInputComponent;
  let fixture: ComponentFixture<NumOnlyInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumOnlyInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumOnlyInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
