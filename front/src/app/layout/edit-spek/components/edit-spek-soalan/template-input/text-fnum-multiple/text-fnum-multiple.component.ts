import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum-multiple',
  templateUrl: './text-fnum-multiple.component.html',
  styleUrls: ['./text-fnum-multiple.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumMultipleComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, field_number: string, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
