import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaPendekComponent } from './nota-pendek.component';

describe('NotaPendekComponent', () => {
  let component: NotaPendekComponent;
  let fixture: ComponentFixture<NotaPendekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotaPendekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaPendekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
