import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxFnumDynamicComponent } from './checkbox-fnum-dynamic.component';

describe('CheckboxFnumDynamicComponent', () => {
  let component: CheckboxFnumDynamicComponent;
  let fixture: ComponentFixture<CheckboxFnumDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxFnumDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxFnumDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
