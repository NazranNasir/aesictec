import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumSingleInlineDynamicComponent } from './text-fnum-single-inline-dynamic.component';

describe('TextFnumSingleInlineDynamicComponent', () => {
  let component: TextFnumSingleInlineDynamicComponent;
  let fixture: ComponentFixture<TextFnumSingleInlineDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumSingleInlineDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumSingleInlineDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
