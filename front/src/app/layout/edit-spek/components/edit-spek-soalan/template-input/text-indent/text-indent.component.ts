import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-indent',
  templateUrl: './text-indent.component.html',
  styleUrls: ['./text-indent.component.css', '../../../../edit-spek.component.css']
})
export class TextIndentComponent implements OnInit {
  @Input() text: string;
  @Input() num: string;
  constructor() { }

  ngOnInit() {
  }

}
