import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nota-pendek',
  templateUrl: './nota-pendek.component.html',
  styleUrls: ['./nota-pendek.component.css', '../../../../edit-spek.component.css']
})
export class NotaPendekComponent implements OnInit {
  @Input() text:string;
  constructor() { }

  ngOnInit() {
  }

}
