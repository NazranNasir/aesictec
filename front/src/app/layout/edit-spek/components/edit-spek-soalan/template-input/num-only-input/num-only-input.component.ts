import {Component, Input, OnInit} from '@angular/core';
import {Poskod} from '../../../../../../models/poskod';
import {PoskodService} from '../../../../../../services/poskod.service';


@Component({
  selector: 'app-num-only-input',
  templateUrl: './num-only-input.component.html',
  styleUrls: ['./num-only-input.component.css', '../../../../edit-spek.component.css']
})
export class NumOnlyInputComponent implements OnInit {
  poskod : Poskod[];

  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;
  constructor(
    private poskodService: PoskodService
  ) { }

  ngOnInit() {
    this.poskodService.getALLPOSKOD().subscribe(poskod => this.poskod = poskod);
  }

}
