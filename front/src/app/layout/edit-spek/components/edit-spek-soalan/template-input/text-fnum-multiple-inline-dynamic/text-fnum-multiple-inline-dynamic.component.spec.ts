import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumMultipleInlineDynamicComponent } from './text-fnum-multiple-inline-dynamic.component';

describe('TextFnumMultipleInlineDynamicComponent', () => {
  let component: TextFnumMultipleInlineDynamicComponent;
  let fixture: ComponentFixture<TextFnumMultipleInlineDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumMultipleInlineDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumMultipleInlineDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
