import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PejabatMultipleComponent } from './pejabat-multiple.component';

describe('PejabatMultipleComponent', () => {
  let component: PejabatMultipleComponent;
  let fixture: ComponentFixture<PejabatMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PejabatMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PejabatMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
