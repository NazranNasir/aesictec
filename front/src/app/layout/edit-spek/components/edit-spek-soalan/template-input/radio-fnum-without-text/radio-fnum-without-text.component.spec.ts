import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioFnumWithoutTextComponent } from './radio-fnum-without-text.component';

describe('RadioFnumWithoutTextComponent', () => {
  let component: RadioFnumWithoutTextComponent;
  let fixture: ComponentFixture<RadioFnumWithoutTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioFnumWithoutTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioFnumWithoutTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
