import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JumlahComponent } from './jumlah.component';

describe('JumlahComponent', () => {
  let component: JumlahComponent;
  let fixture: ComponentFixture<JumlahComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JumlahComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JumlahComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
