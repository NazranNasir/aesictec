import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum',
  templateUrl: './text-fnum.component.html',
  styleUrls: ['./text-fnum.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;
  constructor() { }

  ngOnInit() {
  }

}
