import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaMultipleComponent } from './nota-multiple.component';

describe('NotaMultipleComponent', () => {
  let component: NotaMultipleComponent;
  let fixture: ComponentFixture<NotaMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotaMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
