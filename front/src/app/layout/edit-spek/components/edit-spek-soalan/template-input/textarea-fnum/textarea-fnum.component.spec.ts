import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextareaFnumComponent } from './textarea-fnum.component';

describe('TextareaFnumComponent', () => {
  let component: TextareaFnumComponent;
  let fixture: ComponentFixture<TextareaFnumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextareaFnumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextareaFnumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
