import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-textarea-fnum',
  templateUrl: './textarea-fnum.component.html',
  styleUrls: ['./textarea-fnum.component.css', '../../../../edit-spek.component.css']
})
export class TextareaFnumComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
