import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-nota-multiple',
  templateUrl: './nota-multiple.component.html',
  styleUrls: ['./nota-multiple.component.css', '../../../../edit-spek.component.css']
})
export class NotaMultipleComponent implements OnInit {
  @Input() text: string;
  @Input() array: Array<{id: number, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
