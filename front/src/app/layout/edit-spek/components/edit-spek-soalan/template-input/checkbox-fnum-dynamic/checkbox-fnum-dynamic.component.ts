import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-checkbox-fnum-dynamic',
  templateUrl: './checkbox-fnum-dynamic.component.html',
  styleUrls: ['./checkbox-fnum-dynamic.component.css', '../../../../edit-spek.component.css']
})
export class CheckboxFnumDynamicComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, fnum: string, text: string}>;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
