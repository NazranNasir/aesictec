import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioFnumOriComponent } from './radio-fnum-ori.component';

describe('RadioFnumOriComponent', () => {
  let component: RadioFnumOriComponent;
  let fixture: ComponentFixture<RadioFnumOriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioFnumOriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioFnumOriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
