import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-jumlah',
  templateUrl: './jumlah.component.html',
  styleUrls: ['./jumlah.component.css', '../../../../edit-spek.component.css']
})
export class JumlahComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;
  constructor() { }

  ngOnInit() {
  }

}
