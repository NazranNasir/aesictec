import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumComponent } from './text-fnum.component';

describe('TextFnumComponent', () => {
  let component: TextFnumComponent;
  let fixture: ComponentFixture<TextFnumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
