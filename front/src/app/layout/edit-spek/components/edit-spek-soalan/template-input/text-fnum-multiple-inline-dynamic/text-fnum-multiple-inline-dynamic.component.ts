import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum-multiple-inline-dynamic',
  templateUrl: './text-fnum-multiple-inline-dynamic.component.html',
  styleUrls: ['./text-fnum-multiple-inline-dynamic.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumMultipleInlineDynamicComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, fnum1: string, fnum2: string, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
