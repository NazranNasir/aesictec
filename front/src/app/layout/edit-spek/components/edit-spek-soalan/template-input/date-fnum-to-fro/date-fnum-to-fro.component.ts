import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-date-fnum-to-fro',
  templateUrl: './date-fnum-to-fro.component.html',
  styleUrls: ['./date-fnum-to-fro.component.css']
})
export class DateFnumToFroComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() fnum_dari: string;
  @Input() fnum_hingga: string;
  dari;
  hingga;

  constructor() { }

  ngOnInit() {
  }

}
