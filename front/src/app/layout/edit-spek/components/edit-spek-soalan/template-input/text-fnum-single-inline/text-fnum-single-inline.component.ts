import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum-single-inline',
  templateUrl: './text-fnum-single-inline.component.html',
  styleUrls: ['./text-fnum-single-inline.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumSingleInlineComponent implements OnInit {
  @Input() num: string;
  @Input() text1: string;
  @Input() text2: string;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
