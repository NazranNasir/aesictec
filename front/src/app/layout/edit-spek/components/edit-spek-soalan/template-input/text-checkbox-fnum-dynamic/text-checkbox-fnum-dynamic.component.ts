import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-checkbox-fnum-dynamic',
  templateUrl: './text-checkbox-fnum-dynamic.component.html',
  styleUrls: ['./text-checkbox-fnum-dynamic.component.css', '../../../../edit-spek.component.css']
})
export class TextCheckboxFnumDynamicComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;
  @Input() array: Array<{id: number, fnum:string, text: string, hasTextInput: boolean}>;

  constructor() { }

  ngOnInit() {
  }

}
