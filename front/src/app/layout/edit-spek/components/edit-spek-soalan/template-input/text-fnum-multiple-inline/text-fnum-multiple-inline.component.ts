import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum-multiple-inline',
  templateUrl: './text-fnum-multiple-inline.component.html',
  styleUrls: ['./text-fnum-multiple-inline.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumMultipleInlineComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number1: string;
  @Input() field_number2: string;

  constructor() { }

  ngOnInit() {
  }

}
