import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-radio-fnum-ori',
  templateUrl: './radio-fnum-ori.component.html',
  styleUrls: ['./radio-fnum-ori.component.css', '../../../../edit-spek.component.css']
})
export class RadioFnumOriComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
