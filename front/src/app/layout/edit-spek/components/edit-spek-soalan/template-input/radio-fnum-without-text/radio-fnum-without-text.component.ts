import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-radio-fnum-without-text',
  templateUrl: './radio-fnum-without-text.component.html',
  styleUrls: ['./radio-fnum-without-text.component.css', '../../../../edit-spek.component.css']
})
export class RadioFnumWithoutTextComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() field_number: string;
  @Input() array: Array<{id: number, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
