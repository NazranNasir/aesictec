import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumMultipleInlineComponent } from './text-fnum-multiple-inline.component';

describe('TextFnumMultipleInlineComponent', () => {
  let component: TextFnumMultipleInlineComponent;
  let fixture: ComponentFixture<TextFnumMultipleInlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumMultipleInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumMultipleInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
