import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PejabatAlamatComponent } from './pejabat-alamat.component';

describe('PejabatAlamatComponent', () => {
  let component: PejabatAlamatComponent;
  let fixture: ComponentFixture<PejabatAlamatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PejabatAlamatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PejabatAlamatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
