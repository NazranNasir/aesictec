import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-checkbox-text',
  templateUrl: './checkbox-text.component.html',
  styleUrls: ['./checkbox-text.component.css']
})
export class CheckboxTextComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, fnum: string, text: string}>;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
