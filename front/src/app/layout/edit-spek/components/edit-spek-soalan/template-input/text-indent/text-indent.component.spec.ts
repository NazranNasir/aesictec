import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextIndentComponent } from './text-indent.component';

describe('TextIndentComponent', () => {
  let component: TextIndentComponent;
  let fixture: ComponentFixture<TextIndentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextIndentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextIndentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
