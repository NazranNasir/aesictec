import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumSingleInlineComponent } from './text-fnum-single-inline.component';

describe('TextFnumSingleInlineComponent', () => {
  let component: TextFnumSingleInlineComponent;
  let fixture: ComponentFixture<TextFnumSingleInlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumSingleInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumSingleInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
