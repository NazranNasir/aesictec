import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-text-fnum-single-inline-dynamic',
  templateUrl: './text-fnum-single-inline-dynamic.component.html',
  styleUrls: ['./text-fnum-single-inline-dynamic.component.css', '../../../../edit-spek.component.css']
})
export class TextFnumSingleInlineDynamicComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, num:string, fnum: string, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
