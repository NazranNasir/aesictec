import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadioFnumDynamicComponent } from './radio-fnum-dynamic.component';

describe('RadioFnumDynamicComponent', () => {
  let component: RadioFnumDynamicComponent;
  let fixture: ComponentFixture<RadioFnumDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioFnumDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioFnumDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
