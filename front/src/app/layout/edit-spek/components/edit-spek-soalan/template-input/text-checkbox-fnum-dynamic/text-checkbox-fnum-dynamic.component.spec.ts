import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextCheckboxFnumDynamicComponent } from './text-checkbox-fnum-dynamic.component';

describe('TextCheckboxFnumDynamicComponent', () => {
  let component: TextCheckboxFnumDynamicComponent;
  let fixture: ComponentFixture<TextCheckboxFnumDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextCheckboxFnumDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextCheckboxFnumDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
