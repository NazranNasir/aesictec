import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pejabat-multiple',
  templateUrl: './pejabat-multiple.component.html',
  styleUrls: ['./pejabat-multiple.component.css', '../../../../edit-spek.component.css']
})
export class PejabatMultipleComponent implements OnInit {
  @Input() array: Array<{id: number, fnum: string, text: string}>;

  constructor() { }

  ngOnInit() {
  }

}
