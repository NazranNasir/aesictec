import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pejabat-alamat',
  templateUrl: './pejabat-alamat.component.html',
  styleUrls: ['./pejabat-alamat.component.css', '../../../../edit-spek.component.css']
})
export class PejabatAlamatComponent implements OnInit {
    @Input() fnum_kilang_negeri: string;
    @Input() fnum_kilang_pentadbiran: string;
    @Input() fnum_kilang_banci: string;
    @Input() fnum_kilang_nobp: string;
    @Input() fnum_kilang_strata: string;
    @Input() fnum_pos_negeri: string;
    @Input() fnum_pos_pentadbiran: string;
    @Input() fnum_pos_banci: string;
    @Input() fnum_pos_nobp: string;
    @Input() fnum_pos_strata: string;
    @Input() jenis_alamat: string;
  constructor() { }

  ngOnInit() {
  }

}
