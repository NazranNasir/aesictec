import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-basic-text',
  templateUrl: './basic-text.component.html',
  styleUrls: ['./basic-text.component.css']
})
export class BasicTextComponent implements OnInit {
  @Input() name: string;
  @Input() label: string;
  constructor() { }

  ngOnInit() {
  }

}
