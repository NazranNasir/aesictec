import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-radio-fnum-dynamic',
  templateUrl: './radio-fnum-dynamic.component.html',
  styleUrls: ['./radio-fnum-dynamic.component.css', '../../../../edit-spek.component.css']
})
export class RadioFnumDynamicComponent implements OnInit {
  @Input() num: string;
  @Input() text: string;
  @Input() array: Array<{id: number, text: string, last: string}>;
  @Input() field_number: string;

  constructor() { }

  ngOnInit() {
  }

}
