import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFnumMultipleComponent } from './text-fnum-multiple.component';

describe('TextFnumMultipleComponent', () => {
  let component: TextFnumMultipleComponent;
  let fixture: ComponentFixture<TextFnumMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFnumMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFnumMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
