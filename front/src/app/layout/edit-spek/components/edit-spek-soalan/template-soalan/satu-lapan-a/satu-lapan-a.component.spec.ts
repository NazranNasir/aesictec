import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuLapanAComponent } from './satu-lapan-a.component';

describe('SatuLapanAComponent', () => {
  let component: SatuLapanAComponent;
  let fixture: ComponentFixture<SatuLapanAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuLapanAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuLapanAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
