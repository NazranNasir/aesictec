import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-satu-tujuh-a',
  templateUrl: './satu-tujuh-a.component.html',
  styleUrls: ['./satu-tujuh-a.component.css']
})
export class SatuTujuhAComponent implements OnInit {
  arr: Array<{id: number, text: string, last: string}> = [
    { id:1, text: 'Pertanian, perhutanan dan perikanan', last:'1' },
    { id:2, text: 'Perlombongan dan pengkuarian', last:'2' },
    { id:3, text: 'Pembuatan', last:'3' },
    { id:4, text: 'Bekalan elektrik, gas, wap dan pendinginan udara', last:'4' },
    { id:5, text: 'Bekalan air, pembentungan, pengurusan sisa dan aktiviti pemulihan', last:'5' },
    { id:6, text: 'Pembinaan', last:'6' },
    { id:7, text: 'Pengangkutan dan penyimpanan', last:'7' },
    { id:8, text: 'Penginapan dan aktiviti perkhidmatan makanan dan minuman', last:'8' },
    { id:9, text: 'Maklumat dan komunikasi', last:'9' },
    { id:10, text: 'Aktiviti kewangan', last:'10' },
    { id:11, text: 'Aktiviti hartanah', last:'11' },
    { id:12, text: 'Aktiviti profesional', last:'12' },
    { id:13, text: 'Aktiviti pentadbiran dan khidmat sokongan', last:'13' },
    { id:14, text: 'Aktiviti perkhidmatan lain', last:'14' }
  ];
  constructor() { }

  ngOnInit() {
  }

}
