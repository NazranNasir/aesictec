import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhTujuhAComponent } from './tujuh-tujuh-a.component';

describe('TujuhTujuhAComponent', () => {
  let component: TujuhTujuhAComponent;
  let fixture: ComponentFixture<TujuhTujuhAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhTujuhAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhTujuhAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
