import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuTigaAComponent } from './satu-tiga-a.component';

describe('SatuTigaAComponent', () => {
  let component: SatuTigaAComponent;
  let fixture: ComponentFixture<SatuTigaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuTigaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuTigaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
