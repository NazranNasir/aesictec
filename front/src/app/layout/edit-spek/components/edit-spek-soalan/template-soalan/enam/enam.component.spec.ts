import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnamComponent } from './enam.component';

describe('EnamComponent', () => {
  let component: EnamComponent;
  let fixture: ComponentFixture<EnamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
