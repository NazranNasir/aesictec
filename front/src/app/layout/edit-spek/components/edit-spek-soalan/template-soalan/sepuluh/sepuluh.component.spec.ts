import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SepuluhComponent } from './sepuluh.component';

describe('SepuluhComponent', () => {
  let component: SepuluhComponent;
  let fixture: ComponentFixture<SepuluhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepuluhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SepuluhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
