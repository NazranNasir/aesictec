import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuTujuhAComponent } from './satu-tujuh-a.component';

describe('SatuTujuhAComponent', () => {
  let component: SatuTujuhAComponent;
  let fixture: ComponentFixture<SatuTujuhAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuTujuhAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuTujuhAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
