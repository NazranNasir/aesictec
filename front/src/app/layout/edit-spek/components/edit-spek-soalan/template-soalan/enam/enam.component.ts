import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-enam',
  templateUrl: './enam.component.html',
  styleUrls: ['./enam.component.css']
})
export class EnamComponent implements OnInit {
  @Input() num1: string;
  @Input() num2: string;
  @Input() num3: string;
  @Input() num4: string;
  @Input() num5: string;
  @Input() num6: string;
  @Input() num7: string;
  constructor() { }

  ngOnInit() {
  }

}
