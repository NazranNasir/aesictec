import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-enambelas-a',
  templateUrl: './enambelas-a.component.html',
  styleUrls: ['./enambelas-a.component.css']
})
export class EnambelasAComponent implements OnInit {
  @Input() num1: string;
  @Input() num2: string;
  @Input() num3: string;
  @Input() num4: string;
  @Input() num5: string;
  @Input() num6: string;
  @Input() num7: string;

  constructor() { }

  ngOnInit() {
  }

}
