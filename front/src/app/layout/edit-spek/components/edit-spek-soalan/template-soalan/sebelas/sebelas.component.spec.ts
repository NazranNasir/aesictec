import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SebelasComponent } from './sebelas.component';

describe('SebelasComponent', () => {
  let component: SebelasComponent;
  let fixture: ComponentFixture<SebelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SebelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SebelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
