import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LapanAComponent } from './lapan-a.component';

describe('LapanAComponent', () => {
  let component: LapanAComponent;
  let fixture: ComponentFixture<LapanAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LapanAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LapanAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
