import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuEmpatAComponent } from './satu-empat-a.component';

describe('SatuEmpatAComponent', () => {
  let component: SatuEmpatAComponent;
  let fixture: ComponentFixture<SatuEmpatAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuEmpatAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuEmpatAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
