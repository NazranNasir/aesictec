import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnambelasAComponent } from './enambelas-a.component';

describe('EnambelasAComponent', () => {
  let component: EnambelasAComponent;
  let fixture: ComponentFixture<EnambelasAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnambelasAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnambelasAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
