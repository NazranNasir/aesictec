import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhLimaAComponent } from './tujuh-lima-a.component';

describe('TujuhLimaAComponent', () => {
  let component: TujuhLimaAComponent;
  let fixture: ComponentFixture<TujuhLimaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhLimaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhLimaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
