import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhLapanAComponent } from './tujuh-lapan-a.component';

describe('TujuhLapanAComponent', () => {
  let component: TujuhLapanAComponent;
  let fixture: ComponentFixture<TujuhLapanAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhLapanAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhLapanAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
