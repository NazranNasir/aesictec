import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dua-dua-a',
  templateUrl: './dua-dua-a.component.html',
  styleUrls: ['./dua-dua-a.component.css']
})
export class DuaDuaAComponent implements OnInit {
  arr2_2nota: Array<{id: number, text: string}> = [
    { id:1, text: '51 peratus dan ke atas pemilikan ekuiti dipegang oleh wanita ATAU' },
    { id:2, text: 'Ketua Pegawai Eksekutif atau Pengarah Urusan adalah wanita yang memiliki sekurang-kurangnya 10 peratus ekuiti' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
