import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SembilanAComponent } from './sembilan-a.component';

describe('SembilanAComponent', () => {
  let component: SembilanAComponent;
  let fixture: ComponentFixture<SembilanAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SembilanAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SembilanAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
