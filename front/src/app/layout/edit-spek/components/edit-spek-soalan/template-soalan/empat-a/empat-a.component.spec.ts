import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpatAComponent } from './empat-a.component';

describe('EmpatAComponent', () => {
  let component: EmpatAComponent;
  let fixture: ComponentFixture<EmpatAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpatAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpatAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
