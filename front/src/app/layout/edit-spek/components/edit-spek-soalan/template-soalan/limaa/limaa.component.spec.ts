import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LimaaComponent } from './limaa.component';

describe('LimaaComponent', () => {
  let component: LimaaComponent;
  let fixture: ComponentFixture<LimaaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimaaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimaaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
