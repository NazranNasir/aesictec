import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dua-satu-a',
  templateUrl: './dua-satu-a.component.html',
  styleUrls: ['./dua-satu-a.component.css']
})
export class DuaSatuAComponent implements OnInit {
  arr2_1: Array<{id: number, last:number, text: string}> = [
    { id:1, last:1, text: '(a) Hak milik perseorangan' },
    { id:2, last:2, text: '(b) Perkongsian' },
    { id:3, last:9, text: '(c) Perkongsian liabiliti berhad' },
    { id:4, last:3, text: '(d) Syarikat sendirian berhad' },
    { id:5, last:4, text: '(e) Syarikat awam berhad' },
    { id:6, last:5, text: '(f) Syarikat koperasi' },
    { id:7, last:6, text: '(g) Perbadanan awam' },
    { id:8, last:7, text: '(h) Pertubuhan persendirian yang tidak mencari keuntungan' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
