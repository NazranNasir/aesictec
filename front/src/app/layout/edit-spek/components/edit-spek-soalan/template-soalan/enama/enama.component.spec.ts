import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnamaComponent } from './enama.component';

describe('EnamaComponent', () => {
  let component: EnamaComponent;
  let fixture: ComponentFixture<EnamaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnamaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnamaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
