import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhSatuAComponent } from './tujuh-satu-a.component';

describe('TujuhSatuAComponent', () => {
  let component: TujuhSatuAComponent;
  let fixture: ComponentFixture<TujuhSatuAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhSatuAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhSatuAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
