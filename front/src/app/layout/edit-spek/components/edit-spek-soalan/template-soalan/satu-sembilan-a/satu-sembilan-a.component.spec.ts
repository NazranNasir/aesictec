import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuSembilanAComponent } from './satu-sembilan-a.component';

describe('SatuSembilanAComponent', () => {
  let component: SatuSembilanAComponent;
  let fixture: ComponentFixture<SatuSembilanAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuSembilanAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuSembilanAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
