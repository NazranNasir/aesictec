import { Component, OnInit, Input } from '@angular/core';
import { text } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-sepuluh',
  templateUrl: './sepuluh.component.html',
  styleUrls: ['./sepuluh.component.css']
})
export class SepuluhComponent implements OnInit {
  @Input() text1: string;
  @Input() text2: string;
  @Input() field_number1: string;
  @Input() field_number2: string;
  @Input() array: Array<{id: number, num:string, text: string, fnum: string}>;
  constructor() { }

  ngOnInit() {
  }

}
