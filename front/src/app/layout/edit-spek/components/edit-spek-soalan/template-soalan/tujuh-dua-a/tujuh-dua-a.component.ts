import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tujuh-dua-a',
  templateUrl: './tujuh-dua-a.component.html',
  styleUrls: ['./tujuh-dua-a.component.css']
})
export class TujuhDuaAComponent implements OnInit {
  arr7_2: Array<{id: number, fnum1:string, fnum2:string, text: string}> = [
    { id:1, fnum1:'060299', fnum2: '061599', text: '(a) Akademik' },
    { id:2, fnum1:'060399', fnum2: '061699', text: '(b) Teknikal *' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
