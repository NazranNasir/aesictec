import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnambComponent } from './enamb.component';

describe('EnambComponent', () => {
  let component: EnambComponent;
  let fixture: ComponentFixture<EnambComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnambComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnambComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
