import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhEmpatAComponent } from './tujuh-empat-a.component';

describe('TujuhEmpatAComponent', () => {
  let component: TujuhEmpatAComponent;
  let fixture: ComponentFixture<TujuhEmpatAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhEmpatAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhEmpatAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
