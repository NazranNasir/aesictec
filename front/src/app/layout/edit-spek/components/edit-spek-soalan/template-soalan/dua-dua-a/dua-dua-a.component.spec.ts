import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuaDuaAComponent } from './dua-dua-a.component';

describe('DuaDuaAComponent', () => {
  let component: DuaDuaAComponent;
  let fixture: ComponentFixture<DuaDuaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuaDuaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuaDuaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
