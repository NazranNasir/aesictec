import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhDuaAComponent } from './tujuh-dua-a.component';

describe('TujuhDuaAComponent', () => {
  let component: TujuhDuaAComponent;
  let fixture: ComponentFixture<TujuhDuaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhDuaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhDuaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
