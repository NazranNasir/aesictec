import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhTigaAComponent } from './tujuh-tiga-a.component';

describe('TujuhTigaAComponent', () => {
  let component: TujuhTigaAComponent;
  let fixture: ComponentFixture<TujuhTigaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhTigaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhTigaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
