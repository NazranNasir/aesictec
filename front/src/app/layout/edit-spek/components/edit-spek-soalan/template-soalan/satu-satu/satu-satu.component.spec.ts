import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuSatuComponent } from './satu-satu.component';

describe('SatuSatuComponent', () => {
  let component: SatuSatuComponent;
  let fixture: ComponentFixture<SatuSatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuSatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuSatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
