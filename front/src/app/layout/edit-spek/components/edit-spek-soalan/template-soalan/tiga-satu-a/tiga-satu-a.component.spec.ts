import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TigaSatuAComponent } from './tiga-satu-a.component';

describe('TigaSatuAComponent', () => {
  let component: TigaSatuAComponent;
  let fixture: ComponentFixture<TigaSatuAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TigaSatuAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TigaSatuAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
