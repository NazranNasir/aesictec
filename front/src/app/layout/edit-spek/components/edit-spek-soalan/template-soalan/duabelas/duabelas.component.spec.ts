import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuabelasComponent } from './duabelas.component';

describe('DuabelasComponent', () => {
  let component: DuabelasComponent;
  let fixture: ComponentFixture<DuabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
