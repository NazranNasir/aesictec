import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TujuhEnamAComponent } from './tujuh-enam-a.component';

describe('TujuhEnamAComponent', () => {
  let component: TujuhEnamAComponent;
  let fixture: ComponentFixture<TujuhEnamAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TujuhEnamAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TujuhEnamAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
