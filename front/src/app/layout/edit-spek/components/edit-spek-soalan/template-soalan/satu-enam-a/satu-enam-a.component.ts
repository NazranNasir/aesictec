import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-satu-enam-a',
  templateUrl: './satu-enam-a.component.html',
  styleUrls: ['./satu-enam-a.component.css']
})
export class SatuEnamAComponent implements OnInit {
  arr1_6b: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'010037', text: '(i) Pembekalan Berkadar Standard' },
    { id:2, fnum:'010038', text: '(ii) Pembekalan Berkadar Sifar' },
    { id:3, fnum:'010039', text: '(iii) Pembekalan Dikecualikan' }
];

  constructor() { }

  ngOnInit() {
  }

}
