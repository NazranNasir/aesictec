import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LimabComponent } from './limab.component';

describe('LimabComponent', () => {
  let component: LimabComponent;
  let fixture: ComponentFixture<LimabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LimabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
