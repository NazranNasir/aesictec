import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuEnamAComponent } from './satu-enam-a.component';

describe('SatuEnamAComponent', () => {
  let component: SatuEnamAComponent;
  let fixture: ComponentFixture<SatuEnamAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuEnamAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuEnamAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
