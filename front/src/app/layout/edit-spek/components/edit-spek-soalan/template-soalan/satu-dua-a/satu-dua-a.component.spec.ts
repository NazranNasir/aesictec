import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuDuaAComponent } from './satu-dua-a.component';

describe('SatuDuaAComponent', () => {
  let component: SatuDuaAComponent;
  let fixture: ComponentFixture<SatuDuaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuDuaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuDuaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
