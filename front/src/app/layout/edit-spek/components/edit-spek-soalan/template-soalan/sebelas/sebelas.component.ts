import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sebelas',
  templateUrl: './sebelas.component.html',
  styleUrls: ['./sebelas.component.css']
})
export class SebelasComponent implements OnInit {

  arr1_11_2: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'03', text: '(a) Perubahan model perniagaan' },
    { id:2, fnum:'04', text: '(b) Impak kadar pertukaran matawang' },
    { id:3, fnum:'05', text: '(c) Perubahan harga barangan atau perkhidmatan yang dijual' },
    { id:4, fnum:'06', text: '(d) Memberi kontrak kepada pertubuhan luar' },
    { id:5, fnum:'07', text: '(e) Perubahan organisasi' },
    { id:6, fnum:'08', text: '(f) Perubahan dalam kos tenaga buruh atau bahan mentah' },
    { id:7, fnum:'09', text: '(g) Bencana Alam ' },
    { id:8, fnum:'10', text: '(h) Kemelesetan' },
    { id:9, fnum:'11', text: '(i) Pertukaran produk' },
    { id:10, fnum:'12', text: '(j) Unit perniagaan yang dijual' },
    { id:11, fnum:'13', text: '(k) Perluasan' },
    { id:12, fnum:'14', text: '(l) Kontrak baru / hilang' },
    { id:13, fnum:'15', text: '(m) Penutupan kilang' },
    { id:14, fnum:'16', text: '(n) Pengambil alihan unit-unit perniagaan' },
    { id:15, fnum:'17', text: '(o) Mogok atau sekatan' },
    { id:16, fnum:'18', text: '(p) Lain-lain (sila nayatakan)' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
