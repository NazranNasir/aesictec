import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpatBComponent } from './empat-b.component';

describe('EmpatBComponent', () => {
  let component: EmpatBComponent;
  let fixture: ComponentFixture<EmpatBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpatBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpatBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
