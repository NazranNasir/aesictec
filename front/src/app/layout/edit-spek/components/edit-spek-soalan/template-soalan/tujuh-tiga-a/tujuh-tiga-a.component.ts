import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tujuh-tiga-a',
  templateUrl: './tujuh-tiga-a.component.html',
  styleUrls: ['./tujuh-tiga-a.component.css']
})
export class TujuhTigaAComponent implements OnInit {
  arr7_3: Array<{id: number, fnum1:string, fnum2:string, text: string}> = [
    { id:1, fnum1:'060499', fnum2: '061799', text: '(a) Akademik' },
    { id:2, fnum1:'060599', fnum2: '061899', text: '(b) Teknikal dan Vokasional (TVET)' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
