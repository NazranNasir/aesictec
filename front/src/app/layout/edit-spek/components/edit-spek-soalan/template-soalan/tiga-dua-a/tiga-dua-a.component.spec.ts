import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TigaDuaAComponent } from './tiga-dua-a.component';

describe('TigaDuaAComponent', () => {
  let component: TigaDuaAComponent;
  let fixture: ComponentFixture<TigaDuaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TigaDuaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TigaDuaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
