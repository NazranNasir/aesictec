import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tiga-satu-a',
  templateUrl: './tiga-satu-a.component.html',
  styleUrls: ['./tiga-satu-a.component.css', '../../../../edit-spek.component.css']
})
export class TigaSatuAComponent implements OnInit {
  arr3_1nota: Array<{id: number, text: string}> = [
    { id:1, text: 'Residen Malaysia ialah merujuk kepada individu yang menetap dan syarikat yang beroperasi di Malaysia bagi tempoh sekurang-kurangnya satu tahun dan kepentingan ekonominya berpusat di Malaysia.' },
    { id:2, text: ' Bukan residen Malaysia merujuk kepada individu yang menetap dan syarikat yang beroperasi di luar Malaysia.' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
