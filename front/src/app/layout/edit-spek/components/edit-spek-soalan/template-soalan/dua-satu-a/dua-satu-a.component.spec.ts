import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuaSatuAComponent } from './dua-satu-a.component';

describe('DuaSatuAComponent', () => {
  let component: DuaSatuAComponent;
  let fixture: ComponentFixture<DuaSatuAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuaSatuAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuaSatuAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
