import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuLimaAComponent } from './satu-lima-a.component';

describe('SatuLimaAComponent', () => {
  let component: SatuLimaAComponent;
  let fixture: ComponentFixture<SatuLimaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuLimaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuLimaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
