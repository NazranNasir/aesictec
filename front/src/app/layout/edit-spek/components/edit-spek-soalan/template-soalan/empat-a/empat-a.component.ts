import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empat-a',
  templateUrl: './empat-a.component.html',
  styleUrls: ['./empat-a.component.css']
})
export class EmpatAComponent implements OnInit {
  arr4: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'040404', text: 'Nilai buku bersih pada 1.1.2017' },
    { id:2, fnum:'040199', text: 'Pembelian (baru)' },
    { id:3, fnum:'040299', text: 'Pembelian (terpakai)' },
    { id:3, fnum:'040399', text: 'Pelupusan' },
    { id:3, fnum:'040403', text: 'Susut nilai' },
    { id:3, fnum:'040405', text: 'Nilai buku bersih pada 31.12.2017' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
