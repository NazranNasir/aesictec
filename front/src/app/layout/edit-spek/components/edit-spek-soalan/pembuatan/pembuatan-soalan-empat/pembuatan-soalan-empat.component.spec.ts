import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanEmpatComponent } from './pembuatan-soalan-empat.component';

describe('PembuatanSoalanEmpatComponent', () => {
  let component: PembuatanSoalanEmpatComponent;
  let fixture: ComponentFixture<PembuatanSoalanEmpatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanEmpatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanEmpatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
