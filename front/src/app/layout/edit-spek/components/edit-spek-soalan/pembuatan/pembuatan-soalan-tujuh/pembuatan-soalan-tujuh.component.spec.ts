import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanTujuhComponent } from './pembuatan-soalan-tujuh.component';

describe('PembuatanSoalanTujuhComponent', () => {
  let component: PembuatanSoalanTujuhComponent;
  let fixture: ComponentFixture<PembuatanSoalanTujuhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanTujuhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanTujuhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
