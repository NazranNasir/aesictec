import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanDuabelasComponent } from './pembuatan-soalan-duabelas.component';

describe('PembuatanSoalanDuabelasComponent', () => {
  let component: PembuatanSoalanDuabelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanDuabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanDuabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanDuabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
