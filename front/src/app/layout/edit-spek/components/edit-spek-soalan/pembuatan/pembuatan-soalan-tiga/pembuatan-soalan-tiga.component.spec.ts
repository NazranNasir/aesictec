import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanTigaComponent } from './pembuatan-soalan-tiga.component';

describe('PembuatanSoalanTigaComponent', () => {
  let component: PembuatanSoalanTigaComponent;
  let fixture: ComponentFixture<PembuatanSoalanTigaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanTigaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanTigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
