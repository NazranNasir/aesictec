import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanLimabelasComponent } from './pembuatan-soalan-limabelas.component';

describe('PembuatanSoalanLimabelasComponent', () => {
  let component: PembuatanSoalanLimabelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanLimabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanLimabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanLimabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
