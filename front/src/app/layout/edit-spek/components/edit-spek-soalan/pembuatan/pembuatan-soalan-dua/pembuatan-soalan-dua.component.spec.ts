import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanDuaComponent } from './pembuatan-soalan-dua.component';

describe('PembuatanSoalanDuaComponent', () => {
  let component: PembuatanSoalanDuaComponent;
  let fixture: ComponentFixture<PembuatanSoalanDuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanDuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanDuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
