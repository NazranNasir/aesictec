import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanLimaBComponent } from './pembuatan-soalan-lima-b.component';

describe('PembuatanSoalanLimaBComponent', () => {
  let component: PembuatanSoalanLimaBComponent;
  let fixture: ComponentFixture<PembuatanSoalanLimaBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanLimaBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanLimaBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
