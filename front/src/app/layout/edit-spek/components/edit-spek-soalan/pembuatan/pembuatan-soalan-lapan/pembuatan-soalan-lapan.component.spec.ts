import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanLapanComponent } from './pembuatan-soalan-lapan.component';

describe('PembuatanSoalanLapanComponent', () => {
  let component: PembuatanSoalanLapanComponent;
  let fixture: ComponentFixture<PembuatanSoalanLapanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanLapanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanLapanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
