import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanSepuluhComponent } from './pembuatan-soalan-sepuluh.component';

describe('PembuatanSoalanSepuluhComponent', () => {
  let component: PembuatanSoalanSepuluhComponent;
  let fixture: ComponentFixture<PembuatanSoalanSepuluhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanSepuluhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanSepuluhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
