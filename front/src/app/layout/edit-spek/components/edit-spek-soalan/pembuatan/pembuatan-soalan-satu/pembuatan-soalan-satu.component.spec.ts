import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanSatuComponent } from './pembuatan-soalan-satu.component';

describe('PembuatanSoalanSatuComponent', () => {
  let component: PembuatanSoalanSatuComponent;
  let fixture: ComponentFixture<PembuatanSoalanSatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanSatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanSatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
