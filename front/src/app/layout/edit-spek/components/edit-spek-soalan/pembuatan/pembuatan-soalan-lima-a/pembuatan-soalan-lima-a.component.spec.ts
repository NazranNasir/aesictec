import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanLimaAComponent } from './pembuatan-soalan-lima-a.component';

describe('PembuatanSoalanLimaAComponent', () => {
  let component: PembuatanSoalanLimaAComponent;
  let fixture: ComponentFixture<PembuatanSoalanLimaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanLimaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanLimaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
