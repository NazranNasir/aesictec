import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanEmpatbelasComponent } from './pembuatan-soalan-empatbelas.component';

describe('PembuatanSoalanEmpatbelasComponent', () => {
  let component: PembuatanSoalanEmpatbelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanEmpatbelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanEmpatbelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanEmpatbelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
