import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanTigabelasComponent } from './pembuatan-soalan-tigabelas.component';

describe('PembuatanSoalanTigabelasComponent', () => {
  let component: PembuatanSoalanTigabelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanTigabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanTigabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanTigabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
