import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanEnamAComponent } from './pembuatan-soalan-enam-a.component';

describe('PembuatanSoalanEnamAComponent', () => {
  let component: PembuatanSoalanEnamAComponent;
  let fixture: ComponentFixture<PembuatanSoalanEnamAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanEnamAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanEnamAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
