import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanSebelasComponent } from './pembuatan-soalan-sebelas.component';

describe('PembuatanSoalanSebelasComponent', () => {
  let component: PembuatanSoalanSebelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanSebelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanSebelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanSebelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
