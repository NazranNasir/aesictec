import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanEnambelasComponent } from './pembuatan-soalan-enambelas.component';

describe('PembuatanSoalanEnambelasComponent', () => {
  let component: PembuatanSoalanEnambelasComponent;
  let fixture: ComponentFixture<PembuatanSoalanEnambelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanEnambelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanEnambelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
