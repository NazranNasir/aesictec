import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanEnamBComponent } from './pembuatan-soalan-enam-b.component';

describe('PembuatanSoalanEnamBComponent', () => {
  let component: PembuatanSoalanEnamBComponent;
  let fixture: ComponentFixture<PembuatanSoalanEnamBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanEnamBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanEnamBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
