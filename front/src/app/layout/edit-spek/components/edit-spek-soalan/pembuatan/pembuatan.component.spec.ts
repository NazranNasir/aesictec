import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanComponent } from './pembuatan.component';

describe('PembuatanComponent', () => {
  let component: PembuatanComponent;
  let fixture: ComponentFixture<PembuatanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
