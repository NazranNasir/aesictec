import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PembuatanSoalanSembilanComponent } from './pembuatan-soalan-sembilan.component';

describe('PembuatanSoalanSembilanComponent', () => {
  let component: PembuatanSoalanSembilanComponent;
  let fixture: ComponentFixture<PembuatanSoalanSembilanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembuatanSoalanSembilanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PembuatanSoalanSembilanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
