import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IctecSoalanEmpatAComponent } from './ictec-soalan-empat-a.component';

describe('IctecSoalanEmpatAComponent', () => {
  let component: IctecSoalanEmpatAComponent;
  let fixture: ComponentFixture<IctecSoalanEmpatAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IctecSoalanEmpatAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IctecSoalanEmpatAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
