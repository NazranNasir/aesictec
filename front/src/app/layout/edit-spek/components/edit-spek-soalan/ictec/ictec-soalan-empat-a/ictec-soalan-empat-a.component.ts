import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-ictec-soalan-empat-a',
  templateUrl: './ictec-soalan-empat-a.component.html',
  styleUrls: ['./ictec-soalan-empat-a.component.css']
})
export class IctecSoalanEmpatAComponent implements OnInit {
@Input() num: string;

  constructor() { }

  ngOnInit() {
  }

}
