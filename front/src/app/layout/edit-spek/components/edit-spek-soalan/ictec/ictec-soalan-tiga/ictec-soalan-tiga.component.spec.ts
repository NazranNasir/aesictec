import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IctecSoalanTigaComponent } from './ictec-soalan-tiga.component';

describe('IctecSoalanTigaComponent', () => {
  let component: IctecSoalanTigaComponent;
  let fixture: ComponentFixture<IctecSoalanTigaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IctecSoalanTigaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IctecSoalanTigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
