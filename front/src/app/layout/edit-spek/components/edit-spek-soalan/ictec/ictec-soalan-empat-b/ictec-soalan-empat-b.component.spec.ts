import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IctecSoalanEmpatBComponent } from './ictec-soalan-empat-b.component';

describe('IctecSoalanEmpatBComponent', () => {
  let component: IctecSoalanEmpatBComponent;
  let fixture: ComponentFixture<IctecSoalanEmpatBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IctecSoalanEmpatBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IctecSoalanEmpatBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
