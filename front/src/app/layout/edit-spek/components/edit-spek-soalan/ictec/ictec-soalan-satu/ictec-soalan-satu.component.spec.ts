import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IctecSoalanSatuComponent } from './ictec-soalan-satu.component';

describe('IctecSoalanSatuComponent', () => {
  let component: IctecSoalanSatuComponent;
  let fixture: ComponentFixture<IctecSoalanSatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IctecSoalanSatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IctecSoalanSatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
