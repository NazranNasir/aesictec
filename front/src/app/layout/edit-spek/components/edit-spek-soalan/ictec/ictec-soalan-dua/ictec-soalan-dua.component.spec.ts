import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IctecSoalanDuaComponent } from './ictec-soalan-dua.component';

describe('IctecSoalanDuaComponent', () => {
  let component: IctecSoalanDuaComponent;
  let fixture: ComponentFixture<IctecSoalanDuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IctecSoalanDuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IctecSoalanDuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
