import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanTujuhComponent } from './mk-soalan-tujuh.component';

describe('MkSoalanTujuhComponent', () => {
  let component: MkSoalanTujuhComponent;
  let fixture: ComponentFixture<MkSoalanTujuhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanTujuhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanTujuhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
