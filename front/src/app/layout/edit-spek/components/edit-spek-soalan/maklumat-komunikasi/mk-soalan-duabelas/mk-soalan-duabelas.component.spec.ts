import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanDuabelasComponent } from './mk-soalan-duabelas.component';

describe('MkSoalanDuabelasComponent', () => {
  let component: MkSoalanDuabelasComponent;
  let fixture: ComponentFixture<MkSoalanDuabelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanDuabelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanDuabelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
