import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanSepuluhComponent } from './mk-soalan-sepuluh.component';

describe('MkSoalanSepuluhComponent', () => {
  let component: MkSoalanSepuluhComponent;
  let fixture: ComponentFixture<MkSoalanSepuluhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanSepuluhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanSepuluhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
