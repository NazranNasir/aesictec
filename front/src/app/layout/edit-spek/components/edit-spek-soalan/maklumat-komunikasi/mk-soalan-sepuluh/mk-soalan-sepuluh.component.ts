import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-sepuluh',
  templateUrl: './mk-soalan-sepuluh.component.html',
  styleUrls: ['./mk-soalan-sepuluh.component.css']
})
export class MkSoalanSepuluhComponent implements OnInit {
  arr10: Array<{id: number, num:string, text: string, fnum: string}> = [
    { id:1, num:'(a)', text:'Bahan pembakar dan pelincir', fnum: '01' },
    { id:2, num:'(b)', text:'Bahan-bahan, bekalan dan alat ganti', fnum: '12' },
    { id:3, num:'(c)', text:'Stok perdagangan', fnum: '04' },
    { id:4, num:'(d)', text:'JUMLAH (a, b dan c)', fnum: '99' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
