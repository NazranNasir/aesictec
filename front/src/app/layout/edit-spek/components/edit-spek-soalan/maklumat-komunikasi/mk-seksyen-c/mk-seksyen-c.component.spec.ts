import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSeksyenCComponent } from './mk-seksyen-c.component';

describe('MkSeksyenCComponent', () => {
  let component: MkSeksyenCComponent;
  let fixture: ComponentFixture<MkSeksyenCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSeksyenCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSeksyenCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
