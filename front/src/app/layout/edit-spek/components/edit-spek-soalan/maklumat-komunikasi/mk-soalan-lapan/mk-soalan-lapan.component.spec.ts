import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLapanComponent } from './mk-soalan-lapan.component';

describe('MkSoalanLapanComponent', () => {
  let component: MkSoalanLapanComponent;
  let fixture: ComponentFixture<MkSoalanLapanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLapanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLapanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
