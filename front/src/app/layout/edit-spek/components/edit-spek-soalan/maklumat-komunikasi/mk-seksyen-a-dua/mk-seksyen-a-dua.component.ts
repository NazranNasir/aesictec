import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-seksyen-a-dua',
  templateUrl: './mk-seksyen-a-dua.component.html',
  styleUrls: ['./mk-seksyen-a-dua.component.css', '../../../../edit-spek.component.css']
})
export class MkSeksyenADuaComponent implements OnInit {

  constructor() { }

  arr4_13: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310037', text: '(a) Media sosial (cth: Facebook, Instagram dan blog)' },
    { id:2, fnum:'310038', text: '(b) Laman web kepunyaan sendiri' },
    { id:3, fnum:'310039', text: '(c) Pasaran e-Dagang atas talian (cth: Lazada, Zalora , 11street, Alibaba)' },
    { id:4, fnum:'310040', text: '(d) Rangkaian persendirian yang ditetapkan (cth: extranet, EDI)' },
    { id:5, fnum:'310041', text: '(e) Aplikasi mudah alih (cth: Uber app, Lazada app, Dah makan app)' },
    { id:6, fnum:'310042', text: '(f) Perkhidmatan pembayaran elektronik (cth: Perbankan internet ( Maybank2U, Interbank Giro), Kad kredit, Kad debit, Pembayaran atas talian (iPay88, Mol Pay, Paypal dll.))' }
  ];

  arr4_20: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310056', text: '' },
    { id:2, fnum:'310057', text: '' },
    { id:3, fnum:'310058', text: '' }
  ];
  arr4_20pejabat: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310059', text: '' },
    { id:2, fnum:'310060', text: '' },
    { id:3, fnum:'310061', text: '' }
  ];
  arr4_26: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310070', text: '' },
    { id:2, fnum:'310071', text: '' },
    { id:3, fnum:'310072', text: '' }
  ];

  arr4_26pejabat: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310073', text: '' },
    { id:2, fnum:'310074', text: '' },
    { id:3, fnum:'310075', text: '' }
  ];

  ngOnInit() {
  }

}
