import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-sembilan',
  templateUrl: './mk-soalan-sembilan.component.html',
  styleUrls: ['./mk-soalan-sembilan.component.css']
})
export class MkSoalanSembilanComponent implements OnInit {
  arr9_12: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '13', text:'Dalaman' },
    { id:2, num:'(b)', fnum: '14', text:'Sumber Luaran' }
  ];
  arr9_15: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '03', text:'Bahan untuk pembaikan dan penyelenggaraan' },
    { id:2, num:'(b)', fnum: '05', text:'Alat tulis dan bekalan pejabat' },
    { id:3, num:'(c)', fnum: '57', text:'Lain-lain' }
  ];
  arr9_19: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '71', text:'Pelincir' },
    { id:2, num:'(b)', fnum: '72', text:'Minyak diesel' },
    { id:3, num:'(c)', fnum: '73', text:'Petrol' },
    { id:4, num:'(d)', fnum: '74', text:'Gas petroleum cecair' },
    { id:5, num:'(e)', fnum: '75', text:'Gas asli / Gas asli untuk kereta (NGV)' },
    { id:6, num:'(f)', fnum: '76', text:'Bahan pembakar lain (sila nyatakan)' }
  ];
  arr9_27: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '62', text:'Duti hiburan' },
    { id:2, num:'(b)', fnum: '31', text:'Cukai jalan' },
    { id:3, num:'(c)', fnum: '32', text:'Bayaran pendaftaran perniagaan, lesen memandu, duti setem dsb.' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
