import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-tiga',
  templateUrl: './mk-soalan-tiga.component.html',
  styleUrls: ['./mk-soalan-tiga.component.css', '../../../../edit-spek.component.css']
})
export class MkSoalanTigaComponent implements OnInit {
  arr3_1: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '030001', text:'Modal berbayar *' },
    { id:2, num:'(b)', fnum: '030002', text:'Rizab' }
  ];
  arr3_2nota: Array<{id: number, text: string}> = [
    { id:1, text: 'Residen Malaysia ialah merujuk kepada individu yang menetap dan syarikat yang beroperasi di Malaysia bagi tempoh sekurang-kurangnya satu tahun dan kepentingan ekonominya berpusat di Malaysia.' },
    { id:2, text: ' Bukan residen Malaysia merujuk kepada individu yang menetap dan syarikat yang beroperasi di luar Malaysia.' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
