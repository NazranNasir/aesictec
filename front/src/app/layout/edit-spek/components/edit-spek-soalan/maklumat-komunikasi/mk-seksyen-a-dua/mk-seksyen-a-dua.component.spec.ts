import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSeksyenADuaComponent } from './mk-seksyen-a-dua.component';

describe('MkSeksyenADuaComponent', () => {
  let component: MkSeksyenADuaComponent;
  let fixture: ComponentFixture<MkSeksyenADuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSeksyenADuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSeksyenADuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
