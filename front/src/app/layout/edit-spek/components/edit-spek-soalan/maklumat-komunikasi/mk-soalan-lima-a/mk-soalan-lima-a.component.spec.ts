import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLimaAComponent } from './mk-soalan-lima-a.component';

describe('MkSoalanLimaAComponent', () => {
  let component: MkSoalanLimaAComponent;
  let fixture: ComponentFixture<MkSoalanLimaAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLimaAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLimaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
