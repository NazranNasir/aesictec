import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanTigaComponent } from './mk-soalan-tiga.component';

describe('MkSoalanTigaComponent', () => {
  let component: MkSoalanTigaComponent;
  let fixture: ComponentFixture<MkSoalanTigaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanTigaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanTigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
