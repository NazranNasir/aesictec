import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLapanSatuCComponent } from './mk-soalan-lapan-satu-c.component';

describe('MkSoalanLapanSatuCComponent', () => {
  let component: MkSoalanLapanSatuCComponent;
  let fixture: ComponentFixture<MkSoalanLapanSatuCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLapanSatuCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLapanSatuCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
