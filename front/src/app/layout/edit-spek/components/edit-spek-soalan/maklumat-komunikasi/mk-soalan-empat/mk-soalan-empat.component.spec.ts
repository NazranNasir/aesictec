import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanEmpatComponent } from './mk-soalan-empat.component';

describe('MkSoalanEmpatComponent', () => {
  let component: MkSoalanEmpatComponent;
  let fixture: ComponentFixture<MkSoalanEmpatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanEmpatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanEmpatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
