import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSeksyenBComponent } from './mk-seksyen-b.component';

describe('MkSeksyenBComponent', () => {
  let component: MkSeksyenBComponent;
  let fixture: ComponentFixture<MkSeksyenBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSeksyenBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSeksyenBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
