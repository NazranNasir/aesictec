import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-lapan',
  templateUrl: './mk-soalan-lapan.component.html',
  styleUrls: ['./mk-soalan-lapan.component.css']
})
export class MkSoalanLapanComponent implements OnInit {
  arr8_11: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '12', text:'Tanah' },
    { id:2, num:'(b)', fnum: '31', text:'Sewa ke atas ruangan (cth. Kios penyediaan makanan & minuman)' },
    { id:3, num:'(c)', fnum: '32', text:'Sewa peralatan telekomunikasi' },
    { id:4, num:'(d)', fnum: '13', text:'*Lain-lain' }
  ];
  arr8_21: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '087633', text:'Pengkomputeran awan' },
    { id:2, num:'(b)', fnum: '087634', text:'Perunding Data Raya' },
    { id:3, num:'(c)', fnum: '087635', text:'Pengenalan Frekuensi Radio' },
    { id:4, num:'(d)', fnum: '087636', text:'Aktiviti <i>Streaming</i>' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
