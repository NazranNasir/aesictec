import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanSebelasComponent } from './mk-soalan-sebelas.component';

describe('MkSoalanSebelasComponent', () => {
  let component: MkSoalanSebelasComponent;
  let fixture: ComponentFixture<MkSoalanSebelasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanSebelasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanSebelasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
