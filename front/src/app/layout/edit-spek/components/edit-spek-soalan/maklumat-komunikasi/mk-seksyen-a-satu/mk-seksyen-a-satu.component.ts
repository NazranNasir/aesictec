import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-seksyen-a-satu',
  templateUrl: './mk-seksyen-a-satu.component.html',
  styleUrls: ['./mk-seksyen-a-satu.component.css', '../../../../edit-spek.component.css']
})
export class MkSeksyenASatuComponent implements OnInit {
  arrA5: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310005', text: '(a) Intranet dalam perniagaan anda' },
    { id:2, fnum:'310006', text: '(b) Extranet di antara perniagaan anda dan organisasi lain (termasuk perniagaan yang berkenaan)' },
    { id:3, fnum:'310007', text: '(c) Rangkaian kawasan setempat' },
    { id:4, fnum:'310008', text: '(d) Rangkaian kawasan setempat tanpa wayar' },
    { id:5, fnum:'310009', text: '(e) Rangkaian kawasan luas' },
    { id:6, fnum:'310010', text: '(f) Tiada di atas' }
  ];
  arrA6: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310011', text: '(a) Jalur sempit' },
    { id:2, fnum:'310012', text: '(b) Jalur lebar tetap' },
    { id:3, fnum:'310013', text: '(c) Jalur lebar mudah alih (Boleh pilih lebih daripada satu)' }
  ];
  arrA8: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'310021', text: '(a) Menghantar atau menerima e-mel' },
    { id:2, fnum:'310022', text: '(b) Telefon melalui internet (cth: Skype, Facetime)' },
    { id:3, fnum:'310023', text: '(c) Menghantar informasi atau pesanan dengan segera' },
    { id:4, fnum:'310024', text: '(d) Mendapatkan maklumat berkenaan barangan atau perkhidmatan' },
    { id:5, fnum:'310025', text: '(e) Mendapatkan maklumat dari organisasi kerajaan' },
    { id:6, fnum:'310026', text: '(f) Berhubung dengan organisasi kerajaan (termasuk muat turun / permohonan borang, pembayaran secara online)' },
    { id:7, fnum:'310027', text: '(g) Perbankan melalui internet' },
    { id:8, fnum:'310028', text: '(h) Mengakses perkhidmatan kewangan yang lain (cth. pembelian insurans)' },
    { id:9, fnum:'310029', text: '(i) Penyediaan perkhidmatan pelanggan' },
    { id:10, fnum:'310030', text: '(j) Penghantaran produk secara atas talian (merujuk kepada produk yang dihantar melalui internet dalam bentuk digital contoh: laporan, perisian, permainan komputer dan perkhidmatan lain atas talian seperti perkhidmatan berkaitan komputer atau perkhidmatan maklumat)' },
    { id:11, fnum:'310031', text: '(k) Pemberitahuan jawatan kosong dalaman atau luaran' },
    { id:12, fnum:'310032', text: '(l) Latihan untuk kakitangan (aplikasi e-pembelajaran yang didapati atas intranet atau daripada laman web)' },
    { id:13, fnum:'310033', text: '(m) Lain-lain' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
