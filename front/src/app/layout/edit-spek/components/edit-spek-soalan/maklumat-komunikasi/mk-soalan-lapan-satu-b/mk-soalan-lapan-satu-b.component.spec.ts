import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLapanSatuBComponent } from './mk-soalan-lapan-satu-b.component';

describe('MkSoalanLapanSatuBComponent', () => {
  let component: MkSoalanLapanSatuBComponent;
  let fixture: ComponentFixture<MkSoalanLapanSatuBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLapanSatuBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLapanSatuBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
