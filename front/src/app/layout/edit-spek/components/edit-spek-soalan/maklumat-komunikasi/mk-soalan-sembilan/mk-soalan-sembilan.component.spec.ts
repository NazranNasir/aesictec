import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanSembilanComponent } from './mk-soalan-sembilan.component';

describe('MkSoalanSembilanComponent', () => {
  let component: MkSoalanSembilanComponent;
  let fixture: ComponentFixture<MkSoalanSembilanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanSembilanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanSembilanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
