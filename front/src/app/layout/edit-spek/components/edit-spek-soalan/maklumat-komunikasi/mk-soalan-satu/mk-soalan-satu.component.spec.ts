import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanSatuComponent } from './mk-soalan-satu.component';

describe('MkSoalanSatuComponent', () => {
  let component: MkSoalanSatuComponent;
  let fixture: ComponentFixture<MkSoalanSatuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanSatuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanSatuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
