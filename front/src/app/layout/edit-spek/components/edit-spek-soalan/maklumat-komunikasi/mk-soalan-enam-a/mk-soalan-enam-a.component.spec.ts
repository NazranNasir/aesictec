import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanEnamAComponent } from './mk-soalan-enam-a.component';

describe('MkSoalanEnamAComponent', () => {
  let component: MkSoalanEnamAComponent;
  let fixture: ComponentFixture<MkSoalanEnamAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanEnamAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanEnamAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
