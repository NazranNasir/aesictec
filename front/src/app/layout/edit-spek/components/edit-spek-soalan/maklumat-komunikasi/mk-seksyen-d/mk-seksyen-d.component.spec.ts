import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSeksyenDComponent } from './mk-seksyen-d.component';

describe('MkSeksyenDComponent', () => {
  let component: MkSeksyenDComponent;
  let fixture: ComponentFixture<MkSeksyenDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSeksyenDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSeksyenDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
