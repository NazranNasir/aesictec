import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLimaBComponent } from './mk-soalan-lima-b.component';

describe('MkSoalanLimaBComponent', () => {
  let component: MkSoalanLimaBComponent;
  let fixture: ComponentFixture<MkSoalanLimaBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLimaBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLimaBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
