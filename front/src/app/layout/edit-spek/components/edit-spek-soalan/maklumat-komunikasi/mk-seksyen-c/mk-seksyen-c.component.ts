import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-seksyen-c',
  templateUrl: './mk-seksyen-c.component.html',
  styleUrls: ['./mk-seksyen-c.component.css']
})
export class MkSeksyenCComponent implements OnInit {

  arrC_1: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'01', text: '(a) Tempatan (cth. Amalan Peningkatan Kualiti (APK), Pensijilan Jenama Malaysia, Skim Organik Malaysia(SOM) dsb.)' },
    { id:2, fnum:'02', text: '(b) Antarabangsa (cth. ISO, HACCP, GMP, GVHP, 5S etc.)' },
    { id:3, fnum:'03', text: '(c) Halal ' },
    { id:4, fnum:'04', text: '(d) Tiada' }
  ];

  arrC_2: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'05', text: '(a) Paten' },
    { id:2, fnum:'06', text: '(b) Cap dagangan (termasuk: Jenama yang didaftarkan/ dilindungi)' },
    { id:3, fnum:'07', text: '(c) Hakcipta ' },
    { id:4, fnum:'08', text: '(d) Lain-lain (cth. Reka bentuk perindustrian, petunjuk geografi, reka bentuk susun atur litar bersepadu)' },
    { id:5, fnum:'09', text: '(e) Tiada' }
  ];

  arrC_3: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'10', text: '(a) Agensi kerajaan' },
    { id:2, fnum:'11', text: '(b) Institusi Pengajian Tinggi' },
    { id:3, fnum:'12', text: '(c) Swasta ' },
    { id:4, fnum:'13', text: '(d) Tiada' }
  ];

  arrC_4: Array<{id: number, fnum:string, text: string}> = [
    { id:1, fnum:'14', text: '(a) Inovasi produk' },
    { id:2, fnum:'15', text: '(b) Inovasi proses' },
    { id:3, fnum:'16', text: '(c) Inovasi Organisasi ' },
    { id:4, fnum:'17', text: '(d) Inovasi pemasaran' },
    { id:5, fnum:'18', text: '(e) Tiada' }
  ];


  constructor() { }

  ngOnInit() {
  }

}
