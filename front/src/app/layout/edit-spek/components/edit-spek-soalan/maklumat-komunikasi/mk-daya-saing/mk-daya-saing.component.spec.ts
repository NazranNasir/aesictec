import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkDayaSaingComponent } from './mk-daya-saing.component';

describe('MkDayaSaingComponent', () => {
  let component: MkDayaSaingComponent;
  let fixture: ComponentFixture<MkDayaSaingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkDayaSaingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkDayaSaingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
