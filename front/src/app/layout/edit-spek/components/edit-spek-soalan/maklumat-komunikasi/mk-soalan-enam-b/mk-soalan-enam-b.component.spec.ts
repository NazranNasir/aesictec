import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanEnamBComponent } from './mk-soalan-enam-b.component';

describe('MkSoalanEnamBComponent', () => {
  let component: MkSoalanEnamBComponent;
  let fixture: ComponentFixture<MkSoalanEnamBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanEnamBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanEnamBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
