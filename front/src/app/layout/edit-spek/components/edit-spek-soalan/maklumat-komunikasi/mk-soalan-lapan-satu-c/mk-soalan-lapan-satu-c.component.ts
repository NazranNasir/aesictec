import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-lapan-satu-c',
  templateUrl: './mk-soalan-lapan-satu-c.component.html',
  styleUrls: ['./mk-soalan-lapan-satu-c.component.css']
})
export class MkSoalanLapanSatuCComponent implements OnInit {
  arr8_1_1: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '16', text:'Aktiviti pengaturcaraan komputer (cth. perkhidmatan aplikasi reka bentuk dan pembangunan IT) ' },
    { id:2, num:'(b)', fnum: '17', text:'Perundingan komputer (cth. perkhidmatan sokongan IT)' },
    { id:3, num:'(c)', fnum: '18', text:'Aktiviti pengurusan kemudahan komputer' },
    { id:4, num:'(d)', fnum: '19', text:'Sistem keselamatan teknologi maklumat dan komunikasi (ICT)' },
    { id:5, num:'(e)', fnum: '20', text:'Aktiviti perkhidmatan teknologi maklumat lain' }
  ];

  arr8_1_2: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '21', text:'Perkhidmatan penyediaan infrastruktur hosting, perkhidmatan prosesan data dan aktiviti yang berkaitan (web hosting)' },
    { id:2, num:'(b)', fnum: '22', text:'Aktiviti prosesan data' },
    { id:3, num:'(c)', fnum: '23', text:'Web portal' },
    { id:4, num:'(d)', fnum: '24', text:'Aktiviti perkhidmatan maklumat yang lain termasuk akitiviti agensi dan sindiket berita' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
