import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-lapan-satu-d',
  templateUrl: './mk-soalan-lapan-satu-d.component.html',
  styleUrls: ['./mk-soalan-lapan-satu-d.component.css']
})
export class MkSoalanLapanSatuDComponent implements OnInit {
  arr8_1_1: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '26', text:'Penerbitan buku, brosur dan penerbitan lain' },
    { id:2, num:'(b)', fnum: '27', text:'Penerbitan senarai mel, buku telefon dan direktori lain' },
    { id:3, num:'(c)', fnum: '28', text:'Penerbitan surat khabar, jurnal dan terbitan berkala dalam bentuk cetakan atau elektronik' },
    { id:4, num:'(d)', fnum: '29', text:'Aktiviti penerbitan lain (cth. poskad, kad ucapan dsb.)' }
  ];

  arr8_1_2: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '30', text:'Aplikasi perniagaan dan aplikasi lain' },
    { id:2, num:'(b)', fnum: '31', text:'Permainan komputer untuk semua platform' },
    { id:3, num:'(c)', fnum: '32', text:'Penerbitan perisian kecuali perisian permainan' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
