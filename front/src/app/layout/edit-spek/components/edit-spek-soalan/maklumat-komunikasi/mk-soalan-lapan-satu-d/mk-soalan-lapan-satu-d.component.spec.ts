import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLapanSatuDComponent } from './mk-soalan-lapan-satu-d.component';

describe('MkSoalanLapanSatuDComponent', () => {
  let component: MkSoalanLapanSatuDComponent;
  let fixture: ComponentFixture<MkSoalanLapanSatuDComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLapanSatuDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLapanSatuDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
