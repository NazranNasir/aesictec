import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mk-soalan-lapan-satu-a',
  templateUrl: './mk-soalan-lapan-satu-a.component.html',
  styleUrls: ['./mk-soalan-lapan-satu-a.component.css']
})
export class MkSoalanLapanSatuAComponent implements OnInit {
  arr8_1_1: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '02', text:'Aktiviti pengeluaran wayang gambar, video dan program  televisyen' },
    { id:2, num:'(b)', fnum: '03', text:'Aktiviti pasca produksi wayang gambar, video dan program  televisyen' },
    { id:3, num:'(c)', fnum: '04', text:'Aktiviti pengedaran wayang gambar, video dan program televisyen' },
    { id:4, num:'(d)', fnum: '05', text:'Perkhidmatan penayangan wayang gambar' },
    { id:5, num:'(e)', fnum: '06', text:'Aktiviti rakaman bunyi dan penerbitan muzik' }
  ];

  arr8_1_2: Array<{id: number, num:string, fnum: string, text: string}> = [
    { id:1, num:'(a)', fnum: '07', text:'Penyiaran radio' },
    { id:2, num:'(b)', fnum: '08', text:'Aktiviti pemprograman dan penyiaran televisyen' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
