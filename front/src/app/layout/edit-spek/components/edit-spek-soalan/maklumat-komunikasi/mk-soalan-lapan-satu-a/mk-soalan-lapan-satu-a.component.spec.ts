import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanLapanSatuAComponent } from './mk-soalan-lapan-satu-a.component';

describe('MkSoalanLapanSatuAComponent', () => {
  let component: MkSoalanLapanSatuAComponent;
  let fixture: ComponentFixture<MkSoalanLapanSatuAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanLapanSatuAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanLapanSatuAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
