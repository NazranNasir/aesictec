import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MkSoalanDuaComponent } from './mk-soalan-dua.component';

describe('MkSoalanDuaComponent', () => {
  let component: MkSoalanDuaComponent;
  let fixture: ComponentFixture<MkSoalanDuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MkSoalanDuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MkSoalanDuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
