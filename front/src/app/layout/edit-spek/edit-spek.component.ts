import { Component, OnInit, Input } from '@angular/core';
import {ActivatedRoute } from '@angular/router';

import { KodPenyiasatan } from './../../models/kod-penyiasatan';
import { KodPenyiasatanService } from './../../services/kod-penyiasatan.service';
import { NoSiri } from './../../models/no-siri';
import { NoSiriService } from './../../services/no-siri.service';
import { TahunRujukan } from './../../models/tahun-rujukan';
import { TahunRujukanService } from './../../services/tahun-rujukan.service';

@Component({
  selector: 'app-edit-spek',
  templateUrl: './edit-spek.component.html',
  styleUrls: ['./edit-spek.component.css']
})
export class EditSpekComponent implements OnInit {
  @Input() kodPenyiasatan: KodPenyiasatan;
  @Input() noSiri: NoSiri;
  @Input() tahunRujukan: TahunRujukan;
  borang: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private kodPenyiasatanService: KodPenyiasatanService,
    private noSiriService: NoSiriService,
    private tahunRujukanService: TahunRujukanService
  ) { }

  ngOnInit() {
    const id =  +this.activatedRoute.snapshot.paramMap.get('id');
    const nosiri =  +this.activatedRoute.snapshot.paramMap.get('nosiri');
    const tahun =  +this.activatedRoute.snapshot.paramMap.get('tahun');
    this.borang =  +this.activatedRoute.snapshot.paramMap.get('borang');
    this.kodPenyiasatanService.getKP(id).subscribe(kodPenyiasatan => this.kodPenyiasatan = kodPenyiasatan);
    this.noSiriService.getNOSIRI(nosiri).subscribe(noSiri => this.noSiri = noSiri);
    this.tahunRujukanService.getTAHUNRUJUKAN(tahun).subscribe(tahunRujukan => this.tahunRujukan = tahunRujukan);
  }

}
