import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EditSpekRoutingModule } from './edit-spek-routing.module';
import { EditSpekComponent } from './edit-spek.component';
import { EditSpekHeaderComponent } from './components/edit-spek-header/edit-spek-header.component';
import { EditSpekSoalanComponent } from './components/edit-spek-soalan/edit-spek-soalan.component';
import { PengakuanComponent } from './components/edit-spek-soalan/pengakuan/pengakuan.component';
import { IctecSoalanSatuComponent } from './components/edit-spek-soalan/ictec/ictec-soalan-satu/ictec-soalan-satu.component';
import { IctecSoalanDuaComponent } from './components/edit-spek-soalan/ictec/ictec-soalan-dua/ictec-soalan-dua.component';
import { IctecSoalanTigaComponent } from './components/edit-spek-soalan/ictec/ictec-soalan-tiga/ictec-soalan-tiga.component';
import { IctecSoalanEmpatAComponent } from './components/edit-spek-soalan/ictec/ictec-soalan-empat-a/ictec-soalan-empat-a.component';
import { IctecSoalanEmpatBComponent } from './components/edit-spek-soalan/ictec/ictec-soalan-empat-b/ictec-soalan-empat-b.component';
import { BasicTextComponent } from './components/edit-spek-soalan/template-input/basic-text/basic-text.component';
import { UmumSoalanSatuComponent } from './components/edit-spek-soalan/umum/umum-soalan-satu/umum-soalan-satu.component';
import { UmumSoalanDuaComponent } from './components/edit-spek-soalan/umum/umum-soalan-dua/umum-soalan-dua.component';
import { UmumSoalanTigaComponent } from './components/edit-spek-soalan/umum/umum-soalan-tiga/umum-soalan-tiga.component';
import { UmumSoalanEmpatComponent } from './components/edit-spek-soalan/umum/umum-soalan-empat/umum-soalan-empat.component';
import { UmumSoalanLimaComponent } from './components/edit-spek-soalan/umum/umum-soalan-lima/umum-soalan-lima.component';
import { UmumSoalanEnamComponent } from './components/edit-spek-soalan/umum/umum-soalan-enam/umum-soalan-enam.component';
import { UmumSoalanTujuhComponent } from './components/edit-spek-soalan/umum/umum-soalan-tujuh/umum-soalan-tujuh.component';
import { UmumSoalanLapanComponent } from './components/edit-spek-soalan/umum/umum-soalan-lapan/umum-soalan-lapan.component';
import { UmumSoalanSembilanComponent } from './components/edit-spek-soalan/umum/umum-soalan-sembilan/umum-soalan-sembilan.component';
import { UmumSoalanSepuluhComponent } from './components/edit-spek-soalan/umum/umum-soalan-sepuluh/umum-soalan-sepuluh.component';
import { UmumSoalanSebelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-sebelas/umum-soalan-sebelas.component';
import { UmumSoalanDuabelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-duabelas/umum-soalan-duabelas.component';
import { UmumSoalanTigabelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-tigabelas/umum-soalan-tigabelas.component';
import { UmumSoalanEmpatbelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-empatbelas/umum-soalan-empatbelas.component';
import { UmumSoalanLimabelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-limabelas/umum-soalan-limabelas.component';
import { UmumSoalanEnambelasComponent } from './components/edit-spek-soalan/umum/umum-soalan-enambelas/umum-soalan-enambelas.component';
import { SatuSatuComponent } from './components/edit-spek-soalan/template-soalan/satu-satu/satu-satu.component';
import { TextIndentComponent } from './components/edit-spek-soalan/template-input/text-indent/text-indent.component';
import { PejabatComponent } from './components/edit-spek-soalan/template-input/pejabat/pejabat.component';
import { SatuDuaAComponent } from './components/edit-spek-soalan/template-soalan/satu-dua-a/satu-dua-a.component';
import { TextFnumComponent } from './components/edit-spek-soalan/template-input/text-fnum/text-fnum.component';
import { SatuTigaAComponent } from './components/edit-spek-soalan/template-soalan/satu-tiga-a/satu-tiga-a.component';
import { TextareaFnumComponent } from './components/edit-spek-soalan/template-input/textarea-fnum/textarea-fnum.component';
import { RadioFnumOriComponent } from './components/edit-spek-soalan/template-input/radio-fnum-ori/radio-fnum-ori.component';
import { SatuEmpatAComponent } from './components/edit-spek-soalan/template-soalan/satu-empat-a/satu-empat-a.component';
import { SatuLimaAComponent } from './components/edit-spek-soalan/template-soalan/satu-lima-a/satu-lima-a.component';
import { SatuEnamAComponent } from './components/edit-spek-soalan/template-soalan/satu-enam-a/satu-enam-a.component';
import { SatuTujuhAComponent } from './components/edit-spek-soalan/template-soalan/satu-tujuh-a/satu-tujuh-a.component';
import { SatuLapanAComponent } from './components/edit-spek-soalan/template-soalan/satu-lapan-a/satu-lapan-a.component';
import { SatuSembilanAComponent } from './components/edit-spek-soalan/template-soalan/satu-sembilan-a/satu-sembilan-a.component';
import { DuaSatuAComponent } from './components/edit-spek-soalan/template-soalan/dua-satu-a/dua-satu-a.component';
import { DuaDuaAComponent } from './components/edit-spek-soalan/template-soalan/dua-dua-a/dua-dua-a.component';
import { NotaPendekComponent } from './components/edit-spek-soalan/template-input/nota-pendek/nota-pendek.component';
import { PejabatAlamatComponent } from './components/edit-spek-soalan/template-input/pejabat-alamat/pejabat-alamat.component';
import { RadioFnumDynamicComponent } from './components/edit-spek-soalan/template-input/radio-fnum-dynamic/radio-fnum-dynamic.component';
import { PejabatMultipleComponent } from './components/edit-spek-soalan/template-input/pejabat-multiple/pejabat-multiple.component';
import { NotaMultipleComponent } from './components/edit-spek-soalan/template-input/nota-multiple/nota-multiple.component';
import { TextFnumMultipleComponent } from './components/edit-spek-soalan/template-input/text-fnum-multiple/text-fnum-multiple.component';
import { CheckboxFnumDynamicComponent } from './components/edit-spek-soalan/template-input/checkbox-fnum-dynamic/checkbox-fnum-dynamic.component';
import { TigaSatuAComponent } from './components/edit-spek-soalan/template-soalan/tiga-satu-a/tiga-satu-a.component';
import { TigaDuaAComponent } from './components/edit-spek-soalan/template-soalan/tiga-dua-a/tiga-dua-a.component';
import { EditSpekValidasiComponent } from './components/edit-spek-validasi/edit-spek-validasi.component';
import { EmpatAComponent } from './components/edit-spek-soalan/template-soalan/empat-a/empat-a.component';
import { TujuhSatuAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-satu-a/tujuh-satu-a.component';
import { TujuhDuaAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-dua-a/tujuh-dua-a.component';
import { TujuhTigaAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-tiga-a/tujuh-tiga-a.component';
import { TujuhEmpatAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-empat-a/tujuh-empat-a.component';
import { TujuhLimaAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-lima-a/tujuh-lima-a.component';
import { TujuhEnamAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-enam-a/tujuh-enam-a.component';
import { TujuhLapanAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-lapan-a/tujuh-lapan-a.component';
import { TujuhTujuhAComponent } from './components/edit-spek-soalan/template-soalan/tujuh-tujuh-a/tujuh-tujuh-a.component';
import { TextFnumMultipleInlineComponent } from './components/edit-spek-soalan/template-input/text-fnum-multiple-inline/text-fnum-multiple-inline.component';
import { TextFnumMultipleInlineDynamicComponent } from './components/edit-spek-soalan/template-input/text-fnum-multiple-inline-dynamic/text-fnum-multiple-inline-dynamic.component';
import { LapanAComponent } from './components/edit-spek-soalan/template-soalan/lapan-a/lapan-a.component';
import { SembilanAComponent } from './components/edit-spek-soalan/template-soalan/sembilan-a/sembilan-a.component';
import { MkSoalanSatuComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-satu/mk-soalan-satu.component';
import { MkSoalanDuaComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-dua/mk-soalan-dua.component';
import { MkSoalanTigaComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-tiga/mk-soalan-tiga.component';
import { MkSoalanEmpatComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-empat/mk-soalan-empat.component';
import { MkSoalanLimaAComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lima-a/mk-soalan-lima-a.component';
import { MkSoalanLimaBComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lima-b/mk-soalan-lima-b.component';
import { MkSoalanEnamAComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-enam-a/mk-soalan-enam-a.component';
import { MkSoalanEnamBComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-enam-b/mk-soalan-enam-b.component';
import { MkSoalanTujuhComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-tujuh/mk-soalan-tujuh.component';
import { MkSoalanLapanSatuAComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lapan-satu-a/mk-soalan-lapan-satu-a.component';
import { MkSoalanLapanSatuBComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lapan-satu-b/mk-soalan-lapan-satu-b.component';
import { MkSoalanLapanSatuCComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lapan-satu-c/mk-soalan-lapan-satu-c.component';
import { MkSoalanLapanSatuDComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lapan-satu-d/mk-soalan-lapan-satu-d.component';
import { MkSoalanLapanComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-lapan/mk-soalan-lapan.component';
import { MkSoalanSembilanComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-sembilan/mk-soalan-sembilan.component';
import { MkSoalanSepuluhComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-sepuluh/mk-soalan-sepuluh.component';
import { MkSoalanSebelasComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-sebelas/mk-soalan-sebelas.component';
import { MkSoalanDuabelasComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-soalan-duabelas/mk-soalan-duabelas.component';
import { DateFnumToFroComponent } from './components/edit-spek-soalan/template-input/date-fnum-to-fro/date-fnum-to-fro.component';
import { EmpatBComponent } from './components/edit-spek-soalan/template-soalan/empat-b/empat-b.component';
import { LimaaComponent } from './components/edit-spek-soalan/template-soalan/limaa/limaa.component';
import { LimabComponent } from './components/edit-spek-soalan/template-soalan/limab/limab.component';
import { EnamaComponent } from './components/edit-spek-soalan/template-soalan/enama/enama.component';
import { EnambComponent } from './components/edit-spek-soalan/template-soalan/enamb/enamb.component';
import { EnamComponent } from './components/edit-spek-soalan/template-soalan/enam/enam.component';
import { DuabelasComponent } from './components/edit-spek-soalan/template-soalan/duabelas/duabelas.component';
import { TextFnumSingleInlineComponent } from './components/edit-spek-soalan/template-input/text-fnum-single-inline/text-fnum-single-inline.component';
import { TextFnumSingleInlineDynamicComponent } from './components/edit-spek-soalan/template-input/text-fnum-single-inline-dynamic/text-fnum-single-inline-dynamic.component';
import { EnambelasAComponent } from './components/edit-spek-soalan/template-soalan/enambelas-a/enambelas-a.component';
import { JumlahComponent } from './components/edit-spek-soalan/template-input/jumlah/jumlah.component';
import { TextCheckboxFnumDynamicComponent } from './components/edit-spek-soalan/template-input/text-checkbox-fnum-dynamic/text-checkbox-fnum-dynamic.component';
import { SebelasComponent } from './components/edit-spek-soalan/template-soalan/sebelas/sebelas.component';
import { SepuluhComponent } from './components/edit-spek-soalan/template-soalan/sepuluh/sepuluh.component';
import { PembuatanComponent } from './components/edit-spek-soalan/pembuatan/pembuatan.component';
import { PembuatanSoalanSatuComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-satu/pembuatan-soalan-satu.component';
import { PembuatanSoalanDuaComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-dua/pembuatan-soalan-dua.component';
import { PembuatanSoalanTigaComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-tiga/pembuatan-soalan-tiga.component';
import { PembuatanSoalanEmpatComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-empat/pembuatan-soalan-empat.component';
import { PembuatanSoalanLimaBComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-lima-b/pembuatan-soalan-lima-b.component';
import { PembuatanSoalanLimaAComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-lima-a/pembuatan-soalan-lima-a.component';
import { PembuatanSoalanEnamAComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-enam-a/pembuatan-soalan-enam-a.component';
import { PembuatanSoalanEnamBComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-enam-b/pembuatan-soalan-enam-b.component';
import { PembuatanSoalanTujuhComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-tujuh/pembuatan-soalan-tujuh.component';
import { PembuatanSoalanLapanComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-lapan/pembuatan-soalan-lapan.component';
import { PembuatanSoalanSembilanComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-sembilan/pembuatan-soalan-sembilan.component';
import { PembuatanSoalanSepuluhComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-sepuluh/pembuatan-soalan-sepuluh.component';
import { PembuatanSoalanSebelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-sebelas/pembuatan-soalan-sebelas.component';
import { PembuatanSoalanDuabelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-duabelas/pembuatan-soalan-duabelas.component';
import { PembuatanSoalanTigabelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-tigabelas/pembuatan-soalan-tigabelas.component';
import { PembuatanSoalanEmpatbelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-empatbelas/pembuatan-soalan-empatbelas.component';
import { PembuatanSoalanLimabelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-limabelas/pembuatan-soalan-limabelas.component';
import { PembuatanSoalanEnambelasComponent } from './components/edit-spek-soalan/pembuatan/pembuatan-soalan-enambelas/pembuatan-soalan-enambelas.component';
import { RadioFnumWithoutTextComponent } from './components/edit-spek-soalan/template-input/radio-fnum-without-text/radio-fnum-without-text.component';
import { MkDayaSaingComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-daya-saing/mk-daya-saing.component';
import { MkSeksyenASatuComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-seksyen-a-satu/mk-seksyen-a-satu.component';
import { MkSeksyenADuaComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-seksyen-a-dua/mk-seksyen-a-dua.component';
import { MkSeksyenBComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-seksyen-b/mk-seksyen-b.component';
import { MkSeksyenCComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-seksyen-c/mk-seksyen-c.component';
import { MkSeksyenDComponent } from './components/edit-spek-soalan/maklumat-komunikasi/mk-seksyen-d/mk-seksyen-d.component';
import { F010003Component } from './components/edit-spek-soalan/bank-soalan/f010003/f010003.component';
import { L010002Component } from './components/edit-spek-soalan/bank-soalan/l010002/l010002.component';
import { F010002Component } from './components/edit-spek-soalan/bank-soalan/f010002/f010002.component';
import { BankSoalanComponent } from './components/edit-spek-soalan/bank-soalan/bank-soalan.component';
import { F010028Component } from './components/edit-spek-soalan/bank-soalan/f010028/f010028.component';
import { F010029Component } from './components/edit-spek-soalan/bank-soalan/f010029/f010029.component';

import { CheckboxTextComponent } from './components/edit-spek-soalan/template-input/checkbox-text/checkbox-text.component';
import { F0200011Component } from './components/edit-spek-soalan/bank-soalan/f0200011/f0200011.component';
import { F0200021Component } from './components/edit-spek-soalan/bank-soalan/f0200021/f0200021.component';
import { F030001Component } from './components/edit-spek-soalan/bank-soalan/f030001/f030001.component';
import { F030003Component } from './components/edit-spek-soalan/bank-soalan/f030003/f030003.component';
import { F030013Component } from './components/edit-spek-soalan/bank-soalan/f030013/f030013.component';
import { F030012Component } from './components/edit-spek-soalan/bank-soalan/f030012/f030012.component';
import { NumOnlyInputComponent } from './components/edit-spek-soalan/template-input/num-only-input/num-only-input.component';
import { F010014Component } from './components/edit-spek-soalan/bank-soalan/f010014/f010014.component';
import { F0100161Component } from './components/edit-spek-soalan/bank-soalan/f0100161/f0100161.component';
import { F0100351Component } from './components/edit-spek-soalan/bank-soalan/f0100351/f0100351.component';
import { F0100361Component } from './components/edit-spek-soalan/bank-soalan/f0100361/f0100361.component';
import { F3100011Component } from './components/edit-spek-soalan/bank-soalan/f3100011/f3100011.component';
import { F310080Component } from './components/edit-spek-soalan/bank-soalan/f310080/f310080.component';
import { F310002Component } from './components/edit-spek-soalan/bank-soalan/f310002/f310002.component';
import { F3100031Component } from './components/edit-spek-soalan/bank-soalan/f3100031/f3100031.component';
import { F310004Component } from './components/edit-spek-soalan/bank-soalan/f310004/f310004.component';
import { F010004Component } from './components/edit-spek-soalan/bank-soalan/f010004/f010004.component';
import { F040101Component } from './components/edit-spek-soalan/bank-soalan/f040101/f040101.component';
import { F050101Component } from './components/edit-spek-soalan/bank-soalan/f050101/f050101.component';
import { F051001Component } from './components/edit-spek-soalan/bank-soalan/f051001/f051001.component';
import { F060101Component } from './components/edit-spek-soalan/bank-soalan/f060101/f060101.component';
import { F310005Component } from './components/edit-spek-soalan/bank-soalan/f310005/f310005.component';
import { F310011Component } from './components/edit-spek-soalan/bank-soalan/f310011/f310011.component';
import { F3100171Component } from './components/edit-spek-soalan/bank-soalan/f3100171/f3100171.component';
import { F310021Component } from './components/edit-spek-soalan/bank-soalan/f310021/f310021.component';
import { F3100341Component } from './components/edit-spek-soalan/bank-soalan/f3100341/f3100341.component';
import { F3100351Component } from './components/edit-spek-soalan/bank-soalan/f3100351/f3100351.component';
import { F3100361Component } from './components/edit-spek-soalan/bank-soalan/f3100361/f3100361.component';
import { F310037Component } from './components/edit-spek-soalan/bank-soalan/f310037/f310037.component';
import { F3100431Component } from './components/edit-spek-soalan/bank-soalan/f3100431/f3100431.component';
import { F080089Component } from './components/edit-spek-soalan/bank-soalan/f080089/f080089.component';
import { F310044Component } from './components/edit-spek-soalan/bank-soalan/f310044/f310044.component';
import { F061401Component } from './components/edit-spek-soalan/bank-soalan/f061401/f061401.component';
import { F070101Component } from './components/edit-spek-soalan/bank-soalan/f070101/f070101.component';
import { F087601Component } from './components/edit-spek-soalan/bank-soalan/f087601/f087601.component';
import {F087609Component} from "./components/edit-spek-soalan/bank-soalan/f087609/f087609.component";
import { F010017Component } from './components/edit-spek-soalan/bank-soalan/f010017/f010017.component';
import { F010027Component } from './components/edit-spek-soalan/bank-soalan/f010027/f010027.component';
import { F010031Component } from './components/edit-spek-soalan/bank-soalan/f010031/f010031.component';
import { F010034Component } from './components/edit-spek-soalan/bank-soalan/f010034/f010034.component';
import { F040404Component } from './components/edit-spek-soalan/bank-soalan/f040404/f040404.component';
import { F052001Component } from './components/edit-spek-soalan/bank-soalan/f052001/f052001.component';
import { F310046Component } from './components/edit-spek-soalan/bank-soalan/f310046/f310046.component';
import { F310051Component } from './components/edit-spek-soalan/bank-soalan/f310051/f310051.component';
import { F310054Component } from './components/edit-spek-soalan/bank-soalan/f310054/f310054.component';
import { F310056Component } from './components/edit-spek-soalan/bank-soalan/f310056/f310056.component';
import { F3100621Component } from './components/edit-spek-soalan/bank-soalan/f3100621/f3100621.component';
import { F090089Component } from './components/edit-spek-soalan/bank-soalan/f090089/f090089.component';
import { F310063Component } from './components/edit-spek-soalan/bank-soalan/f310063/f310063.component';
import { F310065Component } from './components/edit-spek-soalan/bank-soalan/f310065/f310065.component';
import { F310068Component } from './components/edit-spek-soalan/bank-soalan/f310068/f310068.component';
import { F310070Component } from './components/edit-spek-soalan/bank-soalan/f310070/f310070.component';
import { F3100761Component } from './components/edit-spek-soalan/bank-soalan/f3100761/f3100761.component';
import { F310077Component } from './components/edit-spek-soalan/bank-soalan/f310077/f310077.component';
import { F087615Component } from './components/edit-spek-soalan/bank-soalan/f087615/f087615.component';
import { F087625Component } from './components/edit-spek-soalan/bank-soalan/f087625/f087625.component';
import { F080023Component } from './components/edit-spek-soalan/bank-soalan/f080023/f080023.component';
import { F090016Component } from './components/edit-spek-soalan/bank-soalan/f090016/f090016.component';
import { F080001Component } from './components/edit-spek-soalan/bank-soalan/f080001/f080001.component';
import { F090001Component } from './components/edit-spek-soalan/bank-soalan/f090001/f090001.component';
import { F100101Component } from './components/edit-spek-soalan/bank-soalan/f100101/f100101.component';
import { F320101Component } from './components/edit-spek-soalan/bank-soalan/f320101/f320101.component';
import { F330001Component } from './components/edit-spek-soalan/bank-soalan/f330001/f330001.component';
import { F330005Component } from './components/edit-spek-soalan/bank-soalan/f330005/f330005.component';
import { F330010Component } from './components/edit-spek-soalan/bank-soalan/f330010/f330010.component';
import { F330014Component } from './components/edit-spek-soalan/bank-soalan/f330014/f330014.component';
import { F330019Component } from './components/edit-spek-soalan/bank-soalan/f330019/f330019.component';
import { F330032Component } from './components/edit-spek-soalan/bank-soalan/f330032/f330032.component';
import { F330035Component } from './components/edit-spek-soalan/bank-soalan/f330035/f330035.component';
import { F330036Component } from './components/edit-spek-soalan/bank-soalan/f330036/f330036.component';
import { F330039Component } from './components/edit-spek-soalan/bank-soalan/f330039/f330039.component';
import { F330042Component } from './components/edit-spek-soalan/bank-soalan/f330042/f330042.component';
import { F330048Component } from './components/edit-spek-soalan/bank-soalan/f330048/f330048.component';
import { F330051Component } from './components/edit-spek-soalan/bank-soalan/f330051/f330051.component';
import { F340001Component } from './components/edit-spek-soalan/bank-soalan/f340001/f340001.component';
import { F340002Component } from './components/edit-spek-soalan/bank-soalan/f340002/f340002.component';
import { F340003Component } from './components/edit-spek-soalan/bank-soalan/f340003/f340003.component';
import { F160001Component } from './components/edit-spek-soalan/bank-soalan/f160001/f160001.component';
import { F151001Component } from './components/edit-spek-soalan/bank-soalan/f151001/f151001.component';

import {F060199Component} from "./components/edit-spek-soalan/bank-soalan/f060199/f060199.component";
import { F010015Component } from './components/edit-spek-soalan/bank-soalan/f010015/f010015.component';
import { F010040Component } from './components/edit-spek-soalan/bank-soalan/f010040/f010040.component';

@NgModule({
  imports: [
    CommonModule,
    EditSpekRoutingModule,
    NgbModule,
    FormsModule
  ],
  declarations: [
    EditSpekComponent,
    EditSpekHeaderComponent,
    EditSpekSoalanComponent,
    PengakuanComponent,
    IctecSoalanSatuComponent,
    IctecSoalanDuaComponent,
    IctecSoalanTigaComponent,
    IctecSoalanEmpatAComponent,
    IctecSoalanEmpatBComponent,
    BasicTextComponent, UmumSoalanSatuComponent, UmumSoalanDuaComponent, UmumSoalanTigaComponent, UmumSoalanEmpatComponent, UmumSoalanLimaComponent, UmumSoalanEnamComponent, UmumSoalanTujuhComponent, UmumSoalanLapanComponent, UmumSoalanSembilanComponent, UmumSoalanSepuluhComponent, UmumSoalanSebelasComponent, UmumSoalanDuabelasComponent, UmumSoalanTigabelasComponent, UmumSoalanEmpatbelasComponent, UmumSoalanLimabelasComponent, UmumSoalanEnambelasComponent, SatuSatuComponent, TextIndentComponent, PejabatComponent, SatuDuaAComponent, TextFnumComponent, SatuTigaAComponent, TextareaFnumComponent, RadioFnumOriComponent, SatuEmpatAComponent, SatuLimaAComponent, SatuEnamAComponent, SatuTujuhAComponent, SatuLapanAComponent, SatuSembilanAComponent, DuaSatuAComponent, DuaDuaAComponent, NotaPendekComponent, PejabatAlamatComponent, RadioFnumDynamicComponent, PejabatMultipleComponent, NotaMultipleComponent, TextFnumMultipleComponent, CheckboxFnumDynamicComponent, TigaSatuAComponent, TigaDuaAComponent, EditSpekValidasiComponent, EmpatAComponent, TujuhSatuAComponent, TujuhDuaAComponent, TujuhTigaAComponent, TujuhEmpatAComponent, TujuhLimaAComponent, TujuhEnamAComponent, TujuhLapanAComponent, TujuhTujuhAComponent, TextFnumMultipleInlineComponent, TextFnumMultipleInlineDynamicComponent, LapanAComponent, SembilanAComponent, MkSoalanSatuComponent, MkSoalanDuaComponent, MkSoalanTigaComponent, MkSoalanEmpatComponent, MkSoalanLimaAComponent, MkSoalanLimaBComponent, MkSoalanEnamAComponent, MkSoalanEnamBComponent, MkSoalanTujuhComponent, MkSoalanLapanSatuAComponent, MkSoalanLapanSatuBComponent, MkSoalanLapanSatuCComponent, MkSoalanLapanSatuDComponent, MkSoalanLapanComponent, MkSoalanSembilanComponent, MkSoalanSepuluhComponent, MkSoalanSebelasComponent, MkSoalanDuabelasComponent, DateFnumToFroComponent, EmpatBComponent, LimaaComponent, LimabComponent, EnamaComponent, EnambComponent, DuabelasComponent,EnamComponent , TextFnumSingleInlineComponent, TextFnumSingleInlineDynamicComponent, EnambelasAComponent, JumlahComponent, TextCheckboxFnumDynamicComponent, SepuluhComponent, SebelasComponent, RadioFnumWithoutTextComponent,
    EditSpekComponent,
    EditSpekHeaderComponent,
    EditSpekSoalanComponent,
    PengakuanComponent,
    IctecSoalanSatuComponent,
    IctecSoalanDuaComponent,
    IctecSoalanTigaComponent,
    IctecSoalanEmpatAComponent,
    IctecSoalanEmpatBComponent,
    BasicTextComponent,
    UmumSoalanSatuComponent,
    UmumSoalanDuaComponent,
    UmumSoalanTigaComponent,
    UmumSoalanEmpatComponent,
    UmumSoalanLimaComponent,
    UmumSoalanEnamComponent,
    UmumSoalanTujuhComponent,
    UmumSoalanLapanComponent,
    UmumSoalanSembilanComponent,
    UmumSoalanSepuluhComponent,
    UmumSoalanSebelasComponent,
    UmumSoalanDuabelasComponent,
    UmumSoalanTigabelasComponent,
    UmumSoalanEmpatbelasComponent,
    UmumSoalanLimabelasComponent,
    UmumSoalanEnambelasComponent,
    SatuSatuComponent,
    TextIndentComponent,
    PejabatComponent,
    SatuDuaAComponent,
    TextFnumComponent,
    SatuTigaAComponent,
    TextareaFnumComponent,
    RadioFnumOriComponent,
    SatuEmpatAComponent,
    SatuLimaAComponent,
    SatuEnamAComponent,
    SatuTujuhAComponent,
    SatuLapanAComponent,
    SatuSembilanAComponent,
    DuaSatuAComponent,
    DuaDuaAComponent,
    NotaPendekComponent,
    PejabatAlamatComponent,
    RadioFnumDynamicComponent,
    PejabatMultipleComponent,
    NotaMultipleComponent,
    TextFnumMultipleComponent,
    CheckboxFnumDynamicComponent,
    TigaSatuAComponent,
    TigaDuaAComponent,
    EditSpekValidasiComponent,
    EmpatAComponent,
    TujuhSatuAComponent,
    TujuhDuaAComponent,
    TujuhTigaAComponent,
    TujuhEmpatAComponent,
    TujuhLimaAComponent,
    TujuhEnamAComponent,
    TujuhLapanAComponent,
    TujuhTujuhAComponent,
    TextFnumMultipleInlineComponent,
    TextFnumMultipleInlineDynamicComponent,
    LapanAComponent,
    SembilanAComponent,
    MkSoalanSatuComponent,
    MkSoalanDuaComponent,
    MkSoalanTigaComponent,
    MkSoalanEmpatComponent,
    MkSoalanLimaAComponent,
    MkSoalanLimaBComponent,
    MkSoalanEnamAComponent,
    MkSoalanEnamBComponent,
    MkSoalanTujuhComponent,
    MkSoalanLapanSatuAComponent,
    MkSoalanLapanSatuBComponent,
    MkSoalanLapanSatuCComponent,
    MkSoalanLapanSatuDComponent,
    MkSoalanLapanComponent,
    MkSoalanSembilanComponent,
    MkSoalanSepuluhComponent,
    MkSoalanSebelasComponent,
    MkSoalanDuabelasComponent,
    DateFnumToFroComponent,
    EmpatBComponent,
    LimaaComponent,
    LimabComponent,
    EnamaComponent,
    EnambComponent,
    DuabelasComponent,
    EnamComponent,
    TextFnumSingleInlineComponent,
    TextFnumSingleInlineDynamicComponent,
    EnambelasAComponent, JumlahComponent,
    TextCheckboxFnumDynamicComponent,
    SepuluhComponent,
    SebelasComponent,
    PembuatanComponent,
    PembuatanSoalanSatuComponent,
    PembuatanSoalanDuaComponent,
    PembuatanSoalanTigaComponent,
    PembuatanSoalanEmpatComponent,
    PembuatanSoalanLimaBComponent,
    PembuatanSoalanLimaAComponent,
    PembuatanSoalanEnamAComponent,
    PembuatanSoalanEnamBComponent,
    PembuatanSoalanTujuhComponent,
    PembuatanSoalanLapanComponent,
    PembuatanSoalanSembilanComponent,
    PembuatanSoalanSepuluhComponent,
    PembuatanSoalanSebelasComponent,
    PembuatanSoalanDuabelasComponent,
    PembuatanSoalanTigabelasComponent,
    PembuatanSoalanEmpatbelasComponent,
    PembuatanSoalanLimabelasComponent,
    PembuatanSoalanEnambelasComponent,
    MkDayaSaingComponent,
    MkSeksyenASatuComponent,
    MkSeksyenADuaComponent,
    MkSeksyenBComponent,
    MkSeksyenCComponent,
    MkSeksyenDComponent,
    F010003Component,
    F030012Component,
    NumOnlyInputComponent,
    L010002Component,
    F010002Component,
    BankSoalanComponent,
    F010028Component,
    F010029Component,
    CheckboxTextComponent,
    F0200011Component,
    F0200021Component,
    F030001Component,
    F030003Component,
    F030013Component,
    F030012Component,
    F010014Component,
    F0100161Component,
    F0100351Component,
    F0100361Component,
    F030012Component,
    F3100011Component,
    F310080Component,
    F310002Component,
    F3100031Component,
    F310004Component,
    F010004Component,
    F040101Component,
    F050101Component,
    F051001Component,
    F060101Component,
    F040101Component,
    F310005Component,
    F310011Component,
    F3100171Component,
    F310021Component,
    F3100341Component,
    F3100351Component,
    F3100361Component,
    F310037Component,
    F3100431Component,
    F080089Component,
    F310044Component,
    F061401Component,
    F070101Component,
    F087601Component,
    F087609Component,
    F310044Component,
    F310046Component,
    F310051Component,
    F310054Component,
    F310056Component,
    F3100621Component,
    F090089Component,
    F310063Component,
    F310065Component,
    F310068Component,
    F310070Component,
    F3100761Component,
    F310077Component,
    F310044Component,
    F010017Component,
    F010027Component,
    F010031Component,
    F010034Component,
    F040404Component,
    F052001Component,
    F070101Component,
    F060199Component,
    F080001Component,
    F090001Component,
    F100101Component,
    F320101Component,
    F330001Component,
    F330005Component,
    F330010Component,
    F330014Component,
    F330019Component,
    F330032Component,
    F330035Component,
    F330036Component,
    F330039Component,
    F330042Component,
    F330048Component,
    F330051Component,
    F340001Component,
    F340002Component,
    F340003Component,
    F160001Component,
    F151001Component,

    F080001Component,
    F070101Component,
    F087615Component,
    F087625Component,
    F080023Component,
    F090016Component,
    F010015Component,
    F010040Component
]
})
export class EditSpekModule { }
