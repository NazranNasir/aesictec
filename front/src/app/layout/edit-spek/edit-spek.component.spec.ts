import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSpekComponent } from './edit-spek.component';

describe('EditSpekComponent', () => {
  let component: EditSpekComponent;
  let fixture: ComponentFixture<EditSpekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSpekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSpekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
