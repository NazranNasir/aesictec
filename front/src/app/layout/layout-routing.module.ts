import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '', redirectTo: 'dashboard', pathMatch: 'prefix'
      },
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'import', loadChildren: './import/import.module#ImportModule' },
      { path: 'tangkapan-data', loadChildren: './tangkapan-data/tangkapan-data.module#TangkapanDataModule' },
      { path: 'tangkapan-data/edit-spek/:id/:nosiri/:tahun/:borang', loadChildren: './edit-spek/edit-spek.module#EditSpekModule' },
      { path: 'semakan-data', loadChildren: './semakan-data/semakan-data.module#SemakanDataModule' },
      { path: 'kelulusan-data', loadChildren: './kelulusan-data/kelulusan-data.module#KelulusanDataModule' },
      { path: 'penjadualan', loadChildren: './penjadualan/penjadualan.module#PenjadualanModule' },
      { path: 'kemajuan-prosesan', loadChildren: './kemajuan-prosesan/kemajuan-prosesan.module#KemajuanProsesanModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
