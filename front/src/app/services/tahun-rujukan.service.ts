import { Injectable } from '@angular/core';
import { TahunRujukan } from '../models/tahun-rujukan';
import { TAHUNRUJUKAN } from '../mock-data/mock-tahun-rujukan';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TahunRujukanService {

  constructor() { }

  getALLTAHUNRUJUKAN(): Observable<TahunRujukan[]> {
    return of(TAHUNRUJUKAN);
  }

  getTAHUNRUJUKAN(id: number) {
    return of(TAHUNRUJUKAN.find(tahunRujukan => tahunRujukan.id === id));
  }
}
