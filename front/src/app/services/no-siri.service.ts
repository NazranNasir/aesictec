import { Injectable } from '@angular/core';
import { NoSiri } from '../models/no-siri';
import { NOSIRI } from '../mock-data/mock-no-siri';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoSiriService {

  constructor() { }

  getAllNOSIRI(): Observable<NoSiri[]> {
    return of(NOSIRI);
  }

  getNOSIRI(id: number) {
    return of(NOSIRI.find(noSiri => noSiri.id === id));
  }
}
