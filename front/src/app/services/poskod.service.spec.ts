import { TestBed, inject } from '@angular/core/testing';

import { PoskodService } from './poskod.service';

describe('PoskodService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PoskodService]
    });
  });

  it('should be created', inject([PoskodService], (service: PoskodService) => {
    expect(service).toBeTruthy();
  }));
});
