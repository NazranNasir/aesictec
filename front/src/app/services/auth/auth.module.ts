import {NgModule} from "@angular/core";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {AuthEffects} from "./auth.effect";
import {authReducer} from "./auth.reducer";

@NgModule({
  imports: [
    StoreModule.forFeature("authState", {
      auth: authReducer
    }),
    EffectsModule.forFeature([AuthEffects])
  ]
})
export class AuthModule {

}
