import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
import {map} from "rxjs/operators";
import {TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME} from './auth.constant';
// import {AuthenticatedUser} from '../app/shared/model/identity/authenticated-user.interface';
import {JwtHelper} from 'angular2-jwt';
// import {Vendor} from '../app/shared/model/identity/vendor.interface';
// import {HttpClient, HttpHeaders} from '../../node_modules/@angular/common/http';
import {environment} from "../../../environments/environment";

@Injectable()
export class AuthenticationService {
  static AUTH_TOKEN = '/oauth/token';
  public token: string;
  public parsedToken: any;
  private jwtHelper: JwtHelper = new JwtHelper();

  constructor(private http: Http) {
  }

  private _roles: string[];

  get roles(): string[] {
    return this._roles;
  }

  // private _authenticatedUser: AuthenticatedUser;

  // get authenticatedUser(): AuthenticatedUser {
  //   return this._authenticatedUser;
  // }

  // set authenticatedUser(value: AuthenticatedUser) {
  //   this._authenticatedUser = value;
  // }

  login(username: string, password: string) {
    console.log("logging in " + username)
    const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));

    console.log(environment.endpoint + AuthenticationService.AUTH_TOKEN);
    return this.http.post(environment.endpoint + AuthenticationService.AUTH_TOKEN, body, {headers})
      .pipe(map(res => res.json()))
      .pipe(map((res) => {
        if (res.access_token) {
          this.token = res.access_token;
          this.parseRoles();
          return this.token;
        }
        console.log('res; ' + res);
        return null;
      }))
  }

  parseRoles(): void {
    this.parsedToken = this.jwtHelper.decodeToken(this.token);
    console.log('parseRoles: ' + JSON.stringify(this.parsedToken));
    this._roles = this.parsedToken.authorities;
    console.log('role: ' + this._roles);
  }
}
