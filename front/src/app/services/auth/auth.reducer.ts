import {Action} from '@ngrx/store';

export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGOUT = '[Auth] Logout',
  LOGOUT_SUCCESS = '[Auth] Logout Success',
  SET_TOKEN = '[Auth] Set token'
}

export class LoginAction implements Action {
  readonly type = AuthActionTypes.LOGIN;

  constructor(public username: string, public password: string) {

  }
}

export class LoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;

  constructor() {

  }
}

export class LogoutAction implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class SetTokenAction implements Action {
  readonly type = AuthActionTypes.SET_TOKEN;

  constructor(public payload: { token: string }) {

  }
}

export interface AuthState {
  isAuthenticated: boolean;
  accessToken: string
}

export const initialState: AuthState = {
  isAuthenticated: false,
  accessToken: null
};

export const selectorAuth = state => state.authState.auth;

export function authReducer(state: AuthState = initialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN:
      console.log("xxxx");
      return {...state, isAuthenticated: false};

    case AuthActionTypes.LOGIN_SUCCESS:
      return {...state, isAuthenticated: true,};

    case AuthActionTypes.SET_TOKEN:
      console.log(`AuthActionTypes.SET_TOKEN.payload`, action.payload.token);
      return {...state, isAuthenticated: true, accessToken: action.payload.token};

    case AuthActionTypes.LOGOUT:
      return {...state, isAuthenticated: false, accessToken: null};

    default:
      return state;
  }
}

export type AuthActions = LoginAction | LoginSuccessAction | LogoutAction | SetTokenAction;

