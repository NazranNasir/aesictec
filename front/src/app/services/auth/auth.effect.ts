import {Injectable, OnInit} from '@angular/core';
import {Router, RoutesRecognized} from '@angular/router';
import {Action} from '@ngrx/store';
import {Actions, Effect} from '@ngrx/effects';
import {forkJoin, Observable, of, Subject} from 'rxjs';
import {switchMap, takeUntil, tap} from 'rxjs/operators';

import {AuthActionTypes, LoginAction, LoginSuccessAction, SetTokenAction} from './auth.reducer';
// import {AuthenticationService} from '../../../services/authentication.service';
// import {AuthorizationService} from '../../../services/authorization.service';
// import {IdentityService} from '../../../services/identity.service';
// import {LocalStorageService} from "../../../services/local-storage.service";
import {AUTH_ACL, AUTH_USER, TOKEN_NAME} from "./auth.constant";
import {AuthorizationService} from "./authorization.service";
import {AuthenticationService} from "./authentization.service";
// import {IdentityService} from "./identity.service";
import {LocalStorageService} from "./local-storage.service";

@Injectable()
export class AuthEffects implements OnInit {
  redirectUrl: string = null;
  unsubscribed$: Subject<void> = new Subject();

  constructor(private actions$: Actions<Action>,
              private router: Router,
              private authenticationService: AuthenticationService,
              private authorizationService: AuthorizationService,
              // private identityService: IdentityService,
              private localStorageService: LocalStorageService
  ) {

  }

  ngOnInit() {
    this.router.events
      .pipe(
        takeUntil(this.unsubscribed$),
      )
      .subscribe(
        (v) => {
          if (v instanceof RoutesRecognized) {
            this.redirectUrl = v.state.root.queryParams.redirectTo;
          }
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribed$.next();
    this.unsubscribed$.complete();
  }

  @Effect()
  login(): Observable<Action> {
    return this.actions$
      .ofType(AuthActionTypes.LOGIN)
      .pipe(
        switchMap((action: LoginAction) => {
          console.log("effect.ts");
          let username = action.username;
          let password = action.password;
          return forkJoin([of(username), this.authenticationService.login(username, password)]);
        }),
        switchMap(data => {
          const [username, token] = data;
          return [
            new SetTokenAction({token: token}),
          ];
        })
      );
  }

  @Effect()
  setToken(): Observable<Action> {
    return this.actions$
      .ofType(AuthActionTypes.SET_TOKEN)
      .pipe(
        tap((action: SetTokenAction) => {
          // this.identityService.accessToken = action.payload.token;
          localStorage.setItem('XXX', action.payload.token);
          this.localStorageService.setItem(TOKEN_NAME, action.payload.token);
        }),
        switchMap(() => [new LoginSuccessAction()])
      );
  }

  @Effect({dispatch: false})
  loginSuccess(): Observable<Action> {
    return this.actions$
      .ofType(AuthActionTypes.LOGIN_SUCCESS)
      .pipe(
        tap(() => {
          this.populateRoles();
          this.populateUser();
          this.navigateOnSuccess();
        })
      );
  }

  @Effect({dispatch: false})
  logout(): Observable<Action> {
    return this.actions$
      .ofType(AuthActionTypes.LOGOUT)
      .pipe(
        tap(action => {
          this.localStorageService.removeItem(TOKEN_NAME).subscribe(_ => {
          });
          this.localStorageService.removeItem(AUTH_USER).subscribe(_ => {
          });
          this.localStorageService.removeItem(AUTH_ACL).subscribe(_ => {
          });
          this.authorizationService.flushRoles();
          this.router.navigate(['/login']);
        })
      );
  }

  populateRoles(): void {
    console.log('populate roles');
    this.authorizationService.flushRoles();
    this.authenticationService.roles.forEach((role: string) => {
      this.authorizationService.attachRole(role);
    });
  }

  populateUser(): void {
    console.log('populate user');
    // this.identityService.findAuthenticatedUser()
    //   .map((user: AuthenticatedUser) => {
    //     this.authenticationService.authenticatedUser = user;
    //     this.localStorageService.setItem(AUTH_USER, JSON.stringify(user));
    //   })
    //   .toPromise();
  }

  navigateOnSuccess(): void {
    if (this.redirectUrl) {
      this.router.navigateByUrl(this.redirectUrl);
    } else {
      this.router.navigate(['/']);
    }
    /*
        else if (this.authorizationService.hasRole(RoleType.ROLE_STAFF)) {
          this.router.navigate(['/staff']);
        } else if (this.authorizationService.hasRole(RoleType.ROLE_VENDOR)) {
          this.router.navigate(['/vendor']);
        }
    */
  }
}
