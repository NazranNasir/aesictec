import {Injectable} from "@angular/core";
import {Observable} from 'rxjs';
import {of} from 'rxjs/observable/of';
import {Subject} from "rxjs/Subject";

@Injectable()
export class LocalStorageService {

  public setItem(key: string, value: string): void {
    of(localStorage.setItem(key, value))
  }

  public getItem<T>(key: string): Subject<T> {
    return Subject.create(JSON.parse(localStorage.getItem(key)));
  }

  public removeItem(key: string): Observable<void> {
    return of(localStorage.removeItem(key))
  }
}
