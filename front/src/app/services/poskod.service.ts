import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Poskod} from '../models/poskod';
import {POSKOD} from '../mock-data/mock-poskod';

@Injectable({
  providedIn: 'root'
})
export class PoskodService {

  constructor() { }
  getALLPOSKOD(): Observable<Poskod[]> {
    return of(POSKOD);
  }

  getPOSKOD(id: number) {
    return of(POSKOD.find(poskod => poskod.id === id));
  }
}
