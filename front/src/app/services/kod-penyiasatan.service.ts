import { Injectable } from '@angular/core';
import { KodPenyiasatan } from '../models/kod-penyiasatan';
import { KODPENYIASATAN } from '../mock-data/mock-kod-penyiasatan';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KodPenyiasatanService {

  constructor() { }

  getAllKP(): Observable<KodPenyiasatan[]> {
    return of(KODPENYIASATAN);
  }

  getKP(id: number) {
    return of(KODPENYIASATAN.find(kodPenyiasatan => kodPenyiasatan.id === id));
  }
}
