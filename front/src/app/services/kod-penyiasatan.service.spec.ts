import { TestBed, inject } from '@angular/core/testing';

import { KodPenyiasatanService } from './kod-penyiasatan.service';

describe('KodPenyiasatanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KodPenyiasatanService]
    });
  });

  it('should be created', inject([KodPenyiasatanService], (service: KodPenyiasatanService) => {
    expect(service).toBeTruthy();
  }));
});
