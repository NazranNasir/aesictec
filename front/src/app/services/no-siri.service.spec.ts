import { TestBed, inject } from '@angular/core/testing';

import { NoSiriService } from './no-siri.service';

describe('NoSiriService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoSiriService]
    });
  });

  it('should be created', inject([NoSiriService], (service: NoSiriService) => {
    expect(service).toBeTruthy();
  }));
});
