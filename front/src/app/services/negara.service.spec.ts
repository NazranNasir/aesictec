import { TestBed, inject } from '@angular/core/testing';

import { NegaraService } from './negara.service';

describe('NegaraService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NegaraService]
    });
  });

  it('should be created', inject([NegaraService], (service: NegaraService) => {
    expect(service).toBeTruthy();
  }));
});
