import { TestBed, inject } from '@angular/core/testing';

import { TahunRujukanService } from './tahun-rujukan.service';

describe('TahunRujukanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TahunRujukanService]
    });
  });

  it('should be created', inject([TahunRujukanService], (service: TahunRujukanService) => {
    expect(service).toBeTruthy();
  }));
});
