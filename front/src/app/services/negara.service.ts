import { Injectable } from '@angular/core';
import { Negara } from '../models/negara';
import { NEGARA } from '../mock-data/mock-negara';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NegaraService {

  constructor() { }

  getAllNegara(): Observable<Negara[]> {
    return of(NEGARA);
  }

  getNegara(id: number) {
    return of(NEGARA.find(negara => negara.id === id));
  }
}

