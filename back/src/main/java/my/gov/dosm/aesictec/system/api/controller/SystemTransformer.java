package my.gov.dosm.aesictec.system.api.controller;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import my.gov.dosm.aesictec.system.api.vo.Configuration;
import my.gov.dosm.aesictec.system.api.vo.Module;
import my.gov.dosm.aesictec.system.api.vo.ReferenceNo;
import my.gov.dosm.aesictec.system.api.vo.SubModule;
import my.gov.dosm.aesictec.system.domain.model.CngConfiguration;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNo;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;

/**
 */
@Component("systemTransformer")
public class SystemTransformer {

    public SystemTransformer() {
    }

    // =============================================================================================
    // CONFIGURATION
    // =============================================================================================

    public List<Configuration> toConfigurationVos(List<CngConfiguration> e) {
        List<Configuration> vos = e.stream()
                .map((e1) -> toConfigurationVo(e1))
                .collect(Collectors.toList());
        return vos;
    }

    public Configuration toConfigurationVo(CngConfiguration e) {
        Configuration vo = new Configuration();
        vo.setId(e.getId());
        vo.setKey(e.getKey());
        vo.setValue(e.getValue());
        vo.setDescription(e.getDescription());
        return vo;
    }

    public List<ReferenceNo> toReferenceNoVos(List<CngReferenceNo> e) {
        List<ReferenceNo> vos = e.stream()
                .map((e1) -> toReferenceNoVo(e1))
                .collect(Collectors.toList());
        return vos;
    }

    public ReferenceNo toReferenceNoVo(CngReferenceNo e) {
        ReferenceNo vo = new ReferenceNo();
        vo.setId(e.getId());
        vo.setCode(e.getCode());
        vo.setPrefix(e.getPrefix());
        vo.setReferenceFormat(e.getReferenceFormat());
        vo.setSequenceFormat(e.getSequenceFormat());
        vo.setIncrementValue(e.getIncrementValue());
        vo.setCurrentValue(e.getCurrentValue());
        return vo;
    }

    public List<Module> toModuleVos(List<CngModule> e) {
        List<Module> vos = e.stream()
                .map((e1) -> toModuleVo(e1))
                .collect(Collectors.toList());
        return vos;
    }

    public Module toModuleVo(CngModule e) {
        Module vo = new Module();
        vo.setId(e.getId());
        vo.setCode(e.getCode());
        vo.setDescription(e.getDescription());
        return vo;
    }

    public List<SubModule> toSubModuleVos(List<CngSubModule> e) {
        List<SubModule> vos = e.stream()
                .map((e1) -> toSubModuleVo(e1))
                .collect(Collectors.toList());
        return vos;
    }

    public SubModule toSubModuleVo(CngSubModule e) {
        SubModule vo = new SubModule();
        vo.setId(e.getId());
        vo.setCode(e.getCode());
        vo.setDescription(e.getDescription());
        return vo;
    }
}
