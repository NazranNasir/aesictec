package my.gov.dosm.aesictec.validation.domain.dao;

import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;

/**
 * @author canang technologies
 */
public interface AesEditSpecSchemaDao extends GenericDao<Long, AesEditSpecSchema> {

  AesEditSpecSchema findByCaseNo(String caseNo);

  AesEditSpecSchemaItem findItemByQuestion(AesEditSpecSchema editSpecSchema, String question);

  void saveEditSpec(AesEditSpecSchema editSpec);

  void updateEditSpec(AesEditSpecSchema editSpec);

  List<AesEditSpecSchemaItem> findItems(AesEditSpecSchema spec);

  boolean isFilled(AesEditSpecSchema spec, String question);

  boolean isNotFilled(AesEditSpecSchema spec, String question);

  boolean isNegative(AesEditSpecSchema spec, String question);

  boolean isNotNegative(AesEditSpecSchema spec, String question);

  boolean isIn(AesEditSpecSchema spec, String question, String[] values);

  boolean isNotIn(AesEditSpecSchema spec, String question, String[] values);

  void addItem(AesEditSpecSchema editSpec, AesEditSpecSchemaItem item);

  void updateItem(AesEditSpecSchema editSpec, AesEditSpecSchemaItem item);

  void deleteItem(AesEditSpecSchema editSpec, AesEditSpecSchemaItem item);

}
