package my.gov.dosm.aesictec.validation.api.vo;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author canang technologies
 */
public class EditSpec {

  private Long id;
  private String caseNo;
  private EditSpecState state;
  private Map<String, String> values = new HashMap<String,String>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCaseNo() {
    return caseNo;
  }

  public void setCaseNo(String caseNo) {
    this.caseNo = caseNo;
  }

  public EditSpecState getState() {
    return state;
  }

  public void setState(EditSpecState state) {
    this.state = state;
  }

  public Map<String, String> getValues() {
    return values;
  }

  public void setValues(Map<String, String> values) {
    this.values = values;
  }
}
