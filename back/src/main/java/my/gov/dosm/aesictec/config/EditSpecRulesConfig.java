package my.gov.dosm.aesictec.config;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scripting.bsh.BshScriptEvaluator;

import my.gov.dosm.aesictec.validation.business.rules.Case411RuleBook;
import my.gov.dosm.aesictec.validation.business.rules.ValidationResult;

/**
 * @author canang technologies
 */
@Configuration
public class EditSpecRulesConfig {

  @Bean
  public BshScriptEvaluator evaluator(){
    return new BshScriptEvaluator();
  }
}
