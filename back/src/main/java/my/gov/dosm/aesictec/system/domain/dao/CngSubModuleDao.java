package my.gov.dosm.aesictec.system.domain.dao;


import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;


/**
 */
public interface CngSubModuleDao extends GenericDao<Long, CngSubModule> {

    CngSubModule findByCode(String code);

    List<CngSubModule> find();

    List<CngSubModule> find(CngModule module, Integer offset, Integer limit);

    Integer count();

    Integer count(CngModule module);

    Integer count(String filter);

    boolean isExists(String code);
}
