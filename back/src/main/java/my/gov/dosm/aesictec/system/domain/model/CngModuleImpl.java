package my.gov.dosm.aesictec.system.domain.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import my.gov.dosm.aesictec.core.domain.CngMetadata;

/**
 * @author canang technologies
 * @since 4/18/14
 */
@Entity(name = "CngModule")
@Table(name = "CNG_MODL")
public class CngModuleImpl implements CngModule, Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SQ_CNG_MODL")
    @SequenceGenerator(name = "SQ_CNG_MODL", sequenceName = "SQ_CNG_MODL", allocationSize = 1)
    private Long id;

    @Column(name = "CODE", unique = true, nullable = false)
    private String code;

    @Column(name = "CANONICAL_CODE", unique = true, nullable = false)
    private String canonicalCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ORDINAL", nullable = false)
    private Integer ordinal = 0;

    @Column(name = "ENABLED")
    private boolean enabled = true;

    @Embedded
    private CngMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getCanonicalCode() {
        return canonicalCode;
    }

    @Override
    public void setCanonicalCode(String canonicalCode) {
        this.canonicalCode = canonicalCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Integer getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public CngMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(CngMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngModule.class;
    }
}
