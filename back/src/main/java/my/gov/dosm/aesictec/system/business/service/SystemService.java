package my.gov.dosm.aesictec.system.business.service;


import java.util.List;
import java.util.Map;

import my.gov.dosm.aesictec.system.domain.model.CngConfiguration;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNo;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;

/**
 */
public interface SystemService {

    //==============================================================================================
    // MODULE SUB MODULE
    //==============================================================================================

    CngModule findModuleById(Long id);

    CngSubModule findSubModuleById(Long id);

    CngModule findModuleByCode(String code);

    CngSubModule findSubModuleByCode(String code);

    List<CngModule> findModules();

    List<CngModule> findModules(boolean authorized);

    List<CngModule> findModules(Integer offset, Integer limit);

    List<CngModule> findModules(boolean authorized, Integer offset, Integer limit);

    List<CngSubModule> findSubModules();

    List<CngSubModule> findSubModules(boolean authorized);

    List<CngSubModule> findSubModules(Integer offset, Integer limit);

    List<CngSubModule> findSubModules(CngModule module, Integer offset, Integer limit);

    Integer countModule();

    Integer countSubModule();

    Integer countSubModule(CngModule module);

    void saveModule(CngModule module);

    void updateModule(CngModule module);

    void addSubModule(CngModule module, CngSubModule subModule);

    void updateSubModule(CngModule module, CngSubModule subModule);

    //==============================================================================================
    // REFERENCE NO
    //==============================================================================================

    CngReferenceNo findReferenceNoById(Long id);

    CngReferenceNo findReferenceNoByCode(String code);

    List<CngReferenceNo> findReferenceNos(Integer offset, Integer limit);

    List<CngReferenceNo> findReferenceNos(String filter, Integer offset, Integer limit);

    Integer countReferenceNo();

    Integer countReferenceNo(String filter);

    void saveReferenceNo(CngReferenceNo referenceNo);

    void updateReferenceNo(CngReferenceNo referenceNo);

    void removeReferenceNo(CngReferenceNo referenceNo);

    String generateReferenceNo(String code);

    String generateFormattedReferenceNo(String code, Map<String, Object> map);

    //==============================================================================================
    // CONFIGURATION
    //==============================================================================================

    CngConfiguration findConfigurationById(Long id);

    CngConfiguration findConfigurationByKey(String key);

    List<CngConfiguration> findConfigurationsByPrefix(String prefix);

    List<CngConfiguration> findConfigurations();

    List<CngConfiguration> findConfigurations(Integer offset, Integer limit);

    List<CngConfiguration> findConfigurations(String filter);

    List<CngConfiguration> findConfigurations(String filter, Integer offset, Integer limit);

    Integer countConfiguration();

    Integer countConfiguration(String filter);

    void saveConfiguration(CngConfiguration config);

    void updateConfiguration(CngConfiguration config);

    void removeConfiguration(CngConfiguration config);
}
