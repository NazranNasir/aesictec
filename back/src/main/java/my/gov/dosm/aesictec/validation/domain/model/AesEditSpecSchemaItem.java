package my.gov.dosm.aesictec.validation.domain.model;

import java.util.Date;

/**
 * @author canang technologies
 */
public interface AesEditSpecSchemaItem {

  String getQuestion();

  void setQuestion(String question);

  String getRules();

  void setRules(String rules);

  AesEditSpecSchema getEditSpecSchema();

  void setEditSpecSchema(AesEditSpecSchema spec);
}

