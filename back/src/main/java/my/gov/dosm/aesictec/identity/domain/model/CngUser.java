package my.gov.dosm.aesictec.identity.domain.model;

/**
 * @author canang technologies
 */
public interface CngUser extends CngPrincipal {

    String DEFAULT_PASSWORD = "abc123";

    String getUsername();

    void setUsername(String username);

    String getRealName();

    void setRealName(String firstName);

    String getEmail();

    void setEmail(String email);

    String getPassword();

    void setPassword(String password);

    CngActor getActor();

    void setActor(CngActor actor);

}
