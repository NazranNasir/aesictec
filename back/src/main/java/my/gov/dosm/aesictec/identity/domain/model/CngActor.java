package my.gov.dosm.aesictec.identity.domain.model;

import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 * @author canang technologies
 */
public interface CngActor extends CngMetaObject {

    String getCode();

    void setCode(String code);

    String getIdentityNo();

    void setIdentityNo(String identityNo);

    String getName();

    void setName(String name);

    String getFax();

    void setFax(String fax);

    String getEmail();

    void setEmail(String email);

    String getAddress1();

    void setAddress1(String address1);

    String getAddress2();

    void setAddress2(String address2);

    String getAddress3();

    void setAddress3(String address3);

    String getPostcode();

    void setPostcode(String postcode);

    CngActorType getActorType();

    void setActorType(CngActorType actorType);

}
