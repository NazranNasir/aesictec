package my.gov.dosm.aesictec.identity.domain.model;


import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 * @author canang technologies
 */
public interface CngGroupMember extends CngMetaObject {

    CngGroup getGroup();

    void setGroup(CngGroup group);

    CngPrincipal getPrincipal();

    void setPrincipal(CngPrincipal principal);
}
