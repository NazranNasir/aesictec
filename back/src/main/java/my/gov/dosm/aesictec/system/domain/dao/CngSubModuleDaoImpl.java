package my.gov.dosm.aesictec.system.domain.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;
import my.gov.dosm.aesictec.system.domain.model.CngSubModuleImpl;

/**
 */
@Repository("subModuleDao")
public class CngSubModuleDaoImpl extends GenericDaoSupport<Long, CngSubModule> implements CngSubModuleDao {

    private static final Logger LOG = LoggerFactory.getLogger(CngSubModuleDaoImpl.class);

    public CngSubModuleDaoImpl() {
        super(CngSubModuleImpl.class);
    }

    @Override
    public CngSubModule findByCode(String code) {
        Query query = entityManager.createQuery("select c from CngSubModule c where c.code = :code");
        query.setParameter("code", code);
        return (CngSubModule) query.getSingleResult();
    }

    @Override
    public List<CngSubModule> find(CngModule module, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select c from CngSubModule c where " +
                "c.module = :module");
        query.setParameter("module", module);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<CngSubModule>) query.getResultList();
    }

    @Override
    public Integer count() {
        Query query = entityManager.createQuery("select count(c) from CngSubModule c");
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer count(CngModule module) {
        Query query = entityManager.createQuery("select count(s) from CngSubModule s where " +
                "s.module = :module");
        query.setParameter("module", module);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(s) from CngSubModule s where " +
                "upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Query query = entityManager.createQuery("select count(c) from CngSubModule c where " +
                "c.code = :code");
        query.setParameter("code", code);
        return 0 < ((Long) query.getSingleResult()).intValue();
    }
}
