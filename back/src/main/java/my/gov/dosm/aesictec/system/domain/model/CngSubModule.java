package my.gov.dosm.aesictec.system.domain.model;

import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 */
public interface CngSubModule extends CngMetaObject {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    Integer getOrdinal();

    void setOrdinal(Integer ordinal);

    boolean isEnabled();

    void setEnabled(boolean enabled);

    CngModule getModule();

    void setModule(CngModule module);
}
