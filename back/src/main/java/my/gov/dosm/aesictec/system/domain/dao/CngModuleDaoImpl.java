package my.gov.dosm.aesictec.system.domain.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngModuleImpl;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;


/**
 */
@Repository("moduleDao")
public class CngModuleDaoImpl extends GenericDaoSupport<Long, CngModule> implements CngModuleDao {

    public CngModuleDaoImpl() {
        super(CngModuleImpl.class);
    }

    @Override
    public CngModule findByCode(String code) {
        Query query = entityManager.createQuery("select c from CngModule c where c.code = :code");
        query.setParameter("code", code);
        return (CngModule) query.getSingleResult();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(s) from CngModule s where " +
                "upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Query query = entityManager.createQuery("select count(c) from CngModule c where " +
                "c.code = :code");
        query.setParameter("code", code);
        return 0 < ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public boolean isSubModuleExists(String code) {
        Query query = entityManager.createQuery("select count(c) from CngSubModule c where " +
                "c.code = :code");
        query.setParameter("code", code);
        return 0 < ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public void addSubModule(CngModule module, CngSubModule subModule, CngUser user) {
        subModule.setModule(module);
        entityManager.persist(subModule);
    }

    @Override
    public void updateSubModule(CngModule module, CngSubModule subModule, CngUser user) {
        subModule.setModule(module);
        entityManager.persist(subModule);
    }

    @Override
    public void removeSubModule(CngModule module, CngSubModule subModule, CngUser user) {
        entityManager.remove(subModule);
    }
}
