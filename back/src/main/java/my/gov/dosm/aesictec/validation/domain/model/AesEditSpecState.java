package my.gov.dosm.aesictec.validation.domain.model;

/**
 * @author canang technologies
 */
public enum AesEditSpecState {

  JP_IMPORTED,
  JP_VALIDATED,
  JP_APPROVED,
  SMD_APPROVED,
  SMD_SCHEDULED,
  SMD_FINALIZED;
}
