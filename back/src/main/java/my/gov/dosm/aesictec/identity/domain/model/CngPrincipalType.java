package my.gov.dosm.aesictec.identity.domain.model;

/**
 * @author canang technologies
 */
public enum CngPrincipalType {

    USER, //0
    GROUP, //1
 }
