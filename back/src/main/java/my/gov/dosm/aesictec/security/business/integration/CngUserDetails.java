package my.gov.dosm.aesictec.security.business.integration;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

import my.gov.dosm.aesictec.identity.domain.model.CngUser;

public class CngUserDetails implements UserDetails{

	private CngUser user;
	private List<GrantedAuthority> grantedAuthorities;

	public CngUserDetails() {
	}

	public CngUserDetails(CngUser user, List<GrantedAuthority> grantedAuthorities) {
		this.user = user;
		this.grantedAuthorities = grantedAuthorities;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return grantedAuthorities;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

	public void setUser(CngUser user) {
		this.user = user;
	}

	public CngUser getUser() {
		return user;
	}
}
