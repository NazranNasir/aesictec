package my.gov.dosm.aesictec.core.domain;

import java.util.List;

import my.gov.dosm.aesictec.identity.domain.model.CngUser;

/**
 * @author canang technologies
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find();

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, CngUser user);

    void saveOrUpdate(I entity, CngUser user);

    void update(I entity, CngUser user);

    void deactivate(I entity, CngUser user);

    void remove(I entity, CngUser user);

    void delete(I entity, CngUser user);
}
