package my.gov.dosm.aesictec.validation.domain.dao;

import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;

/**
 * @author canang technologies
 */
public interface AesEditSpecDao extends GenericDao<Long, AesEditSpec> {

  void saveEditSpec(AesEditSpec editSpec);

  void updateEditSpec(AesEditSpec editSpec);

  List<AesEditSpecItem> findItems(AesEditSpec spec);

  boolean isFilled(AesEditSpec spec, String question);

  boolean isNotFilled(AesEditSpec spec, String question);

  boolean isNegative(AesEditSpec spec, String question);

  boolean isNotNegative(AesEditSpec spec, String question);

  boolean isIn(AesEditSpec spec, String question, String[] values);

  boolean isNotIn(AesEditSpec spec, String question, String[] values);

  void addItem(AesEditSpec editSpec, AesEditSpecItem item);

  void updateItem(AesEditSpec editSpec, String question, String response);

  void deleteItem(AesEditSpec editSpec, AesEditSpecItem item);

}
