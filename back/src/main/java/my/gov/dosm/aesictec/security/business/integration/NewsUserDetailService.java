package my.gov.dosm.aesictec.security.business.integration;

import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalRole;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.security.business.integration.model.NewsUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service("newSDetailService")
@Transactional
public class NewsUserDetailService implements UserDetailsService {

  private static final Logger LOG = LoggerFactory.getLogger(NewsUserDetailService.class);

  @Autowired
  private EntityManager entityManager;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
    Query query = entityManager.createQuery("select p from NewssUser p where p.userId= :userId");
    query.setParameter("userId", username);
    NewsUser user = (NewsUser) query.getSingleResult();
    LOG.debug("loading user: {} ", username);

    if (user == null)
      throw new UsernameNotFoundException("No such user");

        List<GrantedAuthority> authorityList = loadGrantedAuthoritiesFor(user);
    return new NewssUserDetails(user, authorityList);
  }

  private List<GrantedAuthority> loadGrantedAuthoritiesFor(NewsUser user) {
    List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
    //1. query nwess_grp_user, select group_id where user_id = user.getUser()
    //2. loop result, add masuk grantdAuthorities

//    Set<CngPrincipalRole> roles = user.getRoles();
//    for (CngPrincipalRole role : roles) {
//      LOG.debug("role; " + role.getRole().name());
//      grantedAuthorities.add(new SimpleGrantedAuthority("groupid"));
//    }
    return grantedAuthorities;
  }
}
