package my.gov.dosm.aesictec.system.domain.model;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import my.gov.dosm.aesictec.core.domain.CngMetadata;

/**
 */
@Entity(name = "CngSubModule")
@Table(name = "CNG_SMDL")
public class CngSubModuleImpl implements CngSubModule, Serializable {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SQ_CNG_SMDL")
    @SequenceGenerator(name = "SQ_CNG_SMDL", sequenceName = "SQ_CNG_SMDL", allocationSize = 1)
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ORDINAL")
    private Integer ordinal = 0;

    @Column(name = "ENABLED")
    private boolean enabled = true;

    @OneToOne(targetEntity = CngModuleImpl.class)
    @JoinColumn(name = "MODULE_ID")
    private CngModule module;

    @Embedded
    private CngMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Integer getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public CngModule getModule() {
        return module;
    }

    public void setModule(CngModule module) {
        this.module = module;
    }

    public CngMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CngMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngSubModule.class;
    }

}
