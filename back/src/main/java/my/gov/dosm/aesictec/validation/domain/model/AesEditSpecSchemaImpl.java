package my.gov.dosm.aesictec.validation.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * @author canang technologies
 */
@Entity(name = "AesEditSpecSchema")
@Table(name = "AES_EDIT_SPEC_SCHM")
public class AesEditSpecSchemaImpl implements AesEditSpecSchema {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(generator = "SQ_AES_EDIT_SPEC_SCHM")
  @SequenceGenerator(name = "SQ_AES_EDIT_SPEC_SCHM", sequenceName = "SQ_AES_EDIT_SPEC_SCHM", allocationSize = 1)
  private Long id;

  @Column(name = "CASE_NO", nullable = false)
  private String caseNo;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getCaseNo() {
    return caseNo;
  }

  @Override
  public void setCaseNo(String caseNo) {
    this.caseNo = caseNo;
  }
}
