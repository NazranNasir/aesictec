package my.gov.dosm.aesictec.identity.api.controller;

import my.gov.dosm.aesictec.identity.domain.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

import my.gov.dosm.aesictec.common.api.controller.CommonTransformer;
import my.gov.dosm.aesictec.common.business.service.CommonService;
import my.gov.dosm.aesictec.identity.api.vo.Group;
import my.gov.dosm.aesictec.identity.api.vo.GroupMember;
import my.gov.dosm.aesictec.identity.api.vo.GroupResult;
import my.gov.dosm.aesictec.identity.api.vo.Principal;
import my.gov.dosm.aesictec.identity.api.vo.Staff;
import my.gov.dosm.aesictec.identity.api.vo.User;
import my.gov.dosm.aesictec.identity.api.vo.UserResult;
import my.gov.dosm.aesictec.identity.business.service.IdentityService;
import my.gov.dosm.aesictec.security.business.service.SecurityService;
import my.gov.dosm.aesictec.system.business.service.SystemService;

import static my.gov.dosm.aesictec.Constants.LIMIT;

/**
 * @author canang technologies
 */
@RestController
@Transactional
@RequestMapping("/api/identity")
public class IdentityController {

    private static final Logger LOG = LoggerFactory.getLogger(IdentityController.class);

    private IdentityService identityService;
    private CommonService commonService;
    private SystemService systemService;
    private SecurityService securityService;
    private IdentityTransformer identityTransformer;
    private CommonTransformer commonTransformer;
    private AuthenticationManager authenticationManager;

    @Autowired
    public IdentityController(IdentityService identityService, CommonService commonService,
                              SystemService systemService, SecurityService securityService,
                              IdentityTransformer identityTransformer,
                              CommonTransformer commonTransformer,
                              AuthenticationManager authenticationManager) {
        this.identityService = identityService;
        this.commonService = commonService;
        this.systemService = systemService;
        this.securityService = securityService;
        this.identityTransformer = identityTransformer;
        this.commonTransformer = commonTransformer;
        this.authenticationManager = authenticationManager;
    }

    // =============================================================================================
    // AUTHENTICATED USER AND ACTOR
    // =============================================================================================

    @GetMapping(value = "/authenticated-user")
    public ResponseEntity<User> findAuthenticatedUser() {
        CngUser currentUser = securityService.getCurrentUser();
        return new ResponseEntity<User>(identityTransformer.toUserVo(currentUser), HttpStatus.OK);
    }

    //==============================================================================================
    // PRINCIPAL
    //==============================================================================================

    @GetMapping(value = "/principals")
    public ResponseEntity<List<Principal>> findPrincipals() {
        return new ResponseEntity<List<Principal>>(identityTransformer.toPrincipalVos(
                identityService.findPrincipals("%", 0, Integer.MAX_VALUE)), HttpStatus.OK);
    }

    @GetMapping(value = "/principals/{name}")
    public ResponseEntity<Principal> findPrincipalByName(@PathVariable String name) {
        return new ResponseEntity<Principal>(identityTransformer.toPrincipalVo(
                identityService.findPrincipalByName(name)), HttpStatus.OK);
    }

    //==============================================================================================
    // USER
    //==============================================================================================

    @GetMapping(value = "/users", params = {"page"})
    public ResponseEntity<UserResult> findPagedUsers(@RequestParam Integer page) {
        Integer count = identityService.countUser("%");
        List<CngUser> users = identityService.findUsers("%", (page - 1) * LIMIT, LIMIT);
        return new ResponseEntity<UserResult>(new UserResult(
                identityTransformer.toUserVos(users),
                count
        ), HttpStatus.OK);
    }

    @GetMapping(value = "/users")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<User>> findUsers() {
        return new ResponseEntity<List<User>>(identityTransformer.toUserVos(
                identityService.findUsers("%", 0, Integer.MAX_VALUE)), HttpStatus.OK);
    }

    @GetMapping(value = "/users/{username}")
    public ResponseEntity<User> findUserByUsername(@PathVariable String username) {
        return new ResponseEntity<User>(identityTransformer.toUserVo(
                identityService.findUserByUsername(username)), HttpStatus.OK);
    }

    @PostMapping(value = "/users")
    public ResponseEntity<String> saveUser(@RequestBody User vo) {
        CngUser user = new CngUserImpl();
        user.setName(vo.getName());
        user.setRealName(vo.getRealName());
        user.setEmail(vo.getEmail());
        user.setPassword(vo.getPassword());
        identityService.saveUser(user);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    @PutMapping(value = "/users/{username}")
    public ResponseEntity<String> updateUser(@PathVariable String username, @RequestBody User vo, @RequestBody Staff testee) {
        CngUser user = identityService.findUserByUsername(username);
        user.setName(vo.getName());
        user.setRealName(vo.getRealName());
        user.setEmail(vo.getEmail());
        user.setPassword(vo.getPassword());
        // todo: actor
        identityService.updateUser(user);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    @DeleteMapping(value = "/users/{username}")
    public ResponseEntity<String> removeUser(@PathVariable String username) {
        CngUser user = identityService.findUserByUsername(username);
        identityService.removeUser(user);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    //==============================================================================================
    // GROUP
    //==============================================================================================

    @GetMapping(value = "/groups", params = {"page"})
    public ResponseEntity<GroupResult> findPagedGroups(@RequestParam Integer page) {
        Integer count = identityService.countGroup();
        List<CngGroup> groups = identityService.findGroups((page - 1) * LIMIT, LIMIT);
        return new ResponseEntity<GroupResult>(
                new GroupResult(
                        identityTransformer.toGroupVos(groups),
                        count
                ), HttpStatus.OK);
    }

    @GetMapping(value = "/groups")
    public ResponseEntity<List<Group>> findGroups() {
        return new ResponseEntity<List<Group>>(identityTransformer.toGroupVos(
                identityService.findGroups(0, Integer.MAX_VALUE)), HttpStatus.OK);
    }

    @GetMapping(value = "/groups/{name}")
    public ResponseEntity<Group> findGroupByName(@PathVariable String name) {
        return new ResponseEntity<Group>(identityTransformer.toGroupVo(
                identityService.findGroupByName(name)), HttpStatus.OK);
    }

    @GetMapping(value = "/groups/{name}/group-members")
    public ResponseEntity<List<GroupMember>> findGroupMembers(@PathVariable String name) {
        CngGroup groupByName = identityService.findGroupByName(name);
        return new ResponseEntity<List<GroupMember>>(
                identityTransformer.toGroupMemberVos(identityService.findGroupMembersAsGroupMember(groupByName)),
                HttpStatus.OK);
    }

    @PostMapping(value = "/groups")
    public ResponseEntity<String> saveGroup(@RequestBody Group vo) {
        CngGroup group = new CngGroupImpl();
        group.setName(vo.getName());
        identityService.saveGroup(group);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    @PutMapping(value = "/groups/{name}")
    public ResponseEntity<String> updateGroup(@PathVariable String name, @RequestBody Group vo) {
        CngGroup group = identityService.findGroupByName(name);
        group.setName(vo.getName());
        identityService.updateGroup(group);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    @DeleteMapping(value = "/groups/{name}")
    public ResponseEntity<String> removeGroup(@PathVariable String name) {
        CngGroup group = identityService.findGroupByName(name);
        identityService.removeGroup(group);
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }
}
