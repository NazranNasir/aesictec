package my.gov.dosm.aesictec.identity.domain.dao;


import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.identity.domain.model.CngActor;
import my.gov.dosm.aesictec.identity.domain.model.CngActorType;

/**
 * @author canang technologies
 */
public interface CngActorDao extends GenericDao<Long, CngActor> {

    CngActor findByCode(String code);

    CngActor findByIdentityNo(String identityNo);

    CngActor findByEmail(String email);

    List<CngActor> find(String filter, Integer offset, Integer limit);

    List<CngActor> find(CngActorType type, Integer offset, Integer limit);

    List<CngActor> find(String filter, CngActorType type, Integer offset, Integer limit);

    Integer count(String filter);

    Integer count(String filter, CngActorType type);

    Integer count(CngActorType type);

}
