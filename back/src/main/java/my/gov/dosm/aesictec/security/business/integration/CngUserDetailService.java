package my.gov.dosm.aesictec.security.business.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import my.gov.dosm.aesictec.identity.domain.dao.CngUserDao;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalRole;

@Service("userDetailService")
@Transactional
public class CngUserDetailService implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(CngUserDetailService.class);

    @Autowired
    private CngUserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        CngUser user = userDao.findByUsername(username);
        LOG.debug("loading user: {} ", username);

        if (user == null)
            throw new UsernameNotFoundException("No such user");

        List<GrantedAuthority> authorityList = loadGrantedAuthoritiesFor(user);
        return new CngUserDetails(user, authorityList);
    }

    private List<GrantedAuthority> loadGrantedAuthoritiesFor(CngUser user) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        Set<CngPrincipalRole> roles = user.getRoles();
        for (CngPrincipalRole role : roles) {
            LOG.debug("role; " + role.getRole().name());
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole().name()));
        }
        return grantedAuthorities;
    }
}
