package my.gov.dosm.aesictec.security.business.integration;

import my.gov.dosm.aesictec.security.business.integration.model.NewsUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class NewssUserDetails implements UserDetails {

  private NewsUser user;
  private List<GrantedAuthority> grantedAuthorities;

  public NewssUserDetails() {
  }

  public NewssUserDetails(NewsUser user, List<GrantedAuthority> grantedAuthorities) {
    this.user = user;
    this.grantedAuthorities = grantedAuthorities;
  }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    return grantedAuthorities;
  }

  @Override
  public String getPassword() {
    return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUserName();
  }

  public boolean isAccountNonExpired() {
    return true;
  }

  public boolean isAccountNonLocked() {
    return true;
  }

  public boolean isCredentialsNonExpired() {
    return true;
  }

  public boolean isEnabled() {
    return true;
  }

  public NewsUser getUser() {
    return user;
  }

  public void setUser(NewsUser user) {
    this.user = user;
  }
}
