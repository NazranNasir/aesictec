package my.gov.dosm.aesictec.common.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import my.gov.dosm.aesictec.common.business.service.CommonService;
import my.gov.dosm.aesictec.system.business.service.SystemService;

/**
 */
@Transactional
@RestController
@RequestMapping("/api/common")
public class CommonController {

  private static final Logger LOG = LoggerFactory.getLogger(CommonController.class);

  private CommonService commonService;
  private SystemService systemService;
  private CommonTransformer commonTransformer;
  private AuthenticationManager authenticationManager;

  @Autowired
  public CommonController(CommonService commonService, SystemService systemService,
                          CommonTransformer commonTransformer,
                          AuthenticationManager authenticationManager) {
    this.commonService = commonService;
    this.systemService = systemService;
    this.commonTransformer = commonTransformer;
    this.authenticationManager = authenticationManager;
  }
}
