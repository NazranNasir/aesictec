package my.gov.dosm.aesictec.security.business.service;


import my.gov.dosm.aesictec.identity.domain.model.CngUser;

/**
 * @author canang technologies
 */
public interface SecurityService {

    CngUser getCurrentUser();
}
