package my.gov.dosm.aesictec.validation.api.vo;

/**
 * @author canang technologies
 */
public enum  EditSpecEvent {
  JP_VALIDATE,
  JP_APPROVE,
  SMD_APPROVE,
  SMD_SCHEDULE,
  SMD_FINALIZE
}
