package my.gov.dosm.aesictec.identity.domain.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import static my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType.GROUP;

/**
 * @author canang technologies
 */
@Entity(name = "CngGroup")
@Table(name = "CNG_GROP")
public class CngGroupImpl extends CngPrincipalImpl implements CngGroup {

    @ManyToMany(fetch = FetchType.LAZY, targetEntity = CngPrincipalImpl.class)
    @JoinTable(name = "CNG_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRINCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<CngPrincipal> members;

    public CngGroupImpl() {
        setPrincipalType(GROUP);
    }

    public Set<CngPrincipal> getMembers() {
        return members;
    }

    public void setMembers(Set<CngPrincipal> members) {

        this.members = members;
    }

    @Override
    public Class<?> getInterfaceClass() {

        return CngGroup.class;
    }

}
