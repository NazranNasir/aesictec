package my.gov.dosm.aesictec.validation.domain.dao;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Arrays;
import java.util.List;

/**
 * @author canang technologies
 */
@Repository("aesEditSpecDao")
public class AesEditSpecDaoImpl extends GenericDaoSupport<Long, AesEditSpec> implements AesEditSpecDao {

  public AesEditSpecDaoImpl() {
    super(AesEditSpecImpl.class);
  }

  @Override
  public void saveEditSpec(AesEditSpec aesEditSpec) {
    entityManager.persist(aesEditSpec);
  }

  @Override
  public void updateEditSpec(AesEditSpec aesEditSpec) {
    entityManager.merge(aesEditSpec);
  }

  @Override
  public List<AesEditSpecItem> findItems(AesEditSpec spec) {
    Query query = entityManager.createQuery("select i from AesEditSpecItem i where i.editSpec = :aesEditSpec");
    query.setParameter("aesEditSpec", spec);
    return query.getResultList();
  }

  @Override
  public boolean isFilled(AesEditSpec spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.editSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and i.response is not null");
    query.setParameter("aesEditSpec", spec);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotFilled(AesEditSpec spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.editSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and i.response is null");
    query.setParameter("aesEditSpec", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNegative(AesEditSpec spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.aesEditSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and cast(i.response as int) < 0");
    query.setParameter("aesEditSpec", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotNegative(AesEditSpec spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.aesEditSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and cast(i.response as int) < 0");
    query.setParameter("aesEditSpec", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isIn(AesEditSpec spec, String question, String[] values) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.editSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and i.response in :values");
    query.setParameter("aesEditSpec", spec);
    query.setParameter("question", question);
    query.setParameter("values",  Arrays.asList(values));
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotIn(AesEditSpec spec, String question, String[] values) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecItem i where " +
      "i.editSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and i.response not in :values");
    query.setParameter("aesEditSpec", spec);
    query.setParameter("question", question);
    query.setParameter("values",  Arrays.asList(values));
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public void addItem(AesEditSpec aesEditSpec, AesEditSpecItem item) {
    item.setEditSpec(aesEditSpec);
    entityManager.persist(item);
  }

  @Override
  public void updateItem(AesEditSpec aesEditSpec, String question, String response) {
    Query query = entityManager.createQuery("update AesEditSpecItem i set i.response = :response where " +
      "i.editSpec = :aesEditSpec " +
      "and i.question = :question " +
      "and i.response not in :values");
    query.setParameter("aesEditSpec", aesEditSpec);
    query.setParameter("question", question);
    query.setParameter("values", response);
    query.executeUpdate();
  }

  @Override
  public void deleteItem(AesEditSpec aesEditSpec, AesEditSpecItem item) {
    entityManager.remove(item);
  }
}
