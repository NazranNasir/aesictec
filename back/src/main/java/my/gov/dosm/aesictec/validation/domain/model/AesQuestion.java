package my.gov.dosm.aesictec.validation.domain.model;

/**
 * @author canang technologies
 */
public interface AesQuestion {

  String getFieldNo();

  void setFieldNo(String fieldNo);

  String getStatement();

  void setStatement(String statement);

//  AesQuestionType getType();
//
//  void setType(AesQuestionType type);

}

