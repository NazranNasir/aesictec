package my.gov.dosm.aesictec.identity.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import static my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType.USER;

/**
 * @author canang technologies
 */
@Entity(name = "CngUser")
@Table(name = "CNG_USER")
public class CngUserImpl extends CngPrincipalImpl implements CngUser {

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @NotNull
    @Column(name = "REAL_NAME", nullable = false)
    private String realName;

    @NotNull
    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @OneToOne(targetEntity = CngActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private CngActor actor;

    public CngUserImpl() {
        setPrincipalType(USER);
    }

    @Override
    public String getUsername() {
        return getName();
    }

    @Override
    public void setUsername(String username) {
        setName(username);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public CngActor getActor() {
        return actor;
    }

    @Override
    public void setActor(CngActor actor) {
        this.actor = actor;
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngUser.class;
    }

}
