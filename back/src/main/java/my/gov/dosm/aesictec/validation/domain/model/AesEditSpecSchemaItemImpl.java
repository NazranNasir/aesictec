package my.gov.dosm.aesictec.validation.domain.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author canang technologies
 */
@Entity(name = "AesEditSpecSchemaItem")
@Table(name = "AES_EDIT_SPEC_SCHM_ITEM")
public class AesEditSpecSchemaItemImpl implements AesEditSpecSchemaItem {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(generator = "SQ_AES_EDIT_SPEC_SCHM_ITEM")
  @SequenceGenerator(name = "SQ_AES_EDIT_SPEC_SCHM_ITEM", sequenceName = "SQ_AES_EDIT_SPEC_SCHM_ITEM", allocationSize = 1)
  private Long id;

  @Column(name = "QUESTION", nullable = false)
  private String question;

  @Column(name = "RULES", nullable = false)
  private String rules;

  @OneToOne(targetEntity = AesEditSpecSchemaImpl.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "EDIT_SPEC_SCHEMA_ID", nullable = false)
  private AesEditSpecSchema editSpecSchema;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getQuestion() {
    return question;
  }

  @Override
  public void setQuestion(String question) {
    this.question = question;
  }

  public String getRules() {
    return rules;
  }

  public void setRules(String rules) {
    this.rules = rules;
  }

  public AesEditSpecSchema getEditSpecSchema() {
    return editSpecSchema;
  }

  public void setEditSpecSchema(AesEditSpecSchema editSpecSchema) {
    this.editSpecSchema = editSpecSchema;
  }
}
