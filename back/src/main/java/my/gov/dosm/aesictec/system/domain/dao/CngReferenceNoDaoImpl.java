package my.gov.dosm.aesictec.system.domain.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.CngMetaState;
import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNo;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNoImpl;

/**
 * @author canang technologies
 */
@SuppressWarnings({"unchecked"})
@Repository("referenceNoDao")
public class CngReferenceNoDaoImpl extends GenericDaoSupport<Long, CngReferenceNo> implements CngReferenceNoDao {

    public CngReferenceNoDaoImpl() {
        super(CngReferenceNoImpl.class);
    }

    @Override
    public CngReferenceNo findByCode(String code) {
        Query query = entityManager.createQuery("select s from CngReferenceNo s where " +
                "s.code = :code and  " +
                " s.metadata.state = :state");
        query.setParameter("code", code);
        query.setParameter("state", CngMetaState.ACTIVE);
        return (CngReferenceNo) query.getSingleResult();
    }

    @Override
    public List<CngReferenceNo> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select s from CngReferenceNo s where " +
                "(upper(s.code) like upper(:filter)  " +
                "or upper(s.description) like upper(:filter))  " +
                "and s.metadata.state = :state");
        query.setParameter("filter", filter);
        query.setParameter("state", CngMetaState.ACTIVE);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(s) from CngReferenceNo s where " +
                "(upper(s.code) like upper(:filter)  " +
                "or upper(s.description) like upper(:filter))  " +
                "and s.metadata.state = :state");
        query.setParameter("state", CngMetaState.ACTIVE);
        return ((Long) query.getSingleResult()).intValue();
    }
}
