package my.gov.dosm.aesictec.common.business.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author canang technologies
 */
@Transactional
@Service("commonService")
public class CommonServiceImpl implements CommonService {

  private static final Logger LOG = LoggerFactory.getLogger(CommonServiceImpl.class);
}
