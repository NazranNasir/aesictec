package my.gov.dosm.aesictec.identity.domain.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.CngMetaState;
import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.identity.domain.model.CngActor;
import my.gov.dosm.aesictec.identity.domain.model.CngActorImpl;
import my.gov.dosm.aesictec.identity.domain.model.CngActorType;

/**
 * @author canang technologies
 */
@Repository("actorDao")
public class CngActorDaoImpl extends GenericDaoSupport<Long, CngActor> implements CngActorDao {

    public CngActorDaoImpl() {
        super(CngActorImpl.class);
    }

    @Override
    public CngActor findByCode(String code) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "a.code = :code");
        query.setParameter("code", code);
        return (CngActor) query.getSingleResult();
    }

    @Override
    public CngActor findByEmail(String email) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "a.email = :email");
        query.setParameter("email", email);
        return (CngActor) query.getSingleResult();
    }

    @Override
    public CngActor findByIdentityNo(String identityNo) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "a.identityNo = :identityNo");
        query.setParameter("identityNo", identityNo);
        return (CngActor) query.getSingleResult();
    }

    @Override
    public List<CngActor> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "(a.firstName like upper(:filter)" +
                "or a.lastName like upper(:filter)) " +
                "order by a.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return query.getResultList();
    }

    @Override
    public List<CngActor> find(CngActorType type, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "a.actorType = :actorType " +
                "order by a.name");
        query.setParameter("actorType", type);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public List<CngActor> find(String filter, CngActorType type, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select a from CngActor a where " +
                "(a.firsName like upper(:filter)" +
                "or a.lastName like upper(:filter)) " +
                "and a.actorType = :actorType " +
                "order by a.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("actorType", type);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(a) from CngActor a where " +
                "(upper(a.firstName) like upper(:filter)  " +
                "or upper(a.lastName) like upper(:filter))  " +
                "and a.metadata.state = :state");
        query.setParameter("state", CngMetaState.ACTIVE);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer count(String filter, CngActorType type) {
        Query query = entityManager.createQuery("select count(a) from CngActor a where " +
                "(upper(a.firstName) like upper(:filter)  " +
                "or upper(a.lastName) like upper(:filter))  " +
                "and a.actorType = :actorType " +
                "and a.metadata.state = :state");
        query.setParameter("state", CngMetaState.ACTIVE);
        query.setParameter("actorType", type);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer count(CngActorType type) {
        Query query = entityManager.createQuery("select count(a) from CngActor a where " +
                "a.actorType = :actorType " +
                "and a.metadata.state = :state");
        query.setParameter("state", CngMetaState.ACTIVE);
        query.setParameter("actorType", type);
        return ((Long) query.getSingleResult()).intValue();
    }
}
