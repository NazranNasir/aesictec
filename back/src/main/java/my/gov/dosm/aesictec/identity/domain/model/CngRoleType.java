package my.gov.dosm.aesictec.identity.domain.model;

/**
 * @author canang technologies
 */
public enum CngRoleType {
    ROLE_ADMINISTRATOR, // 0
    ROLE_USER,          // 1
    ROLE_GUEST;         // 2
}
