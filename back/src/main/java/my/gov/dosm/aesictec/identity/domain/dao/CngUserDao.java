package my.gov.dosm.aesictec.identity.domain.dao;

import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.identity.domain.model.CngGroup;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;

/**
 * @author canang technologies
 */
public interface CngUserDao extends GenericDao<Long, CngUser> {

    CngUser findByEmail(String email);

    CngUser findByUsername(String username);

    List<CngUser> find(String filter, Integer offset, Integer limit);

    List<CngGroup> findGroups(CngUser user);

    Integer count(String filter);

    boolean isExists(String username);
}
