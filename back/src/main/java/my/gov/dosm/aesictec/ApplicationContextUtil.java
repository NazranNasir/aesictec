package my.gov.dosm.aesictec;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@SuppressWarnings("ALL")
@Component
public class ApplicationContextUtil implements ApplicationContextAware {

  private static ApplicationContext applicationContext = null;

  public static <X> X getSpringBean(Class beanClass) {
    return (X) applicationContext.getBean(beanClass);
  }

  public static <X> X getSpringBean(Class beanClass, Object... args) {
    return (X) applicationContext.getBean(beanClass, args);
  }

  public static <X> X getSpringBean(String beanName) {
    return (X) applicationContext.getBean(beanName);
  }

  public static <X> X getSpringBean(String beanName, Object... args) {
    return (X) applicationContext.getBean(beanName, args);
  }

  public static void autoWiredBean(Object obj) {
    applicationContext.getAutowireCapableBeanFactory().autowireBean(obj);
  }

  @Override
  @Autowired
  public void setApplicationContext(ApplicationContext arg0)
    throws BeansException {
    applicationContext = arg0;
  }
}
