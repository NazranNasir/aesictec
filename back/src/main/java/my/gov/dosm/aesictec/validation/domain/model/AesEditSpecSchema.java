package my.gov.dosm.aesictec.validation.domain.model;


/**
 * @author canang technologies
 */
public interface AesEditSpecSchema {

  Long getId();

  void setId(Long id);

  String getCaseNo();

  void setCaseNo(String caseNo);
}
