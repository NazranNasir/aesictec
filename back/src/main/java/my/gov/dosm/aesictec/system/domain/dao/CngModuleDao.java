package my.gov.dosm.aesictec.system.domain.dao;


import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;


/**
 */
public interface CngModuleDao extends GenericDao<Long, CngModule> {

    CngModule findByCode(String code);

    Integer count(String filter);

    boolean isExists(String code);

    boolean isSubModuleExists(String code);

    void addSubModule(CngModule module, CngSubModule subModule, CngUser user);

    void updateSubModule(CngModule module, CngSubModule subModule, CngUser user);

    void removeSubModule(CngModule module, CngSubModule subModule, CngUser user);
}
