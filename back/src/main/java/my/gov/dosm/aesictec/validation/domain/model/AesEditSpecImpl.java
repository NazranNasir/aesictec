package my.gov.dosm.aesictec.validation.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * @author canang technologies
 */
@Entity(name = "AesEditSpec")
@Table(name = "AES_EDIT_SPEC")
public class AesEditSpecImpl implements AesEditSpec {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(generator = "SQ_AES_EDIT_SPEC")
  @SequenceGenerator(name = "SQ_AES_EDIT_SPEC", sequenceName = "SQ_AES_EDIT_SPEC", allocationSize = 1)
  private Long id;

  @Column(name = "CASE_NO", nullable = false)
  private String caseNo;

  @Enumerated(EnumType.ORDINAL)
  @Column(name = "STATE", nullable = false)
  private AesEditSpecState state;

  @OneToOne(targetEntity = AesEditSpecSchemaImpl.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "EDIT_SPEC_SCHEMA_ID", nullable = false)
  private AesEditSpecSchema editSpecSchema;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getCaseNo() {
    return caseNo;
  }

  @Override
  public void setCaseNo(String caseNo) {
    this.caseNo = caseNo;
  }

  @Override
  public AesEditSpecState getState() {
    return state;
  }

  @Override
  public void setState(AesEditSpecState state) {
    this.state = state;
  }

  @Override
  public AesEditSpecSchema getEditSpecSchema() {
    return editSpecSchema;
  }

  @Override
  public void setEditSpecSchema(AesEditSpecSchema editSpecSchema) {
    this.editSpecSchema = editSpecSchema;
  }
}
