package my.gov.dosm.aesictec.system.domain.model;


import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 */
public interface CngModule extends CngMetaObject {

    String getCode();

    void setCode(String code);

    String getCanonicalCode();

    void setCanonicalCode(String canonicalCode);

    String getDescription();

    void setDescription(String description);

    Integer getOrdinal();

    void setOrdinal(Integer ordinal);

    boolean isEnabled();

    void setEnabled(boolean enabled);
}
