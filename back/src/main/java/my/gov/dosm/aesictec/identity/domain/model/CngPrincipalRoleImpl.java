package my.gov.dosm.aesictec.identity.domain.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import my.gov.dosm.aesictec.core.domain.CngMetadata;

/**
 * @author canang technologies
 */
@Table(name = "CNG_PCPL_ROLE")
@Entity(name = "CngPrincipalRole")
public class CngPrincipalRoleImpl implements CngPrincipalRole {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SQ_CNG_PCPL_ROLE")
    @SequenceGenerator(name = "SQ_CNG_PCPL_ROLE", sequenceName = "SQ_CNG_PCPL_ROLE", allocationSize = 1)
    private Long id;

    @Column(name = "ROLE_TYPE")
    private CngRoleType role;

    @ManyToOne(targetEntity = CngPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private CngPrincipal principal;

    @Embedded
    private CngMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CngRoleType getRole() {
        return role;
    }

    public void setRole(CngRoleType role) {
        this.role = role;
    }

    public CngPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(CngPrincipal principal) {
        this.principal = principal;
    }

    public CngMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CngMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngPrincipalRole.class;
    }

}
