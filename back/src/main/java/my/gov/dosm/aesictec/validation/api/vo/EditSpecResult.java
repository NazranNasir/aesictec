package my.gov.dosm.aesictec.validation.api.vo;

import java.util.List;

/**
 * @author canang technologies
 */
public class EditSpecResult {

  private Integer totalSize;
  private List<EditSpec> result;

  public EditSpecResult(List<EditSpec> result, Integer totalSize) {
    this.totalSize = totalSize;
    this.result = result;
  }

  public Integer getTotalSize() {
    return totalSize;
  }

  public void setTotalSize(Integer totalSize) {
    this.totalSize = totalSize;
  }

  public List<EditSpec> getResult() {
    return result;
  }

  public void setResult(List<EditSpec> result) {
    this.result = result;
  }
}
