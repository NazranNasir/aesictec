package my.gov.dosm.aesictec.validation.domain.dao;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;

/**
 * @author canang technologies
 */
@Repository("aesEditSpecSchemaDao")
public class AesEditSpecSchemaDaoImpl extends GenericDaoSupport<Long, AesEditSpecSchema> implements AesEditSpecSchemaDao {

  public AesEditSpecSchemaDaoImpl() {
    super(AesEditSpecSchemaImpl.class);
  }

  @Override
  public AesEditSpecSchema findByCaseNo(String caseNo) {
    Query query = entityManager.createQuery("select i from AesEditSpecSchema i where " +
      "i.caseNo = :caseNo");
    query.setParameter("caseNo", caseNo);
    return (AesEditSpecSchema) query.getSingleResult();
  }

  @Override
  public AesEditSpecSchemaItem findItemByQuestion(AesEditSpecSchema editSpecSchema, String question) {
    Query query = entityManager.createQuery("select i from AesEditSpecSchemaItem i where " +
      "i.question = :question " +
      "and i.editSpecSchema = :schema");
    query.setParameter("question", question);
    query.setParameter("schema", editSpecSchema);
    return (AesEditSpecSchemaItem) query.getSingleResult();
  }

  @Override
  public void saveEditSpec(AesEditSpecSchema aesEditSpecSchema) {
    entityManager.persist(aesEditSpecSchema);
  }

  @Override
  public void updateEditSpec(AesEditSpecSchema aesEditSpecSchema) {
    entityManager.merge(aesEditSpecSchema);
  }

  @Override
  public List<AesEditSpecSchemaItem> findItems(AesEditSpecSchema spec) {
    Query query = entityManager.createQuery("select i from AesEditSpecSchemaItem i where i.editSpecSchema = :aesEditSpecSchema");
    query.setParameter("aesEditSpecSchema", spec);
    return query.getResultList();
  }

  @Override
  public boolean isFilled(AesEditSpecSchema spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.editSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and i.response is not null");
    query.setParameter("aesEditSpecSchema", spec);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotFilled(AesEditSpecSchema spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.editSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and i.response is null");
    query.setParameter("aesEditSpecSchema", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNegative(AesEditSpecSchema spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.aesEditSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and cast(i.response as int) < 0");
    query.setParameter("aesEditSpecSchema", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotNegative(AesEditSpecSchema spec, String question) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.aesEditSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and cast(i.response as int) < 0");
    query.setParameter("aesEditSpecSchema", spec);
    query.setParameter("question", question);
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isIn(AesEditSpecSchema spec, String question, String[] values) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.editSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and i.response in :values");
    query.setParameter("aesEditSpecSchema", spec);
    query.setParameter("question", question);
    query.setParameter("values", Arrays.asList(values));
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public boolean isNotIn(AesEditSpecSchema spec, String question, String[] values) {
    Query query = entityManager.createQuery("select count(i) from AesEditSpecSchemaItem i where " +
      "i.editSpecSchema = :aesEditSpecSchema " +
      "and i.question = :question " +
      "and i.response not in :values");
    query.setParameter("aesEditSpecSchema", spec);
    query.setParameter("question", question);
    query.setParameter("values", Arrays.asList(values));
    return 0 < ((Long) query.getSingleResult()).intValue();
  }

  @Override
  public void addItem(AesEditSpecSchema aesEditSpecSchema, AesEditSpecSchemaItem item) {
    item.setEditSpecSchema(aesEditSpecSchema);
    entityManager.persist(item);
  }

  @Override
  public void updateItem(AesEditSpecSchema aesEditSpecSchema, AesEditSpecSchemaItem item) {
    item.setEditSpecSchema(aesEditSpecSchema);
    entityManager.merge(item);
  }

  @Override
  public void deleteItem(AesEditSpecSchema aesEditSpecSchema, AesEditSpecSchemaItem item) {
    entityManager.remove(item);
  }
}
