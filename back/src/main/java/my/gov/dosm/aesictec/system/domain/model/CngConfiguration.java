package my.gov.dosm.aesictec.system.domain.model;

import java.math.BigDecimal;

import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 */
public interface CngConfiguration extends CngMetaObject {

    String getKey();

    void setKey(String value);

    String getValue();

    void setValue(String value);

    byte[] getValueByteArray();

    void setValueByteArray(byte[] value);

    String getDescription();

    void setDescription(String description);

    Integer getValueAsInteger();

    Double getValueAsDouble();

    Long getValueAsLong();

    BigDecimal getValueAsBigDecimal();

    Boolean getValueAsBoolean();

}
