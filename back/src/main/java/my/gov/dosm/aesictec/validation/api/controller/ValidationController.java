package my.gov.dosm.aesictec.validation.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import my.gov.dosm.aesictec.common.api.controller.CommonTransformer;
import my.gov.dosm.aesictec.common.business.service.CommonService;
import my.gov.dosm.aesictec.identity.api.controller.IdentityController;
import my.gov.dosm.aesictec.identity.api.controller.IdentityTransformer;
import my.gov.dosm.aesictec.identity.business.service.IdentityService;
import my.gov.dosm.aesictec.security.business.service.SecurityService;
import my.gov.dosm.aesictec.system.business.service.SystemService;
import my.gov.dosm.aesictec.validation.api.vo.EditSpec;
import my.gov.dosm.aesictec.validation.api.vo.EditSpecResult;
import my.gov.dosm.aesictec.validation.business.rules.ValidationResult;
import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;

import static my.gov.dosm.aesictec.Constants.LIMIT;

/**
 * @author canang technologies
 */
@RestController
@Transactional
@RequestMapping("/api/validation")
public class ValidationController {

  private static final Logger LOG = LoggerFactory.getLogger(IdentityController.class);

  private IdentityService identityService;
  private ValidationService validationService;
  private CommonService commonService;
  private SystemService systemService;
  private SecurityService securityService;
  private ValidationTransformer validationTransformer;
  private IdentityTransformer identityTransformer;
  private CommonTransformer commonTransformer;
  private AuthenticationManager authenticationManager;


  public ValidationController(IdentityService identityService, ValidationService validationService,
                              CommonService commonService, SystemService systemService,
                              SecurityService securityService, ValidationTransformer validationTransformer,
                              IdentityTransformer identityTransformer,
                              CommonTransformer commonTransformer, AuthenticationManager authenticationManager) {
    this.identityService = identityService;
    this.validationService = validationService;
    this.commonService = commonService;
    this.systemService = systemService;
    this.securityService = securityService;
    this.validationTransformer = validationTransformer;
    this.identityTransformer = identityTransformer;
    this.commonTransformer = commonTransformer;
    this.authenticationManager = authenticationManager;
  }

  //==============================================================================================
  // USER
  //==============================================================================================

  // todo: use state to query
  @GetMapping(value = "/edit-specs", params = {"page", "state"})
  public ResponseEntity<EditSpecResult> findEditSpecs(@RequestParam Integer page, @RequestParam String state) {
    Integer count = validationService.countEditSpec();
    List<AesEditSpec> specs = validationService.findEditSpecs((page - 1) * LIMIT, LIMIT);
    List<EditSpec> specVos = validationTransformer.toEditSpecVos(specs);
    return new ResponseEntity<EditSpecResult>(new EditSpecResult(specVos, count), HttpStatus.OK);
  }

  @GetMapping(value = "/edit-specs/{id}")
  public ResponseEntity<EditSpec> findUserByUsername(@PathVariable Long id) {
    AesEditSpec editSpec = validationService.findEditSpecById(id);
    EditSpec editSpecVo = validationTransformer.toEditSpecVo(editSpec);
    return new ResponseEntity<EditSpec>(editSpecVo, HttpStatus.OK);
  }

  @PutMapping(value = "/edit-specs/{id}")
  public ResponseEntity<String> updateEditSpec(@PathVariable Long id, @PathVariable EditSpec vo) {
    AesEditSpec editSpec = validationService.findEditSpecById(id);
    Map<String, String> values = vo.getValues();
    for (String question : values.keySet()) {
      validationService.updateEditSpecItem(editSpec, question, (String) values.get(question));
    }
    return new ResponseEntity<String>("Success", HttpStatus.OK);
  }

  @PutMapping(value = "/edit-specs/{id}/validate")
  public ResponseEntity<String> validateEditSpec(@PathVariable Long id) {
    AesEditSpec editSpec = validationService.findEditSpecById(id);
    // todo
    return new ResponseEntity<String>("Success", HttpStatus.OK);
  }
}
