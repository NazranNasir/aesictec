package my.gov.dosm.aesictec.validation.domain.model;

/**
 * @author canang technologies
 */
public enum AesEditSpecEvent {

  JP_VALIDATE,
  JP_APPROVE,
  SMD_APPROVE,
  SMD_SCHEDULE,
  SMD_FINALIZE
}
