package my.gov.dosm.aesictec.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
@ComponentScan(basePackages = {
  // internals
  "my.gov.dosm.aesictec.config",
  "my.gov.dosm.aesictec.identity",
  "my.gov.dosm.aesictec.security",
  "my.gov.dosm.aesictec.system",
  "my.gov.dosm.aesictec.common",

  // modules
  "my.gov.dosm.aesictec.validation",

  //report vo
})
@EntityScan(basePackages = {
  "my.gov.dosm.aesictec"
})

@Import({
  SecurityConfig.class,
  SwaggerConfig.class,
  AuthorizationServerConfig.class,
  ResourceServerConfig.class,
  CorsConfig.class,
  CacheConfig.class,
  WebConfig.class,
  EditSpecStateMachineConfig.class,
  EditSpecRulesConfig.class
})

public class ApplicationConfig {

  @Configuration
  @Profile("default")
  @PropertySource({"classpath:application-default.properties"})
  static class Default {
    // nothing needed here if you are only overriding property values
  }

  @Configuration
  @Profile("local")
  @PropertySource({"classpath:application-local.properties"})
  static class Local {
    // nothing needed here if you are only overriding property values
  }

}
