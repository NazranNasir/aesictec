package my.gov.dosm.aesictec.identity.domain.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.identity.domain.model.CngGroup;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngUserImpl;

/**
 * @author canang technologies
 */
@Repository("userDao")
public class CngUserDaoImpl extends GenericDaoSupport<Long, CngUser> implements CngUserDao {

    private static final Logger LOG = LoggerFactory.getLogger(CngUserDaoImpl.class);

    public CngUserDaoImpl() {
        super(CngUserImpl.class);
    }

    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<CngGroup> findGroups(CngUser user) {
        Query query = entityManager.createQuery("select r from CngGroup r inner join r.members m where m.id = :id");
        query.setParameter("id", user.getId());
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public boolean isExists(String username) {
        Query query = entityManager.createQuery("select count(u) from CngUser u where " +
                "upper(u.name) = upper(:username) ");
        query.setParameter("username", username);
        return 0 < ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public CngUser findByEmail(String email) {
        Query query = entityManager.createQuery("select u from CngUser u where u.email = :email ");
        query.setParameter("email", email);
        return (CngUser) query.getSingleResult();
    }

    @Override
    public CngUser findByUsername(String username) {
        Query query = entityManager.createQuery("select u from CngUser u where u.name = :username ");
        query.setParameter("username", username);
        return (CngUser) query.getSingleResult();
    }

    @Override
    public List<CngUser> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select s from CngUser s where (" +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.realName) like upper(:filter)) " +
                "order by s.realName, s.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<CngUser>) query.getResultList();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(s) from CngUser s where " +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.realName) like upper(:filter)");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.getSingleResult()).intValue();
    }
}
