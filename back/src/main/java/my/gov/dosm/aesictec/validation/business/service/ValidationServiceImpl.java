package my.gov.dosm.aesictec.validation.business.service;

import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.NameValueReferableMap;
import com.deliveredtechnologies.rulebook.model.Rule;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.model.rulechain.cor.CoRRuleBook;

import my.gov.dosm.aesictec.validation.business.rules.ValidationResult;
import my.gov.dosm.aesictec.validation.domain.dao.AesEditSpecDao;
import my.gov.dosm.aesictec.validation.domain.dao.AesEditSpecSchemaDao;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecEvent;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.scripting.bsh.BshScriptEvaluator;
import org.springframework.scripting.support.StaticScriptSource;
import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author canang technologies
 */
@Service("validationService")
@Transactional
public class ValidationServiceImpl implements ValidationService {

  private static final Logger LOG = LoggerFactory.getLogger(ValidationServiceImpl.class);

  @Autowired
  private AesEditSpecSchemaDao editSpecSchemaDao;

  @Autowired
  private AesEditSpecDao editSpecDao;

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private BshScriptEvaluator scriptEvaluator;

  // ===============================================================================================
  // EDIT SPEC SCHEMA
  // ===============================================================================================

  @Override
  public AesEditSpecSchema findEditSpecSchemaById(Long id) {
    return editSpecSchemaDao.findById(id);
  }

  @Override
  public AesEditSpecSchema findEditSpecSchemaByCaseNo(String caseNo) {
    return editSpecSchemaDao.findByCaseNo(caseNo);
  }

  @Override
  public List<AesEditSpecSchema> findEditSpecSchemas() {
    return editSpecSchemaDao.find();
  }

  @Override
  public List<AesEditSpecSchema> findEditSpecSchemas(Integer offset, Integer limit) {
    return editSpecSchemaDao.find(offset, limit);
  }

  @Override
  public List<AesEditSpecSchemaItem> findEditSpecSchemaItem(AesEditSpecSchema e) {
    return editSpecSchemaDao.findItems(e);
  }

  @Override
  public Integer countEditSpecSchema() {
    return editSpecSchemaDao.count();
  }

  @Override
  public void saveEditSpecSchema(AesEditSpecSchema editSpec) {
    editSpecSchemaDao.saveEditSpec(editSpec);
    entityManager.flush();
  }

  @Override
  public void updateEditSpecSchema(AesEditSpecSchema editSpec) {
    editSpecSchemaDao.updateEditSpec(editSpec);
    entityManager.flush();
  }

  @Override
  public void addEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item) {
    editSpecSchemaDao.addItem(spec, item);
    entityManager.flush();
  }

  @Override
  public void updateEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item) {
    editSpecSchemaDao.updateItem(spec, item);
    entityManager.flush();
  }

  @Override
  public void deleteEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item) {
    editSpecSchemaDao.deleteItem(spec, item);
    entityManager.flush();
  }

  // ===============================================================================================
  // EDIT SPEC
  // ===============================================================================================

  @Override
  public AesEditSpec findEditSpecById(Long id) {
    return editSpecDao.findById(id);
  }

  @Override
  public List<AesEditSpec> findEditSpecs() {
    return editSpecDao.find();
  }

  @Override
  public List<AesEditSpec> findEditSpecs(Integer offset, Integer limit) {
    return editSpecDao.find(offset, limit);
  }

  @Override
  public List<AesEditSpecItem> findEditSpecItem(AesEditSpec spec) {
    return editSpecDao.findItems(spec);
  }

  @Override
  public Integer countEditSpec() {
    return editSpecDao.count();
  }

  @Override
  public void saveEditSpec(AesEditSpec editSpec) {
    editSpecDao.saveEditSpec(editSpec);
  }

  @Override
  public void updateEditSpec(AesEditSpec editSpec) {
    editSpecDao.updateEditSpec(editSpec);
  }

  @Override
  public void addEditSpecItem(AesEditSpec spec, AesEditSpecItem item) {
    editSpecDao.addItem(spec, item);
    entityManager.flush();
  }

  @Override
  public void updateEditSpecItem(AesEditSpec editSpec, String question, String response) {
    editSpecDao.updateItem(editSpec, question, response);
    entityManager.flush();
  }

  @Override
  public void deleteEditSpecItem(AesEditSpec spec, AesEditSpecItem item) {
    editSpecDao.deleteItem(spec, item);
    entityManager.flush();
  }

  @Override
  public boolean isFilled(AesEditSpec editSpec, String question) {
    return editSpecDao.isFilled(editSpec, question);
  }

  @Override
  public boolean isNotFilled(AesEditSpec editSpec, String question) {
    return editSpecDao.isNotFilled(editSpec, question);
  }

  @Override
  public boolean isNegative(AesEditSpec editSpec, String question) {
    return editSpecDao.isNegative(editSpec, question);
  }

  @Override
  public boolean isNotNegative(AesEditSpec editSpec, String question) {
    return editSpecDao.isNotNegative(editSpec, question);
  }

  @Override
  public boolean isIn(AesEditSpec editSpec, String question, String[] values) {
    return editSpecDao.isIn(editSpec, question, values);
  }

  @Override
  public boolean isNotIn(AesEditSpec editSpec, String question, String[] values) {
    return editSpecDao.isNotIn(editSpec, question, values);
  }

  @Override
  public void runRules(AesEditSpec spec) {
    // do for each item
    List<AesEditSpecItem> items = findEditSpecItems(spec);
    for (AesEditSpecItem item : items) {
      // find rule in schema
      AesEditSpecSchemaItem editSpecSchemaItem = findEditSpecSchemaItem(spec.getEditSpecSchema(), item.getQuestion());
      NameValueReferableMap refMap = new FactMap();
      refMap.setValue("editSpec", spec);
      refMap.setValue(item.getQuestion(), item.getResponse());
      CoRRuleBook<String> coRRuleBook = new CoRRuleBook<String>() {
        @Override
        public void defineRules() {
          String rules = editSpecSchemaItem.getRules();
          LOG.debug("rules: {}", rules);
          if (null != rules && rules.length() > 0) {
            StaticScriptSource source = new StaticScriptSource(rules);
            Map<String, Object> arguments = new HashMap<String, Object>();
            arguments.put("validationService", this);
            Rule<AesEditSpec, String> rule = (Rule<AesEditSpec, String>) scriptEvaluator.evaluate(source, arguments);
            addRule(rule);
          }
        }
      };

      coRRuleBook.run(refMap);
      String val = coRRuleBook.getResult().get().getValue();
      LOG.debug(val);
      // todo: save this in error ifPresents
    }
  }

  private AesEditSpecSchemaItem findEditSpecSchemaItem(AesEditSpecSchema editSpecSchema, String question) {
    return editSpecSchemaDao.findItemByQuestion(editSpecSchema, question);
  }

  public List<AesEditSpecItem> findEditSpecItems(AesEditSpec editSpec) {
    return editSpecDao.findItems(editSpec);
  }

  @Override
  public ValidationResult runRules(AesEditSpec spec, RuleBook<ValidationResult> ruleBook) {
    List<AesEditSpecItem> items = findEditSpecItems(spec);
    NameValueReferableMap refMap = new FactMap();
    refMap.setValue("editSpec", spec);
    for (AesEditSpecItem item : items) {
      refMap.setValue(item.getQuestion(), item.getResponse());
    }
    ruleBook.run(refMap);
    return ruleBook.getResult().get().getValue();
  }


  @Override
  public void onStateChange(State<AesEditSpecState, AesEditSpecEvent> state, Message<AesEditSpecEvent> message) {
    Long id = message.getHeaders().get("editSpecId", Long.class);
    LOG.debug("state change for object id : {}", id);
    AesEditSpec editSpec = editSpecDao.findById(id);

    LOG.debug("From {} To {}", editSpec.getState(), state.getId());

    editSpec.setState(state.getId());
    editSpecDao.updateEditSpec(editSpec);
    entityManager.flush();
    LOG.trace("flush");
  }
}
