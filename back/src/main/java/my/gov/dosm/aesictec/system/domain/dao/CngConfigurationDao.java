package my.gov.dosm.aesictec.system.domain.dao;


import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.system.domain.model.CngConfiguration;


/**
 */
public interface CngConfigurationDao extends GenericDao<Long, CngConfiguration> {

    CngConfiguration findByKey(String key);

    List<CngConfiguration> findByPrefix(String prefix);

    List<CngConfiguration> find(String filter, Integer offset, Integer limit);

    List<CngConfiguration> find(String filter);

    List<CngConfiguration> findInverse(String filter);

    Integer count(String filter);

}
