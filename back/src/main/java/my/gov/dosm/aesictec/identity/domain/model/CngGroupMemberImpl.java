package my.gov.dosm.aesictec.identity.domain.model;


import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import my.gov.dosm.aesictec.core.domain.CngMetadata;

/**
 * @author canang technologies
 */
@Entity(name = "CngGroupMember")
@Table(name = "CNG_GROP_MMBR")
public class CngGroupMemberImpl implements CngGroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SQ_CNG_GROP_MMBR")
    @SequenceGenerator(name = "SQ_CNG_GROP_MMBR", sequenceName = "SQ_CNG_GROP_MMBR", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = CngGroupImpl.class)
    @JoinColumn(name = "GROUP_ID")
    private CngGroup group;

    @OneToOne(targetEntity = CngPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private CngPrincipal principal;

    @Embedded
    private CngMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CngGroup getGroup() {
        return group;
    }

    public void setGroup(CngGroup group) {
        this.group = group;
    }

    public CngPrincipal getPrincipal() {
        return principal;
    }

    public void setPrincipal(CngPrincipal principal) {
        this.principal = principal;
    }

    public CngMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CngMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngGroupMember.class;
    }


}
