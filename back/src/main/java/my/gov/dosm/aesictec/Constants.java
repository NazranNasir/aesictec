package my.gov.dosm.aesictec;

/**
 * constant lives here
 *
 * @author canang technologies
 */
public interface Constants {

    // constants lives here
    public static final String APPLICATION = "application";
    public static final String APPLICATION_NAME = "Profile";

    public static final int LIMIT = 10;
}
