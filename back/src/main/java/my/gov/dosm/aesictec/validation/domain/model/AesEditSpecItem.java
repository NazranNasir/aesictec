package my.gov.dosm.aesictec.validation.domain.model;

import java.util.Date;

/**
 * @author canang technologies
 */
public interface AesEditSpecItem {

  String getQuestion();

  void setQuestion(String question);

  String getResponse();

  void setResponse(String response);

  String getError();

  void setError(String error);

  Integer getResponseAsInteger();

  Date getResponseAsDate();

  AesEditSpec getEditSpec();

  void setEditSpec(AesEditSpec spec);
}

