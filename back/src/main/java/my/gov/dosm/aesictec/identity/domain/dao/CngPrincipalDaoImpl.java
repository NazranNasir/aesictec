package my.gov.dosm.aesictec.identity.domain.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.CngMetadata;
import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipal;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalImpl;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalRole;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;

import static my.gov.dosm.aesictec.core.domain.CngMetaState.ACTIVE;


/**
 * @author canang technologies
 */
@Repository("principalDao")
public class CngPrincipalDaoImpl extends GenericDaoSupport<Long, CngPrincipal> implements CngPrincipalDao {

    private static final Logger LOG = LoggerFactory.getLogger(CngPrincipalDaoImpl.class);

    public CngPrincipalDaoImpl() {
        super(CngPrincipalImpl.class);
    }

    @Override
    public List<CngPrincipal> findAllPrincipals() {
        List<CngPrincipal> results = new ArrayList<CngPrincipal>();
        Query query = entityManager.createQuery("select p from CngUser p order by p.name");
        results.addAll((List<CngPrincipal>) query.getResultList());

        Query queryGroup = entityManager.createQuery("select p from CngGroup p order by p.name ");
        results.addAll((List<CngPrincipal>) queryGroup.getResultList());

        return results;
    }

    @Override
    public List<CngPrincipal> find(String filter) {
        Query query = entityManager.createQuery("select p from CngPrincipal p where p.name like :filter order by p.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return query.getResultList();
    }

    @Override
    public List<CngPrincipal> find(String filter, CngPrincipalType type) {
        Query query = entityManager.createQuery("select p from CngPrincipal p where " +
                "p.name like :filter " +
                "and p.principalType = :principalType " +
                "order by p.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("principalType", type);
        return query.getResultList();
    }

    @Override
    public List<CngPrincipal> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select p from CngPrincipal p where " +
                "p.id in (" +
                "select u.id from CngUser u where " +
                "(upper(u.name) like upper(:filter)" +
                "or upper(u.realName) like upper(:filter))" +
                ") " +
                "or p.id in (select g.id from CngGroup g  where upper(g.name) like upper(:filter)) " +
                "order by p.name");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public void addRole(CngPrincipal principal, CngPrincipalRole role, CngUser user) {
        role.setPrincipal(principal);

        // prepare metadata
        CngMetadata metadata = new CngMetadata();
        metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setCreatorId(user.getId());
        metadata.setState(ACTIVE);
        role.setMetadata(metadata);
        entityManager.persist(role);
    }

    @Override
    public void deleteRole(CngPrincipal principal, CngPrincipalRole role, CngUser user) {
        entityManager.remove(role);
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(u) from CngPrincipal u where " +
                "u.name like :filter ");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return ((Long)query.getSingleResult()).intValue();
    }

    @Override
    public CngPrincipal findByName(String name) {
        Query query = entityManager.createQuery("select p from CngPrincipal p where " +
                "upper(p.name) = upper(:name) ");
        query.setParameter("name", name);
        return (CngPrincipal) query.getSingleResult();
    }
}
