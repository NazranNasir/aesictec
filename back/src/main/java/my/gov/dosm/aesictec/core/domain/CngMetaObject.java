package my.gov.dosm.aesictec.core.domain;

import java.io.Serializable;

/**
 * @author canang technologies
 * @since 1/27/14
 */
public interface CngMetaObject extends Serializable {

    /**
     * entity id
     *
     * @return
     */
    Long getId();

    /**
     * metadata
     *
     * @return
     */
    CngMetadata getMetadata();

    void setMetadata(CngMetadata metadata);

    /**
     * implementing interface
     *
     * @return
     */
    Class<?> getInterfaceClass();
}

