package my.gov.dosm.aesictec.security.business.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import my.gov.dosm.aesictec.identity.domain.dao.CngUserDao;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;

/**
 * @author canang technologies
 */
@Transactional
@Service("securityService")
public class SecurityServiceImpl implements SecurityService {

    private static final Logger LOG = LoggerFactory.getLogger(SecurityServiceImpl.class);

    private EntityManager entityManager;
    private ApplicationContext applicationContext;
    private CngUserDao userDao;

    @Autowired
    public SecurityServiceImpl(EntityManager entityManager, ApplicationContext applicationContext,
                               CngUserDao userDao) {
        this.entityManager = entityManager;
        this.applicationContext = applicationContext;
        this.userDao = userDao;
    }

    @Override
    public CngUser getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof UserDetails) {
            return userDao.findByUsername(((UserDetails) auth.getPrincipal()).getUsername());
        } else if (auth.getPrincipal() instanceof User) {
            return userDao.findByUsername(((User) auth.getPrincipal()).getUsername());
        } else if (auth.getPrincipal() instanceof String) {
            return userDao.findByUsername((String) auth.getPrincipal());
        } else {
            return null;
        }
    }
}
