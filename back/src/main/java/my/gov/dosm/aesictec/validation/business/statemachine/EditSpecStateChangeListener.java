package my.gov.dosm.aesictec.validation.business.statemachine;

import org.springframework.messaging.Message;
import org.springframework.statemachine.state.State;

import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecEvent;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;

/**
 * @author canang technologies
 */
public interface EditSpecStateChangeListener {

  void onStateChange(State<AesEditSpecState,AesEditSpecEvent> state, Message<AesEditSpecEvent> message);

}
