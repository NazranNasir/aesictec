package my.gov.dosm.aesictec.identity.domain.model;

import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 * @author canang technologies
 */
public interface CngPrincipalRole extends CngMetaObject {

    CngPrincipal getPrincipal();

    void setPrincipal(CngPrincipal principal);

    CngRoleType getRole();

    void setRole(CngRoleType role);
}
