package my.gov.dosm.aesictec.identity.api.vo;

/**
 * @author canang technologies
 */
public class Staff extends Actor {
    private String realName;

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

}
