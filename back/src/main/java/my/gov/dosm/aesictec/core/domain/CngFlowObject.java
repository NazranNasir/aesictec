package my.gov.dosm.aesictec.core.domain;

/**
 * @author canang technologies
 * @since 4/2/2016.
 */
public interface CngFlowObject extends CngMetaObject {
    /**
     * get flow data
     *
     * @return
     */
    CngFlowdata getFlowdata();

    /**
     * set flow data
     *
     * @param flowdata
     */
    void setFlowdata(CngFlowdata flowdata);

}
