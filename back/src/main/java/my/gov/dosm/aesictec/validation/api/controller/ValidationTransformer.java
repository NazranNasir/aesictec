package my.gov.dosm.aesictec.validation.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import my.gov.dosm.aesictec.common.api.controller.CommonTransformer;
import my.gov.dosm.aesictec.identity.api.controller.IdentityTransformer;
import my.gov.dosm.aesictec.validation.api.vo.EditSpec;
import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;

/**
 * @author canang technologies
 */
@Component("validationTransformer")
public class ValidationTransformer {

  private static final Logger LOG = LoggerFactory.getLogger(IdentityTransformer.class);

  private CommonTransformer commonTransformer;
  private ValidationService validationService;

  @Autowired
  public ValidationTransformer(CommonTransformer commonTransformer,
                               ValidationService validationService) {
    this.commonTransformer = commonTransformer;
    this.validationService = validationService;
  }

  //==============================================================================================
  // PRINCIPAL
  //==============================================================================================

  public EditSpec toEditSpecVo(AesEditSpec e) {
    if (null == e) return null;
    EditSpec vo = new EditSpec();
    vo.setId(e.getId());
    vo.setValues(toMap(validationService.findEditSpecItem(e)));
    return vo;
  }

  public List<EditSpec> toEditSpecVos(List<AesEditSpec> e) {
    List<EditSpec> vos = e.stream()
      .map((e1) -> toEditSpecVo(e1))
      .collect(Collectors.toList());
    return vos;
  }

  public Map<String, String> toMap(List<AesEditSpecItem> e) {
    Map<String, String> vos = e.stream()
      .collect(Collectors.toMap(AesEditSpecItem::getQuestion, AesEditSpecItem::getResponse));
    return vos;
  }
}
