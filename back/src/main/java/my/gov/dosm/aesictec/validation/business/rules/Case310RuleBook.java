package my.gov.dosm.aesictec.validation.business.rules;

import com.deliveredtechnologies.rulebook.lang.RuleBookBuilder;
import com.deliveredtechnologies.rulebook.lang.RuleBuilder;
import com.deliveredtechnologies.rulebook.model.Rule;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.model.rulechain.cor.CoRRuleBook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;

/**
 * @author canang technologies
 */
//Soalan 1
//  The Survey Code is 411.
//  The establishment number must match with the number in the survey frame as installed in the data capture programming system for each survey.
//  All the fields must NOT BE NEGATIVE except (Q3) F030002 can be negative.
//  F010002 must be filled.
//  F010028 must be filled.
//  F010029 must be filled. (This field refers to MSIC code in mailing label).
//  F010030 must be filled.
//  Soalan 2
//  F020001 must be filled either 1, 2, 3, 4, 5, 6, 7 or 9.
//  If F020001 is 1, 2, 3 or 9 then F020002 must be 1 or 2.
//  If F020001 is 4, 5, 6 or 7, then F020002 must NOT be filled.
//  Soalan 3
//  F030001 must be ≥ 0.
//  F030003 + F030004 + F030005 + F030006 + F030007 + F030008 + F030009 + F030010 + F030011 = 100.
//  If F020001 is 1, 2 or 7, then F030010 must NOT be filled.
//  If F020001 is 5, then F030011 must not be filled.
//  If F020001 is 6, then F030010 must = 100.
//  If F030011 > 50%, then F030012 must be filled with 3 digits.
//  If F030011 > 50%, then F030013 must be filled.
//  Soalan 4A
//  F310001 must be filled either 1 or 2.
//  F310081 must be > 0.
//  If F310001 is 1, then F310002 must be within in range of 1 to 100.
//  If F310001 is 2, then F310002 must NOT be filled.
//  F310003 must be filled either 1 or 2.
//  If F310004, F310005, F310006, F310007, F310008, F310009, F310010, F310011, F310012, F310013, F310014, F310015, F310016, F310021, F310022, F310023, F310024, F310025, F310026, F310027, F310028, F310029, F310030, F310031, F310032, F310033, F310034 or F310035 is filled then F310003 = 1.
//  If F310003 = 1, then F310004 must be within in range 1 to 100.
//  If F310003 = 1, then F310005, F3100306, F310007, F310008, F310009 or F310010 must be filled.
//  If F310005, F310006, F310007, F310008 or F310009 is filled, then F310010 must NOT be filled.
//  If F310010 = 1, then F310005, F310006, F310007, F310008 and F310009 must NOT be filled.
//  If F310003 = 1, then F310011 or F310012 or F310013 must be filled.
//  If F310013 = 1, then F310014, F310015 or F310016 must be filled.
//  If F310003 = 1, then F310017 must be filled.
//  If F310017 = 1, then F310018 or F310019 or F310020 must be filled..
//  If F310017 = 2, then F310018, F310019 and F310020 must NOT be filled..
//  If F310003 = 1, then F310021, F310022, F310023, F310024, F310025, F310026, F310027, F310028, F310029, F310030, F310031, F310032 or F310033 must be filled..
//  If F310003 = 1, then F310034 must be filled either 1 or 2.
//  If F310003 = 1, then F310035 must be filled either 1 or 2.
//  Soalan 4B
//  F310036 must be filled either 1 or 2.
//  If F310037, F310038, F310039, F310040, F310041, F310042, F310043, F310044, F310045, F310046, F310047, F310048, F310049, F310050, F310051, F310052, F310053, F310054, F310055, F310056, F310057, F310058, F310059, F310060, F310061, F310062, F310063, F310064, F310065, F310066, F310067, F310068, F310069, F310070, F310071, F310072, F310073, F310074, F310075, F310076, F310077, F310078, F310079 or F310080 = 1, THEN F310036 = 1.
//  If F310036 = 1, then F310037, F310038, F310039, F310040, F310041 or F310042 must be filled.
//  F310043 must be filled either 1 or 2.
//  F310043 = 1, THEN F080089 must be > 0.
//  If F310043 is 1, then F310044 or F310045 must be filled.
//  If F310045 is filled, then must be within range 1 to 100.
//  If F310045 > 0 AND F310044 > 0, then F310044 = (F310045 x F080089)/100.
//  If F310044 > 0, then F310044 ≤ F080089.
//  If F310043 = 1, then (F310046 + F310047 + F310048 + F310049)= 100.
//  If F310049 is > 0, then F310050 must be filled .
//  If F310050 is filled then F310049 > 0.
//  If F080089, F310044, F310045, F310046, F310047, F310048, F310049, F310050, F310051, F310052, F310053, F310054, F310055, F310056, F310057 or F310058 is filled then F310043 = 1.
//  If F310043 is 1, then, (F310051 + F310052 + F310053) = 100.
//  If F310043 is 1, then, (F310054 + F310055) = 100.
//  If F310055 ≥ 0, then, F310056, F310057 or F310058 must be filled..
//  If F310056 is filled then F310059 must be filled.
//  If F310057 is filled then F310060 must be filled.
//  If F310058 is filled then F310061 must be filled.
//  F310062 must be 1 or 2.
//  If F310063, F310064, F310065, F310066, F310067, F310068, F310069, F310070, F310071 or F310072 is filled then F310062 = 1 .
//  If F310043 = 1, THEN F090089 must be > 0.
//  If F310062 = 1, then F310063 or F310064 must be filled.
//  If F310064 is filled, then F310064 must be within range 1 to 100.
//  If F310064 > 0 AND F310063 > 0, then F310063= (F310064 x F090089)/100.
//  If F310063 > 0, then F310063 ≤ F090089.
//  If F310062 = 1 then, (F310065 + F310066 + F310067) = 100.
//  If F310062 = 1 then (F310068 + F310069) = 100.
//  If F310069 ≥ 0, then, F310070, F310071 or F310072 must be filled..
//  If F310070 is filled then F310073 must be filled.
//  If F310071 is filled then F310074 must be filled.
//  If F310072 is filled then F310075 must be filled.
//  F310076 must be filled either 1 or 2.
//  If 310076 is 1, then F310077 AND 310079 must be > 0.
//  If F310076 is 1, then F310078 OR F310080 must be within range 1 to 100.
@Component("case310RuleBook")
public class Case310RuleBook extends CoRRuleBook<ValidationResult> {

  @Autowired
  private ValidationService validationService;

  @Override
  public void defineRules() {
    setDefaultResult(new ValidationResult());

    // =============================================================================================
    // SOALAN 1
    // =============================================================================================
    Rule<AesEditSpec, String> rule = null;
      RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(String.class)
      .when(facts -> validationService.isNotFilled(facts.getValue("editSpec"), "F010002"))
      .then((facts, result) -> result.setValue("F010002 is not filled"))
      .build();
//    addRule(build);

    addRule(RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
        .when(facts -> validationService.isNotFilled(facts.getValue("editSpec"), "F010028"))
        .then((facts, result) -> result.getValue().add("F010028 is not filled"))
      .build());

    addRule(RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
      .when(facts -> validationService.isNotFilled(facts.getValue("editSpec"), "F010029"))
      .then((facts, result) -> result.getValue().add("F010029 is not filled"))
      .build());

    addRule(RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
        .when(facts -> validationService.isNotFilled(facts.getValue("editSpec"), "F010030"))
        .then((facts, result) -> result.getValue().add("F010030 is not filled"))
      .build());

    // =============================================================================================
    // SOALAN 2
    // =============================================================================================
    Rule<AesEditSpec, ValidationResult> editSpec =
      RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
      .when(facts -> validationService.isNotIn(facts.getValue("editSpec"), "F020001 ", new String[]{"1", "2", "3", "4", "5", "6", "7", "9"}))
      .then((facts, result) -> result.getValue().add("F020001 must be filled either 1, 2, 3, 4, 5, 6, 7 or 9"))
      .build();
    addRule(editSpec);

    Rule<AesEditSpec, ValidationResult> build =
      RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
      .when(facts -> validationService.isNotIn(facts.getValue("editSpec"), "F020001 ", new String[]{"1", "2", "3", "9"}))
      .then((facts, result) -> validationService.isIn(facts.getValue("editSpec"), "F020002 ", new String[]{"1", "2"}))
      .then((facts, result) -> result.getValue().add("F020002 must be either 1, or 2"))
      .build();
    addRule(build);

    addRule(RuleBuilder.create().withFactType(AesEditSpec.class).withResultType(ValidationResult.class)
      .when(facts -> validationService.isNotIn(facts.getValue("editSpec"), "F020001 ", new String[]{"4", "5"}))
      .then((facts, result) -> validationService.isNotFilled(facts.getValue("editSpec"), "F020002 "))
      .then((facts, result) -> result.getValue().add("F020002 must be either 1, or 2"))
      .build());

    // =============================================================================================
    // SOALAN 3
    // =============================================================================================
  }
}
