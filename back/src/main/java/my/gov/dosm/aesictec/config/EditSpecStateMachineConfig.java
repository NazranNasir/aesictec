package my.gov.dosm.aesictec.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecEvent;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;


/**
 * @author canang technologies
 */
@Configuration
@EnableStateMachine
public class EditSpecStateMachineConfig extends EnumStateMachineConfigurerAdapter<AesEditSpecState, AesEditSpecEvent> {


  @Override
  public void configure(StateMachineStateConfigurer<AesEditSpecState, AesEditSpecEvent> states) throws Exception {
    states
      .withStates()
      .initial(AesEditSpecState.JP_IMPORTED)
      .end(AesEditSpecState.SMD_FINALIZED)
      .states(EnumSet.allOf(AesEditSpecState.class));
  }

  @Override
  public void configure(StateMachineTransitionConfigurer<AesEditSpecState, AesEditSpecEvent> transitions) throws Exception {

    transitions
      .withExternal()
      .source(AesEditSpecState.JP_IMPORTED)
      .target(AesEditSpecState.JP_VALIDATED)
      .event(AesEditSpecEvent.JP_VALIDATE)
      .and()
      .withExternal()
      .source(AesEditSpecState.JP_VALIDATED)
      .target(AesEditSpecState.JP_APPROVED)
      .event(AesEditSpecEvent.JP_APPROVE)
      .and()
      .withExternal()
      .source(AesEditSpecState.JP_APPROVED)
      .target(AesEditSpecState.SMD_APPROVED)
      .event(AesEditSpecEvent.SMD_APPROVE)
      .and()
      .withExternal()
      .source(AesEditSpecState.SMD_APPROVED)
      .target(AesEditSpecState.SMD_SCHEDULED)
      .event(AesEditSpecEvent.SMD_SCHEDULE)
      .and()
      .withExternal()
      .source(AesEditSpecState.SMD_SCHEDULED)
      .target(AesEditSpecState.SMD_FINALIZED)
      .event(AesEditSpecEvent.SMD_FINALIZE);
  }

  @Override
  public void configure(StateMachineConfigurationConfigurer<AesEditSpecState, AesEditSpecEvent> config) throws Exception {
    config
      .withConfiguration()
      .autoStartup(true);
//      .listener(new StateMachineListener());
  }

//  private static final class StateMachineListener extends StateMachineListenerAdapter<AesEditSpecState, AesEditSpecEvent> {
//    @Override
//    public void stateChanged(State<AesEditSpecState, AesEditSpecEvent> from, State<AesEditSpecState, AesEditSpecEvent> to) {
//      System.out.println("state to " + to.getId());
//    }
//  }
}

