package my.gov.dosm.aesictec.validation.api.vo;

/**
 * @author canang technologies
 */
public enum EditSpecState {
  JP_IMPORTED,
  JP_VALIDATED,
  JP_APPROVED,
  SMD_APPROVED,
  SMD_SCHEDULED,
  SMD_FINALIZED;

}
