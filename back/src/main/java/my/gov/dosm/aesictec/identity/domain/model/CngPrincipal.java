package my.gov.dosm.aesictec.identity.domain.model;


import java.util.Set;

import my.gov.dosm.aesictec.core.domain.CngMetaObject;


/**
 * @author canang technologies
 */
public interface CngPrincipal extends CngMetaObject {

    String getName();

    void setName(String name);

    CngPrincipalType getPrincipalType();

    void setPrincipalType(CngPrincipalType principalType);

    boolean isLocked();

    void setLocked(boolean locked);

    boolean isEnabled();

    void setEnabled(boolean enabled);

    Set<CngPrincipalRole> getRoles();

    void setRoles(Set<CngPrincipalRole> roles);

}
