package my.gov.dosm.aesictec.system.domain.dao;


import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNo;

/**
 * @author canang technologies
 */
public interface CngReferenceNoDao extends GenericDao<Long, CngReferenceNo> {

    CngReferenceNo findByCode(String code);

    List<CngReferenceNo> find(String filter, Integer offset, Integer limit);

    Integer count(String filter);
}
