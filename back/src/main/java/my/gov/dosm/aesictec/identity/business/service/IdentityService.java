package my.gov.dosm.aesictec.identity.business.service;

import java.util.List;
import java.util.Set;

import my.gov.dosm.aesictec.identity.domain.dao.RecursiveGroupException;
import my.gov.dosm.aesictec.identity.domain.model.*;

/**
 * @author canang technologies
 */
public interface IdentityService {

    // =============================================================================================
    // PRINCIPAL
    // =============================================================================================

    CngPrincipal findPrincipalById(Long id);

    CngPrincipal findPrincipalByName(String name);

    List<CngPrincipal> findPrincipals(String filter, Integer offset, Integer limit);

    Set<String> findSids(CngPrincipal principal);

    Integer countPrincipal(String filter);

    void addPrincipalRole(CngPrincipal principal, CngPrincipalRole principalRole);

    void deletePrincipalRole(CngPrincipal principal, CngPrincipalRole principalRole);

    // =============================================================================================
    // USER
    // =============================================================================================

    CngUser findUserByEmail(String email);

    CngUser findUserByUsername(String username);

    CngUser findUserById(Long id);

    List<CngUser> findUsers(Integer offset, Integer limit);

    List<CngUser> findUsers(String filter, Integer offset, Integer limit);

    Integer countUser();

    Integer countUser(String filter);

    boolean isUserExists(String username);

    void saveUser(CngUser user);

    void updateUser(CngUser user);

    void removeUser(CngUser user);

    void changePassword(CngUser user, String newPassword);

    // =============================================================================================
    // GROUP
    // =============================================================================================

    CngGroup getRootGroup();

    CngGroup findGroupByName(String name);

    CngGroup findOrCreateGroupByName(String name);

    CngGroup findGroupById(Long id);

    List<CngGroup> findGroups(Integer offset, Integer limit);

    List<CngGroup> findImmediateGroups(CngPrincipal principal);

    List<CngGroup> findImmediateGroups(CngPrincipal principal, Integer offset, Integer limit);

    Set<CngGroup> findEffectiveGroups(CngPrincipal principal);

    Set<String> findEffectiveGroupsAsString(CngPrincipal principal);

    List<CngGroup> findAvailableUserGroups(CngUser user);

    List<CngPrincipal> findAvailableGroupMembers(CngGroup group);

    List<CngGroupMember> findGroupMembersAsGroupMember(CngGroup group);

    List<CngPrincipal> findGroupMembers(CngGroup group);

    List<CngPrincipal> findGroupMembers(CngGroup group, Integer offset, Integer limit);

    Integer countGroup();

    Integer countGroupMember(CngGroup group);

    boolean isGroupExists(String name);

    boolean hasMembership(CngGroup group, CngPrincipal principal);

    CngGroup createGroupWithRole(String groupName, CngRoleType... types);

    void saveGroup(CngGroup group);

    void updateGroup(CngGroup group);

    void removeGroup(CngGroup group);

    void addGroupMember(CngGroup group, CngPrincipal principal) throws RecursiveGroupException;

    void deleteGroupMember(CngGroup group, CngPrincipal principal);
}
