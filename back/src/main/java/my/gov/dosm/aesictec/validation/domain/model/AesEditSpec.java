package my.gov.dosm.aesictec.validation.domain.model;


/**
 * @author canang technologies
 */
public interface AesEditSpec {

  Long getId();

  void setId(Long id);

  String getCaseNo();

  void setCaseNo(String caseNo);

  AesEditSpecState getState();

  void setState(AesEditSpecState state);

  AesEditSpecSchema getEditSpecSchema();

  void setEditSpecSchema(AesEditSpecSchema editSpecSchema);

}
