package my.gov.dosm.aesictec.identity.domain.dao;


import java.util.List;
import java.util.Set;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.identity.domain.model.CngGroup;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngGroupMember;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipal;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType;

/**
 * @author canang technologies
 */
public interface CngGroupDao extends GenericDao<Long, CngGroup> {

    // ====================================================================================================
    // HELPERS
    // ====================================================================================================
    boolean hasMembership(CngGroup group, CngPrincipal principal);

    boolean isExists(String name);

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    CngGroup findByName(String name);

    List<CngGroup> findAll();

    List<CngGroup> findImmediate(CngPrincipal principal);

    List<CngGroup> findImmediate(CngPrincipal principal, Integer offset, Integer limit);

    Set<CngGroup> findEffectiveAsNative(CngPrincipal principal);

    List<CngGroup> findAvailableGroups(CngUser user);

    List<CngGroupMember> findMembersAsGroupMember(CngGroup group);

    List<CngPrincipal> findAvailableMembers(CngGroup group);

    List<CngPrincipal> findMembers(CngGroup group, CngPrincipalType principalType);

    List<CngPrincipal> findMembers(CngGroup group);

    List<CngPrincipal> findMembers(CngGroup group, Integer offset, Integer limit);

    List<CngGroup> find(String filter, Integer offset, Integer limit);

    List<CngGroup> findMemberships(CngPrincipal principal);

    Integer count(String filter);

    Integer countMember(CngGroup group);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addMember(CngGroup group, CngPrincipal member, CngUser user) throws RecursiveGroupException;

    void addMembers(CngGroup group, List<CngPrincipal> members, CngUser user) throws RecursiveGroupException;

    void deleteMember(CngGroup group, CngPrincipal member);

}
