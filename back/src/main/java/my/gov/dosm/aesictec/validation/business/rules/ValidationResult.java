package my.gov.dosm.aesictec.validation.business.rules;

import java.util.ArrayList;
import java.util.List;

/**
 * @author canang technologies
 */
public class ValidationResult {

  private List<String> messages = new ArrayList<String>();

  void add(String message) {
    messages.add(message);
  }

  @Override
  public String toString() {
    return "ValidationResult{" +
      "results=" + messages.toString() +
      '}';
  }
}
