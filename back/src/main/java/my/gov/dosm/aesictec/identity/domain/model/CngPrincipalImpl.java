package my.gov.dosm.aesictec.identity.domain.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import my.gov.dosm.aesictec.core.domain.CngMetadata;

/**
 * @author canang technologies
 */
@Entity(name = "CngPrincipal")
@Table(name = "CNG_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class CngPrincipalImpl implements CngPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SQ_CNG_PCPL")
    @SequenceGenerator(name = "SQ_CNG_PCPL", sequenceName = "SQ_CNG_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "ENABLED", nullable = false)
    private boolean enabled = true;

    @Column(name = "LOCKED", nullable = false)
    private boolean locked = false;

    @Column(name = "PRINCIPAL_TYPE")
    private CngPrincipalType principalType;

    @OneToMany(targetEntity = CngPrincipalRoleImpl.class, mappedBy = "principal", fetch = FetchType.EAGER)
    private Set<CngPrincipalRole> roles;

    @Embedded
    private CngMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public CngPrincipalType getPrincipalType() {
        return principalType;
    }

    public void setPrincipalType(CngPrincipalType principalType) {
        this.principalType = principalType;
    }

    public Set<CngPrincipalRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<CngPrincipalRole> roles) {
        this.roles = roles;
    }

    public CngMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(CngMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CngPrincipalImpl that = (CngPrincipalImpl) o;

        if (!id.equals(that.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public Class<?> getInterfaceClass() {
        return CngPrincipal.class;
    }

    @Override
    public String toString() {
        return "CngPrincipalImpl{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", principalType=" + principalType +
                '}';
    }
}
