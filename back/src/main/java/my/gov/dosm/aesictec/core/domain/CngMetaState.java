package my.gov.dosm.aesictec.core.domain;

/**
 * @author canang technologies
 * @since 1/27/14
 */
public enum CngMetaState {

    INACTIVE, // 0
    ACTIVE,   // 1

}
