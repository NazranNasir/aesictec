package my.gov.dosm.aesictec.validation.business.service;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import java.util.List;

import my.gov.dosm.aesictec.validation.business.rules.ValidationResult;
import my.gov.dosm.aesictec.validation.business.statemachine.EditSpecStateChangeListener;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;

/**
 * @author canang technologies
 */
public interface ValidationService extends EditSpecStateChangeListener {

  // ===============================================================================================
  // EDIT SPEC SCHEMA
  // ===============================================================================================

  AesEditSpecSchema findEditSpecSchemaById(Long id);

  AesEditSpecSchema findEditSpecSchemaByCaseNo(String caseNo);

  List<AesEditSpecSchema> findEditSpecSchemas();

  List<AesEditSpecSchema> findEditSpecSchemas(Integer offset, Integer limit);

  List<AesEditSpecSchemaItem> findEditSpecSchemaItem(AesEditSpecSchema e);

  Integer countEditSpecSchema();

  void saveEditSpecSchema(AesEditSpecSchema editSpec);

  void updateEditSpecSchema(AesEditSpecSchema editSpec);

  void addEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item);

  void updateEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item);

  void deleteEditSpecSchemaItem(AesEditSpecSchema spec, AesEditSpecSchemaItem item);

  // ===============================================================================================
  // EDIT SPEC
  // ===============================================================================================

  AesEditSpec findEditSpecById(Long id);

  List<AesEditSpec> findEditSpecs();

  List<AesEditSpec> findEditSpecs(Integer offset, Integer limit);

  List<AesEditSpecItem> findEditSpecItem(AesEditSpec e);

  Integer countEditSpec();

  void saveEditSpec(AesEditSpec editSpec);

  void updateEditSpec(AesEditSpec editSpec);

  void addEditSpecItem(AesEditSpec spec, AesEditSpecItem item);

  void updateEditSpecItem(AesEditSpec spec, String key, String value);

  void deleteEditSpecItem(AesEditSpec spec, AesEditSpecItem item);


  // validation
  boolean isFilled(AesEditSpec editSpec, String question);

  boolean isNotFilled(AesEditSpec editSpec, String question);

  boolean isNegative(AesEditSpec editSpec, String question);

  boolean isNotNegative(AesEditSpec editSpec, String question);

  boolean isIn(AesEditSpec editSpec, String question, String[] values);

  boolean isNotIn(AesEditSpec editSpec, String question, String[] values);

  // business
  void runRules(AesEditSpec spec);

  ValidationResult runRules(AesEditSpec spec, RuleBook<ValidationResult> ruleBook);


}
