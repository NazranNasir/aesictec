package my.gov.dosm.aesictec.identity.domain.model;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author canang technologies
 */
@Entity(name = "CngStaff")
@Table(name = "CNG_STAF")
public class CngStaffImpl extends CngActorImpl implements CngStaff {

    public CngStaffImpl() {
        super();
        setActorType(CngActorType.STAFF);
    }

    public Class<?> getInterfaceClass() {
        return CngStaff.class;
    }
}
