package my.gov.dosm.aesictec.identity.domain.dao;

import org.hibernate.type.LongType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.identity.domain.model.CngGroup;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipal;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngGroupImpl;
import my.gov.dosm.aesictec.identity.domain.model.CngGroupMember;
import my.gov.dosm.aesictec.identity.domain.model.CngGroupMemberImpl;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType;

import static my.gov.dosm.aesictec.core.domain.CngMetaState.ACTIVE;


/**
 * @author canang technologies
 */
@Repository("groupDao")
public class CngGroupDaoImpl extends GenericDaoSupport<Long, CngGroup> implements CngGroupDao {

    private static final Logger LOG = LoggerFactory.getLogger(CngGroupDaoImpl.class);

    public CngGroupDaoImpl() {
        super(CngGroupImpl.class);
    }
    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<CngGroup> findAll() {
        Query query = entityManager.createQuery("select g from CngGroup g order by g.name");
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public CngGroup findByName(String name) {
        Query query = entityManager.createQuery("select g from CngGroup g where g.name = :name");
        query.setParameter("name", name);
        List resultList = query.getResultList();
        return (resultList.size() == 0) ? null : (CngGroup) resultList.get(0);
    }


    @Override
    public List<CngGroup> findImmediate(CngPrincipal principal) {
        Query query = entityManager.createQuery("select gm.group from CngGroupMember gm inner join gm.principal where " +
                "gm.principal = :principal");
        query.setParameter("principal", principal);
        return (List<CngGroup>) query.getResultList();
    }


    @Override
    public List<CngGroup> findImmediate(CngPrincipal principal, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select gm.group from CngGroupMember gm inner join gm.principal where " +
                "gm.principal = :principal");
        query.setParameter("principal", principal);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public Set<CngGroup> findEffectiveAsNative(CngPrincipal principal) {
        Set<CngGroup> groups = new HashSet<CngGroup>();
        Query nativeQuery = entityManager.createNativeQuery("WITH RECURSIVE " +
                "Q AS " +
                "( SELECT  GROUP_ID, PRINCIPAL_ID " +
                "    FROM    CNG_GROP_MMBR " +
                "    WHERE   PRINCIPAL_ID = " + principal.getId() +
                "    UNION ALL " +
                "    SELECT  GM.GROUP_ID, GM.PRINCIPAL_ID " +
                "    FROM    CNG_GROP_MMBR GM " +
                "    JOIN    Q " +
                "    ON      GM.PRINCIPAL_ID = Q.GROUP_ID " +
                ") " +
                "SELECT  GROUP_ID FROM  Q");
        nativeQuery.setParameter("GROUP_ID", LongType.INSTANCE);
        List<Long> results = (List<Long>) nativeQuery.getResultList();
        for (Long result : results) {
            groups.add(findById(result));
        }
        return groups;
    }

    @Override
    public List<CngGroup> findAvailableGroups(CngUser user) {
        Query query = entityManager.createQuery("select p from CngGroup p where " +
                "p not in (select gm.group from CngGroupMember gm where gm.principal = :user) " +
                "and p <> :user " +
                "and p.metadata.state = :state " +
                "order by p.name asc");
        query.setParameter("state", ACTIVE);
        query.setParameter("user", user);
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public List<CngGroupMember> findMembersAsGroupMember(CngGroup group) {
        Query query = entityManager.createQuery("select gm from CngGroupMember gm where " +
                "gm.group = :group ");
        query.setParameter("group", group);
        return (List<CngGroupMember>) query.getResultList();
    }

    @Override
    public List<CngPrincipal> findAvailableMembers(CngGroup group) {
        Query query = entityManager.createQuery("select p from CngPrincipal p where " +
                "p not in (select gm.principal from CngGroupMember gm where gm.group = :group) " +
                "and p <> :group " +
                "and p.metadata.state = :state " +
                "order by p.name asc");
        query.setParameter("state", ACTIVE);
        query.setParameter("group", group);
        return (List<CngPrincipal>) query.getResultList();
    }

    @Override
    public List<CngPrincipal> findMembers(CngGroup group, CngPrincipalType principalType) {
        Query query = entityManager.createQuery("select gm.group from CngGroupMember gm inner join gm.principal where " +
                "gm.group = :group " +
                "and gm.principal.principalType= :principalType " +
                "and gm.principal.metadata.state = :state ");
        query.setParameter("group", group);
        query.setParameter("principalType", principalType);
        return (List<CngPrincipal>) query.getResultList();
    }

    @Override
    public List<CngPrincipal> findMembers(CngGroup group) {
        Query query = entityManager.createQuery("select gm.principal from CngGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal.metadata.state = :state " +
                "order by gm.principal.name");
        query.setParameter("group", group);
        query.setParameter("state", ACTIVE);
        return (List<CngPrincipal>) query.getResultList();
    }

    @Override
    public List<CngPrincipal> findMembers(CngGroup group, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select gm.principal from CngGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal.metadata.state = :state " +
                "order by gm.principal.name");
        query.setParameter("group", group);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<CngPrincipal>) query.getResultList();
    }

    @Override
    public List<CngGroup> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select distinct g from CngGroup g where " +
                "g.name like upper(:filter) ");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public List<CngGroup> findMemberships(CngPrincipal principal) {
        Query query = entityManager.createQuery("select distinct gm.group from CngGroupMember gm where " +
                "gm.principal = :principal ");
        query.setParameter("principal", principal);
        return (List<CngGroup>) query.getResultList();
    }

    @Override
    public Integer count() {
        Query query = entityManager.createQuery("select count(g) from CngGroup g");
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(g) from CngGroup g where " +
                "g.name like upper(:filter)");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public Integer countMember(CngGroup group) {
        Query query = entityManager.createQuery("select count(gm.principal) from CngGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal.metadata.state = :state");
        query.setParameter("group", group);
        query.setParameter("state", ACTIVE);
        return ((Long) query.getSingleResult()).intValue();
    }

    @Override
    public boolean isExists(String name) {
        Query query = entityManager.createQuery("select count(g) from CngGroup g where " +
                "g.name = :name");
        query.setParameter("name", name);
        return ((Long) query.getSingleResult()).intValue() >= 1;
    }

    @Override
    public boolean hasMembership(CngGroup group, CngPrincipal principal) {
        Query query = entityManager.createQuery("select count(gm) from CngGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal = :principal");
        query.setParameter("group", group);
        query.setParameter("principal", principal);
        return ((Long) query.getSingleResult()).intValue() >= 1;
    }

// =============================================================================
    // CRUD METHODS
    // =============================================================================

    @Override
    public void addMember(CngGroup group, CngPrincipal member, CngUser user) throws RecursiveGroupException {
        if (member instanceof CngGroup) {
            if (checkRecursive(group, (CngGroup) member))
                throw new RecursiveGroupException("Recursive user group " + group.getName() + " > " + member.getName());
        }

        CngGroupMember groupMember = new CngGroupMemberImpl();
        groupMember.setGroup(group);
        groupMember.setPrincipal(member);
        entityManager.persist(groupMember);
    }

    @Override
    public void addMembers(CngGroup group, List<CngPrincipal> members, CngUser user) throws RecursiveGroupException {
        List<CngPrincipal> currentMembers = findMembers(group);
        List<CngPrincipal> newMembers = new ArrayList<CngPrincipal>();

        for (CngPrincipal currentMember : currentMembers) {
            if (!newMembers.contains(currentMember)) {
                deleteMember(group, currentMember);
            }
        }
        for (CngPrincipal newMember : newMembers) {
            if (!currentMembers.contains(newMember)) {
                addMember(group, newMember, user);
            }
        }
    }

    @Override
    public void deleteMember(CngGroup group, CngPrincipal member) {
        Query query = entityManager.createQuery("select g from CngGroupMember g where " +
                "g.group = :group " +
                "and g.principal = :principal");
        query.setParameter("group", group);
        query.setParameter("principal", member);
        CngGroupMember groupMember = (CngGroupMember) query.getSingleResult();
        entityManager.remove(groupMember);
    }

    private boolean checkRecursive(CngGroup parent, CngGroup child) {
        Set<CngGroup> hierarchicalGroup = findEffectiveAsNative(parent);
        return !hierarchicalGroup.add(child);
    }
}
