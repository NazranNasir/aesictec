package my.gov.dosm.aesictec.system.business.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ParserContext;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import my.gov.dosm.aesictec.identity.business.service.IdentityService;
import my.gov.dosm.aesictec.security.business.service.SecurityService;
import my.gov.dosm.aesictec.system.domain.dao.CngConfigurationDao;
import my.gov.dosm.aesictec.system.domain.dao.CngModuleDao;
import my.gov.dosm.aesictec.system.domain.dao.CngReferenceNoDao;
import my.gov.dosm.aesictec.system.domain.dao.CngSubModuleDao;
import my.gov.dosm.aesictec.system.domain.model.CngConfiguration;
import my.gov.dosm.aesictec.system.domain.model.CngModule;
import my.gov.dosm.aesictec.system.domain.model.CngReferenceNo;
import my.gov.dosm.aesictec.system.domain.model.CngSubModule;

/**
 */
@Transactional
@Service("systemService")
public class SystemServiceImpl implements SystemService {

    public static final DateFormat LONG_YEAR_FORMAT = new SimpleDateFormat("yyyy");
    public static final DateFormat SHORT_YEAR_FORMAT = new SimpleDateFormat("yy");
    public static final DateFormat LONG_MONTH_FORMAT = new SimpleDateFormat("MMM");
    public static final DateFormat SHORT_MONTH_FORMAT = new SimpleDateFormat("MM");
    public static final DateFormat LONG_DAY_FORMAT = new SimpleDateFormat("dd");
    public static final DateFormat SHORT_DAY_FORMAT = new SimpleDateFormat("dd");
    public static final DateFormat LONG_HOUR_FORMAT = new SimpleDateFormat("hh");
    public static final DateFormat SHORT_HOUR_FORMAT = new SimpleDateFormat("hh");

    private static final Logger LOG = LoggerFactory.getLogger(SystemServiceImpl.class);

    private EntityManager entityManager;
    private ApplicationContext applicationContext;
    private IdentityService identityService;
    private SecurityService securityService;
    private CngModuleDao moduleDao;
    private CngSubModuleDao subModuleDao;
    private CngConfigurationDao configurationDao;
    private CngReferenceNoDao referenceNoDao;

    @Autowired
    public SystemServiceImpl(EntityManager entityManager, ApplicationContext applicationContext,
                             IdentityService identityService, SecurityService securityService,
                             CngModuleDao moduleDao, CngSubModuleDao subModuleDao,
                             CngConfigurationDao configurationDao, CngReferenceNoDao referenceNoDao) {
        this.entityManager = entityManager;
        this.applicationContext = applicationContext;
        this.identityService = identityService;
        this.securityService = securityService;
        this.moduleDao = moduleDao;
        this.subModuleDao = subModuleDao;
        this.configurationDao = configurationDao;
        this.referenceNoDao = referenceNoDao;
    }

    //==============================================================================================
    // MODULE SUBMODULE
    //==============================================================================================

    @Override
    public CngModule findModuleById(Long id) {
        return moduleDao.findById(id);
    }

    @Override
    public CngSubModule findSubModuleById(Long id) {
        return subModuleDao.findById(id);
    }

    @Override
    public CngModule findModuleByCode(String code) {
        return moduleDao.findByCode(code);
    }

    @Override
    public CngSubModule findSubModuleByCode(String code) {
        return subModuleDao.findByCode(code);
    }

    @Override
    public List<CngModule> findModules(boolean authorized) {
//        return authorized ?
//                moduleDao.findAuthorized(identityService.findSids(securityService.getCurrentUser())) :
//                moduleDao.findEditSpecs();
        return null;
    }

    @Override
    public List<CngModule> findModules() {
        return moduleDao.find();
    }

    @Override
    public List<CngModule> findModules(Integer offset, Integer limit) {
        return moduleDao.find(offset, limit);
    }

    @Override
    public List<CngModule> findModules(boolean authorized, Integer offset, Integer limit) {
//        return authorized ?
//                moduleDao.findAuthorized(identityService.findSids(securityService.getCurrentUser()), offset, limit) :
//                moduleDao.findEditSpecs(offset, limit);
        return null;
    }

    @Override
    public List<CngSubModule> findSubModules() {
        return subModuleDao.find();
    }

    @Override
    public List<CngSubModule> findSubModules(boolean authorized) {
//        return authorized ?
//                subModuleDao.findAuthorized(identityService.findSids(securityService.getCurrentUser())) :
//                subModuleDao.findEditSpecs();
        return null;
    }

    @Override
    public List<CngSubModule> findSubModules(Integer offset, Integer limit) {
        return subModuleDao.find(offset, limit);
    }

    @Override
    public List<CngSubModule> findSubModules(CngModule module, Integer offset, Integer limit) {
        return subModuleDao.find(module, offset, limit);
    }

    @Override
    public Integer countModule() {
        return moduleDao.count();
    }

    @Override
    public Integer countSubModule() {
        return subModuleDao.count();
    }

    @Override
    public Integer countSubModule(CngModule module) {
        return subModuleDao.count(module);
    }

    @Override
    public void saveModule(CngModule module) {
        moduleDao.save(module, securityService.getCurrentUser());
    }

    @Override
    public void updateModule(CngModule module) {
        moduleDao.update(module, securityService.getCurrentUser());
    }

    @Override
    public void addSubModule(CngModule module, CngSubModule subModule) {
        moduleDao.addSubModule(module, subModule, securityService.getCurrentUser());
    }

    @Override
    public void updateSubModule(CngModule module, CngSubModule subModule) {
        moduleDao.updateSubModule(module, subModule, securityService.getCurrentUser());
    }

    //==============================================================================================
    // REFERENCE NO
    //==============================================================================================

    @Override
    public CngReferenceNo findReferenceNoById(Long id) {
        return referenceNoDao.findById(id);
    }

    @Override
    public CngReferenceNo findReferenceNoByCode(String code) {
        return referenceNoDao.findByCode(code);
    }

    @Override
    public List<CngReferenceNo> findReferenceNos(Integer offset, Integer limit) {
        return referenceNoDao.find(offset, limit);
    }

    @Override
    public List<CngReferenceNo> findReferenceNos(String filter, Integer offset, Integer limit) {
        return referenceNoDao.find(filter, offset, limit);
    }

    @Override
    public Integer countReferenceNo() {
        return referenceNoDao.count();
    }

    @Override
    public Integer countReferenceNo(String filter) {
        return referenceNoDao.count(filter);
    }

    @Override
    public void saveReferenceNo(CngReferenceNo referenceNo) {
        referenceNoDao.save(referenceNo, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void updateReferenceNo(CngReferenceNo referenceNo) {
        referenceNoDao.update(referenceNo, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void removeReferenceNo(CngReferenceNo referenceNo) {
        referenceNoDao.remove(referenceNo, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public String generateReferenceNo(String code) {
        String generatedRefNo = null;
        synchronized (this) {
            CngReferenceNo referenceNo = referenceNoDao.findByCode(code);
            Integer oldValue = referenceNo.getCurrentValue();
            Integer newValue = referenceNo.getCurrentValue() + referenceNo.getIncrementValue();

            // update
            referenceNo.setCurrentValue(newValue);
            referenceNoDao.save(referenceNo, securityService.getCurrentUser());
            entityManager.flush();
            NumberFormat numberFormat = new DecimalFormat(referenceNo.getSequenceFormat());

            // format
            generatedRefNo = referenceNo.getPrefix() + numberFormat.format(oldValue);
        }
        return generatedRefNo;
    }

    public String generateFormattedReferenceNo(String code, Map<String, Object> map) {
        synchronized (this) {
            CngReferenceNo referenceNo = referenceNoDao.findByCode(code);

            // get old and new value
            Integer oldValue = referenceNo.getCurrentValue();
            Integer newValue = referenceNo.getCurrentValue() + referenceNo.getIncrementValue();

            // update
            referenceNo.setCurrentValue(newValue);
            referenceNoDao.save(referenceNo, securityService.getCurrentUser());
            entityManager.flush();

            Date now = new Date();
            NumberFormat numberFormat = new DecimalFormat(referenceNo.getSequenceFormat());
            SpelParserConfiguration configuration = new SpelParserConfiguration(true, true);
            StandardEvaluationContext context = new StandardEvaluationContext(configuration);
            ParserContext templateContext = new TemplateParserContext("{", "}");
            context.setVariable("a", referenceNo.getPrefix());
            context.setVariable("b", LONG_YEAR_FORMAT.format(now));
            context.setVariable("c", SHORT_YEAR_FORMAT.format(now));
            context.setVariable("d", LONG_MONTH_FORMAT.format(now));
            context.setVariable("e", SHORT_MONTH_FORMAT.format(now));
            context.setVariable("f", LONG_DAY_FORMAT.format(now));
            context.setVariable("g", SHORT_DAY_FORMAT.format(now));
            context.setVariable("h", LONG_HOUR_FORMAT.format(now));
            context.setVariable("i", SHORT_HOUR_FORMAT.format(now));
            context.setVariable("j", numberFormat.format(oldValue));

            for (String key : map.keySet()) {
                context.setVariable(key, map.get(key));
            }

            SpelExpressionParser parser = new SpelExpressionParser();
            Expression expression = parser.parseExpression(referenceNo.getReferenceFormat(), templateContext);
            return expression.getValue(context, String.class);
        }
    }

    //==============================================================================================
    // CONFIGURATION
    //==============================================================================================

    @Override
    public CngConfiguration findConfigurationById(Long id) {
        return configurationDao.findById(id);
    }

    @Override
    public CngConfiguration findConfigurationByKey(String key) {
        return configurationDao.findByKey(key);
    }

    @Override
    public List<CngConfiguration> findConfigurationsByPrefix(String prefix) {
        return configurationDao.findByPrefix(prefix);
    }

    @Override
    public List<CngConfiguration> findConfigurations() {
        return configurationDao.find();
    }

    @Override
    public List<CngConfiguration> findConfigurations(Integer offset, Integer limit) {
        return configurationDao.find(offset, limit);
    }

    @Override
    public List<CngConfiguration> findConfigurations(String filter) {
        return configurationDao.find(filter);
    }

    @Override
    public List<CngConfiguration> findConfigurations(String filter, Integer offset, Integer limit) {
        return configurationDao.find(filter, offset, limit);
    }


    @Override
    public Integer countConfiguration() {
        return configurationDao.count();
    }

    @Override
    public Integer countConfiguration(String filter) {
        return configurationDao.count(filter);
    }

    @Override
    public void saveConfiguration(CngConfiguration config) {
        configurationDao.save(config, securityService.getCurrentUser());
    }

    @Override
    public void updateConfiguration(CngConfiguration config) {
        configurationDao.update(config, securityService.getCurrentUser());
    }

    @Override
    public void removeConfiguration(CngConfiguration config) {
        configurationDao.remove(config, securityService.getCurrentUser());
    }
}
