package my.gov.dosm.aesictec.identity.domain.model;

import java.util.Set;

/**
 * @author canang technologies
 */
public interface CngGroup extends CngPrincipal {

    Set<CngPrincipal> getMembers();

    void setMembers(Set<CngPrincipal> members);
}
