package my.gov.dosm.aesictec.validation.business.statemachine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.access.StateMachineFunction;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.statemachine.support.LifecycleObjectSupport;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecEvent;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;

/**
 * @author canang technologies
 */
@Component
public class EditSpecStateHandler extends LifecycleObjectSupport {

  @Autowired
  private StateMachine<AesEditSpecState, AesEditSpecEvent> stateMachine;

  private Set<EditSpecStateChangeListener> listeners = new HashSet<>();

  @Override
  protected void onInit() throws Exception {
    stateMachine
      .getStateMachineAccessor()
      .doWithAllRegions(new StateMachineFunction<StateMachineAccess<AesEditSpecState, AesEditSpecEvent>>() {
        @Override
        public void apply(StateMachineAccess<AesEditSpecState, AesEditSpecEvent> function) {
          function.addStateMachineInterceptor(new StateMachineInterceptorAdapter<AesEditSpecState, AesEditSpecEvent>() {
            @Override
            public void postStateChange(State<AesEditSpecState, AesEditSpecEvent> state, Message<AesEditSpecEvent> message, Transition<AesEditSpecState, AesEditSpecEvent> transition, StateMachine<AesEditSpecState, AesEditSpecEvent> stateMachine) {
              listeners.forEach(listener -> listener.onStateChange(state, message));
            }

            @Override
            public void preStateChange(State<AesEditSpecState, AesEditSpecEvent> state, Message<AesEditSpecEvent> message, Transition<AesEditSpecState, AesEditSpecEvent> transition, StateMachine<AesEditSpecState, AesEditSpecEvent> stateMachine) {
//              listeners.forEach(listener -> listener.onStateChange(state, message));
            }
          });
        }
      });
  }

  public void registerListener(EditSpecStateChangeListener listener) {
    listeners.add(listener);
  }

  public void handleEvent(Message event, AesEditSpecState sourceState) {
    stateMachine.stop();
    stateMachine
      .getStateMachineAccessor()
      .doWithAllRegions(access -> access.resetStateMachine(
        new DefaultStateMachineContext<AesEditSpecState, AesEditSpecEvent>(sourceState, null, null, null)));
    stateMachine.start();
    stateMachine.sendEvent(event);
  }
}
