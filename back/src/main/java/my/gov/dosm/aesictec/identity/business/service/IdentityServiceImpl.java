package my.gov.dosm.aesictec.identity.business.service;

import my.gov.dosm.aesictec.identity.domain.dao.*;
import my.gov.dosm.aesictec.identity.domain.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import my.gov.dosm.aesictec.security.business.service.SecurityService;

/**
 * @author canang technologies
 */
@Transactional
@Service("CngIdentityService")
public class IdentityServiceImpl implements IdentityService {

    private static final String GROUP_ROOT = "GRP_ADMN";
    private static final Logger LOG = LoggerFactory.getLogger(IdentityServiceImpl.class);

    private EntityManager entityManager;
    private SecurityService securityService;
    private CngPrincipalDao principalDao;
    private CngUserDao userDao;
    private CngGroupDao groupDao;

    @Autowired
    public IdentityServiceImpl(EntityManager entityManager, SecurityService securityService,
                               CngPrincipalDao principalDao, CngUserDao userDao,
                               CngGroupDao groupDao) {
        this.entityManager = entityManager;
        this.securityService = securityService;
        this.principalDao = principalDao;
        this.userDao = userDao;
        this.groupDao = groupDao;
    }

    //==============================================================================================
    // PRINCIPAL
    //==============================================================================================

    @Override
    public CngPrincipal findPrincipalById(Long id) {
        return principalDao.findById(id);
    }

    @Override
    public CngPrincipal findPrincipalByName(String name) {
        return principalDao.findByName(name);
    }

    @Override
    public List<CngPrincipal> findPrincipals(String filter, Integer offset, Integer limit) {
        return principalDao.find(filter, offset, limit);
    }

    @Override
    public Set<String> findSids(CngPrincipal principal) {
        Set<CngGroup> groups = null;
        Set<String> principals = new HashSet<String>();
        try {
            groups = groupDao.findEffectiveAsNative(principal);
        } catch (Exception e) {
            LOG.error("Error occured loading principals", e);
        } finally {
            if (null != groups) {
                for (CngGroup group : groups) {
                    principals.add(group.getName());
                }
            }
            principals.add(principal.getName());
        }
        return principals;
    }

    @Override
    public Integer countPrincipal(String filter) {
        return principalDao.count(filter);
    }

    @Override
    public void addPrincipalRole(CngPrincipal principal, CngPrincipalRole principalRole) {
        principalDao.addRole(principal, principalRole, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void deletePrincipalRole(CngPrincipal principal, CngPrincipalRole principalRole) {
        principalDao.deleteRole(principal, principalRole, securityService.getCurrentUser());
        entityManager.flush();
    }

    //==============================================================================================
    // USER
    //==============================================================================================

    @Override
    public CngUser findUserByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public CngUser findUserByUsername(String username) {

        return userDao.findByUsername(username);
    }

    @Override
    public CngUser findUserById(Long id) {
        return userDao.findById(id);
    }

    @Override
    public List<CngUser> findUsers(Integer offset, Integer limit) {
        return userDao.find(offset, limit);
    }

    @Override
    public List<CngUser> findUsers(String filter, Integer offset, Integer limit) {
        return userDao.find(filter, offset, limit);
    }

    @Override
    public Integer countUser() {
        return userDao.count();
    }

    @Override
    public Integer countUser(String filter) {
        return userDao.count(filter);
    }

    @Override
    public boolean isUserExists(String username) {
        return userDao.isExists(username);
    }

    @Override
    public void saveUser(CngUser user) {
        userDao.save(user, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void updateUser(CngUser user) {
        userDao.update(user, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void removeUser(CngUser user) {
        userDao.remove(user, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void changePassword(CngUser user, String newPassword) {
        user.setPassword(newPassword);
        userDao.update(user, securityService.getCurrentUser());
        entityManager.flush();
    }

    //==============================================================================================
    // GROUP
    //==============================================================================================

    @Override
    public CngGroup getRootGroup() {
        return groupDao.findByName(GROUP_ROOT);
    }

    @Override
    public CngGroup findGroupByName(String name) {
        return groupDao.findByName(name);
    }

    @Override
    public CngGroup findOrCreateGroupByName(String name) {
        CngGroup group = null;
        if (groupDao.isExists(name))
            group = groupDao.findByName(name);
        else {
            group = new CngGroupImpl();
            group.setName(name);
            group.setEnabled(true);
            group.setLocked(false);
            groupDao.save(group, securityService.getCurrentUser());
        }
        entityManager.flush();
        entityManager.refresh(group);
        return group;
    }

    @Override
    public CngGroup findGroupById(Long id) {
        return groupDao.findById(id);
    }

    @Override
    public List<CngGroup> findGroups(Integer offset, Integer limit) {
        return groupDao.find(offset, limit);
    }

    @Override
    public List<CngGroup> findImmediateGroups(CngPrincipal principal) {
        return groupDao.findImmediate(principal);
    }

    @Override
    public List<CngGroup> findImmediateGroups(CngPrincipal principal, Integer offset, Integer limit) {
        return groupDao.findImmediate(principal, offset, limit);
    }

    @Override
    public Set<CngGroup> findEffectiveGroups(CngPrincipal principal) {
        return groupDao.findEffectiveAsNative(principal);
    }

    @Override
    public Set<String> findEffectiveGroupsAsString(CngPrincipal principal) {
        Set<String> groups = new HashSet<>();
        Set<CngGroup> groupSet = groupDao.findEffectiveAsNative(principal);
        for (CngGroup adGroup : groupSet) {
            groups.add(adGroup.getName());
        }
        return groups;
    }

    @Override
    public List<CngGroup> findAvailableUserGroups(CngUser user) {
        return groupDao.findAvailableGroups(user);
    }

    @Override
    public List<CngPrincipal> findAvailableGroupMembers(CngGroup group) {
        return groupDao.findAvailableMembers(group);
    }

    @Override
    public List<CngGroupMember> findGroupMembersAsGroupMember(CngGroup group) {
        List<CngGroupMember> members = groupDao.findMembersAsGroupMember(group);
        return members;
    }

    @Override
    public List<CngPrincipal> findGroupMembers(CngGroup group) {
        return groupDao.findMembers(group);
    }

    @Override
    public List<CngPrincipal> findGroupMembers(CngGroup group, Integer offset, Integer limit) {
        return groupDao.findMembers(group, offset, limit);
    }

    @Override
    public Integer countGroup() {
        return groupDao.count();
    }

    @Override
    public Integer countGroupMember(CngGroup group) {
        return groupDao.countMember(group);
    }

    @Override
    public boolean isGroupExists(String name) {
        return groupDao.isExists(name);
    }

    @Override
    public boolean hasMembership(CngGroup group, CngPrincipal principal) {
        return groupDao.hasMembership(group, principal);
    }

    @Override
    public CngGroup createGroupWithRole(String name, CngRoleType... types) {
        CngGroup group = new CngGroupImpl();
        group.setName(name);
        group.setEnabled(true);
        group.setLocked(false);
        groupDao.save(group, securityService.getCurrentUser());
        entityManager.flush();
        entityManager.refresh(group);

        for (CngRoleType type : types) {
            CngPrincipalRole role = new CngPrincipalRoleImpl();
            role.setRole(type);
            principalDao.addRole(group, role, securityService.getCurrentUser());
            entityManager.flush();
        }
        entityManager.refresh(group);
        return group;
    }

    @Override
    public void saveGroup(CngGroup group) {
        groupDao.save(group, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void updateGroup(CngGroup group) {
        groupDao.update(group, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void removeGroup(CngGroup group) {
        groupDao.remove(group, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void addGroupMember(CngGroup group, CngPrincipal principal) throws RecursiveGroupException {
        groupDao.addMember(group, principal, securityService.getCurrentUser());
        entityManager.flush();
    }

    @Override
    public void deleteGroupMember(CngGroup group, CngPrincipal principal) {
        groupDao.deleteMember(group, principal);
        entityManager.flush();
    }

}
