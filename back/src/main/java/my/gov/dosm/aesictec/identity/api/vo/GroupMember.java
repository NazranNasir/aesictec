package my.gov.dosm.aesictec.identity.api.vo;


import my.gov.dosm.aesictec.core.api.MetaObject;

/**
 */
public class GroupMember extends MetaObject {
    private Principal principal;

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }
}
