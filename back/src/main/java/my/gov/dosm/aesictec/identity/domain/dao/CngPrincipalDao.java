package my.gov.dosm.aesictec.identity.domain.dao;


import java.util.List;

import my.gov.dosm.aesictec.core.domain.GenericDao;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipal;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalRole;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType;

/**
 * @author canang technologies
 * @since 1/30/14
 */
public interface CngPrincipalDao extends GenericDao<Long, CngPrincipal> {

    // ====================================================================================================
    // HELPERS
    // ====================================================================================================

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    CngPrincipal findByName(String name);

    List<CngPrincipal> findAllPrincipals();

    List<CngPrincipal> find(String filter);

    List<CngPrincipal> find(String filter, CngPrincipalType type);

    List<CngPrincipal> find(String filter, Integer offset, Integer limit);

    void addRole(CngPrincipal principal, CngPrincipalRole principalRole, CngUser user);

    void deleteRole(CngPrincipal principal, CngPrincipalRole principalRole, CngUser user);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

}
