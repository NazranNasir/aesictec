package my.gov.dosm.aesictec.system.domain.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.Query;

import my.gov.dosm.aesictec.core.domain.CngMetaState;
import my.gov.dosm.aesictec.core.domain.GenericDaoSupport;
import my.gov.dosm.aesictec.system.domain.model.CngConfiguration;
import my.gov.dosm.aesictec.system.domain.model.CngConfigurationImpl;

/**
 */
@Repository("configurationDao")
public class CngConfigurationDaoImpl extends GenericDaoSupport<Long, CngConfiguration> implements CngConfigurationDao {


    public CngConfigurationDaoImpl() {
        super(CngConfigurationImpl.class);
    }

    public CngConfigurationDaoImpl(Class clazz) {
        super(clazz);
    }

    @Override
    public CngConfiguration findByKey(String key) {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "s.key = :key and  " +
                " s.metadata.state = :state");
        query.setParameter("key", key);
        query.setParameter("state", CngMetaState.ACTIVE);
        return (CngConfiguration) query.getSingleResult();
    }

    @Override
    public List<CngConfiguration> findByPrefix(String prefix) {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "upper(s.key) like upper(:prefix)  " +
                "and s.metadata.state = :state");
        query.setParameter("prefix", prefix + WILDCARD);
        query.setParameter("state", CngMetaState.ACTIVE);
        return query.getResultList();
    }

    @Override
    public List<CngConfiguration> find() {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "s.metadata.state = :state");
        query.setParameter("state", CngMetaState.ACTIVE);
        return query.getResultList();
    }

    @Override
    public List<CngConfiguration> find(String filter, Integer offset, Integer limit) {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "(upper(s.key) like upper(:filter)  " +
                "or upper(s.description) like upper(:filter))  " +
                "and s.metadata.state = :state");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("state", CngMetaState.ACTIVE);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    @Override
    public List<CngConfiguration> find(String filter) {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "upper(s.key) like upper(:filter)  " +
                "and s.metadata.state = :state");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("state", CngMetaState.ACTIVE);
        return query.getResultList();
    }

    @Override
    public List<CngConfiguration> findInverse(String filter) {
        Query query = entityManager.createQuery("select s from CngConfiguration s where " +
                "upper(s.key) not like upper(:filter)  " +
                "and s.metadata.state = :state");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("state", CngMetaState.ACTIVE);
        return query.getResultList();
    }

    @Override
    public Integer count(String filter) {
        Query query = entityManager.createQuery("select count(s) from CngConfiguration s where " +
                "(upper(s.key) like upper(:filter)  " +
                "or upper(s.description) like upper(:filter))  " +
                "and s.metadata.state = :state");
        query.setParameter("filter", WILDCARD + filter + WILDCARD);
        query.setParameter("state", CngMetaState.ACTIVE);
        return ((Long) query.getSingleResult()).intValue();
    }

}
