package my.gov.dosm.aesictec.validation.domain.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author canang technologies
 */
@Entity(name = "AesEditSpecItem")
@Table(name = "AES_EDIT_SPEC_ITEM")
public class AesEditSpecItemImpl implements AesEditSpecItem {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(generator = "SQ_AES_EDIT_SPEC_ITEM")
  @SequenceGenerator(name = "SQ_AES_EDIT_SPEC_ITEM", sequenceName = "SQ_AES_EDIT_SPEC_ITEM", allocationSize = 1)
  private Long id;

  @Column(name = "QUESTION", nullable = false)
  private String question;

  @Column(name = "RESPONSE", nullable = false)
  private String response;

  @Column(name = "ERROR", nullable = true)
  private String error;

  @OneToOne(targetEntity = AesEditSpecImpl.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "EDIT_SPEC_ID", nullable = false)
  private AesEditSpec editSpec;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getQuestion() {
    return question;
  }

  @Override
  public void setQuestion(String question) {
    this.question = question;
  }

  @Override
  public String getResponse() {
    return response;
  }

  @Override
  public void setResponse(String response) {
    this.response = response;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  @Override
  public Integer getResponseAsInteger() {
    return Integer.parseInt(getResponse());
  }

  @Override
  public Date getResponseAsDate() {
    return null;
  }

  public AesEditSpec getEditSpec() {
    return editSpec;
  }

  public void setEditSpec(AesEditSpec editSpec) {
    this.editSpec = editSpec;
  }
}
