package my.gov.dosm.aesictec.core.api.controller;


import org.springframework.stereotype.Component;

import my.gov.dosm.aesictec.core.api.FlowObject;
import my.gov.dosm.aesictec.core.api.FlowState;
import my.gov.dosm.aesictec.core.api.MetaObject;
import my.gov.dosm.aesictec.core.api.MetaState;
import my.gov.dosm.aesictec.core.domain.CngFlowObject;
import my.gov.dosm.aesictec.core.domain.CngMetaObject;

/**
 */
@Component("coreTransformer")
public class CoreTransformer {

    public void toMetadata(CngMetaObject m, MetaObject vo){
        vo.setCreatedDate(m.getMetadata().getCreatedDate());
        vo.setDeletedDate(m.getMetadata().getDeletedDate());
        vo.setModifiedDate(m.getMetadata().getModifiedDate());
        vo.setMetaState(MetaState.get(m.getMetadata().getState().ordinal()));
    }

    public void toFlowdata(CngFlowObject m, FlowObject vo){
        toMetadata(m,vo);
        vo.setFlowState(FlowState.get(m.getFlowdata().getState().ordinal()));
    }
}
