package my.gov.dosm.aesictec.security.business.integration.model;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity(name = "NewssUser")
@Table(name = "NEWSS_USR")
@Immutable
public class NewsUser {

  @Id
  @Column(name = "USER_ID", updatable = false, insertable = false)
  private String userId;

  @Column(name = "USER_NAME")
  private String userName;

  @Column(name = "PASSWORD")
  private String password;

  @Transient
  private Boolean locked = false;

  @Transient
  private Boolean enabled = true;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password.toLowerCase();
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Boolean getLocked() {
    return locked;
  }

  public void setLocked(Boolean locked) {
    this.locked = locked;
  }

  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }
}
