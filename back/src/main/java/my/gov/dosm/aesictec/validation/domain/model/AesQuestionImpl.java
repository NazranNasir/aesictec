package my.gov.dosm.aesictec.validation.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author canang technologies
 */
@Entity(name = "AesQuestion")
@Table(name = "AES_QSTN")
public class AesQuestionImpl implements AesQuestion {

  @Id
  @Column(name = "ID", nullable = false)
  @GeneratedValue(generator = "SQ_AES_QSTN")
  @SequenceGenerator(name = "SQ_AES_QSTN", sequenceName = "SQ_AES_QSTN", allocationSize = 1)
  private Long id;

  @Column(name = "FIELD_NO", nullable = false, unique = true)
  private String fieldNo;

  @Column(name = "STATEMENT", nullable = false, unique = true)
  private String statement;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Override
  public String getFieldNo() {
    return fieldNo;
  }

  @Override
  public void setFieldNo(String fieldNo) {
    this.fieldNo = fieldNo;
  }

  @Override
  public String getStatement() {
    return statement;
  }

  @Override
  public void setStatement(String statement) {
    this.statement = statement;
  }
}
