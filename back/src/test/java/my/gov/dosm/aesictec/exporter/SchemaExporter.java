package my.gov.dosm.aesictec.exporter;


import java.io.File;
import java.io.FileNotFoundException;

/**
 * @author canang technologies
 */
public class SchemaExporter {
    public static void main(String[] args) throws FileNotFoundException {
       HibernateExporterUtil exporter = new HibernateExporterUtil(
                "org.hibernate.dialect.PostgreSQL82Dialect",
                "my.gov.dosm.aesictec"
                );
        exporter.setGenerateDropQueries(false);
        exporter.export(new File("create.sql"));
    }
}
