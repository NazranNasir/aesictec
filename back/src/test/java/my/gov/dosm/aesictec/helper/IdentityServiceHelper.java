package my.gov.dosm.aesictec.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

import my.gov.dosm.aesictec.identity.business.service.IdentityService;
import my.gov.dosm.aesictec.identity.domain.model.CngGroup;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipal;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalRole;
import my.gov.dosm.aesictec.identity.domain.model.CngPrincipalType;
import my.gov.dosm.aesictec.identity.domain.model.CngRoleType;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.security.business.service.SecurityService;


@Service
public class IdentityServiceHelper {
    private static final Logger log = LoggerFactory.getLogger(IdentityServiceHelper.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private SecurityService securityService;

    public void changeUser(String username) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, "abc123");
        Authentication authed = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authed);
    }

    public CngUser getCurrentUser() {
        return securityService.getCurrentUser();
    }

    public CngUser changeUserByGroup(String groupName) {
        log.debug("Loading user for {}", groupName);
        CngGroup group = identityService.findGroupByName(groupName);
        List<CngPrincipal> members = identityService.findGroupMembers(group);
        for (CngPrincipal member : members) {
            if (member.getPrincipalType().equals(CngPrincipalType.USER)) {
                String username = ((CngUser) member).getUsername();
                log.debug("Changing user to {}", username);
                changeUser(username);
                return (CngUser) member;
            }
        }
        return null;
    }

    public boolean findUserRole(CngUser user, CngRoleType roleType) {
        Set<CngGroup> groups = identityService.findEffectiveGroups(user);
        for (CngGroup group : groups) {
            for (CngPrincipalRole role : group.getRoles()) {
                if (role.getRole().equals(roleType)) return true;
            }
        }
        return false;

    }

}

