package my.gov.dosm.aesictec;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class TestJdbcTemplate {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Test
  public void test1() {

    Integer count = jdbcTemplate.queryForObject("select count(1) from cng_pcpl", Integer.class);
    System.out.println("Total record found:" + count);
  }
  
  @Test
  public void test2() {
    jdbcTemplate.update("insert into aes_edit_spec(id, case_no, state) values (1, 'xxx', 01)");
    jdbcTemplate.update("insert into aes_edit_spec(id, case_no, state) values (2, 'yyy', 02)");
    Integer count = jdbcTemplate.queryForObject("select count(*) from aes_edit_spec", Integer.class);
    System.out.println("Total record found:" + count);
    
  }
}
