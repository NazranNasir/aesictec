package my.gov.dosm.aesictec;

import bsh.EvalError;
import bsh.Interpreter;

public class SimpleBeanShellTest {

  public static void main(String args[]) throws EvalError {
    Interpreter interpreter = new Interpreter();
    System.out.println("If F020001 is 1, 2, 3 or 9 then F020002 must be 1 or 2");
    interpreter.set("F020001", 10);
    interpreter.set("F020002", 1);
    interpreter.eval("Boolean result = (F020001 == 1 || F020001 == 2 || F020001 == 3 || F020001 == 9 ) " +
      "&& (F020002 == 1 || F020002 == 2)");
    Boolean result = (Boolean) interpreter.get("result");
    System.out.println(result ? "SAH" : "TIDAK SAH");
//    System.out.println("2 + 2");
//
//    interpreter.set("one", 1);
//    interpreter.set("two", 2);
//    interpreter.eval("int three = one + two");
//    System.out.println(interpreter.eval("three"));
//
//    String script = //"import java.util.List; import java.util.ArrayList;\n" +
//      "List l = new ArrayList();" +
//        "l.add(\"Ali\");" +
//        "l.add(\"Abu\");" +
//        "l.add(\"Bakar\");";
//    interpreter.eval(script);
//    System.out.println(interpreter.get("l"));
//
//    interpreter.eval("print(5)");
//
//    String F010030;
//    interpreter.set("F010030",null);
//    System.out.println(interpreter.eval("F010030 != null"));
//

  }






}
