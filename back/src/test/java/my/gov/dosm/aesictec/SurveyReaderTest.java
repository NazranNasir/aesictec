package my.gov.dosm.aesictec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.persistence.EntityManager;


/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class SurveyReaderTest {

  private static final Logger LOG = LoggerFactory.getLogger(SurveyReaderTest.class);

  @Autowired
  private EntityManager entityManager;

  @Before
  public void before() {
  }

  @After
  public void after() {
    // cleanup
  }

  @Test
  public void surveyReader310() {
    try {
      String caseNo = "310";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      for (int i = 0; i < schemas.length; i++) {
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void surveyReader319() {
    try {
      String caseNo = "319";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      for (int i = 0; i < schemas.length; i++) {
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void surveyReader320() {
    try {
      String caseNo = "320";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      for (int i = 0; i < schemas.length; i++) {
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void surveyReader330() {
    try {
      String caseNo = "330";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      for (int i = 0; i < schemas.length; i++) {
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String trimLine(String file) throws IOException {
    String line;
    StringBuffer append = new StringBuffer();

    BufferedReader bufferreader = new BufferedReader(new FileReader(file));
    while ((line = bufferreader.readLine()) != null) {
      append.append(line.trim());
    }
    return append.toString();
  }

  private String[] split(String caseNo, String appended) {
    String[] split = appended.split("SURVEY" + caseNo);
    String schema = null;
    String raw = split[0];
    String response = split[1];
    schema = raw.substring("SURVEY|#BatchNo|BATCHCODE|FID|DATETIME".length(), raw.length());
    return new String[]{schema, response};
  }
}
