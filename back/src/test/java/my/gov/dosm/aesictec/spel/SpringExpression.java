package my.gov.dosm.aesictec.spel;

import my.gov.dosm.aesictec.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class SpringExpression {

  @Test
  public void test1() {
    ExpressionParser parser = new SpelExpressionParser();
    Expression exp = parser.parseExpression("new String('hello world').toUpperCase()");
    String message = exp.getValue(String.class);
    System.out.println(message);
  }

  @Test
  public void testOperator() {
    ExpressionParser parser = new SpelExpressionParser();
    System.out.println(parser.parseExpression("1 gt 2").getValue(Boolean.class));
  }

  @Test
  public void testAssignment() {
    Inventor inventor = new Inventor();
    StandardEvaluationContext inventorContext = new StandardEvaluationContext(inventor);

    ExpressionParser parser = new SpelExpressionParser();
    parser.parseExpression("name").setValue(inventorContext, "Jabatan Statistic");

    System.out.println(inventor.getName());
  }

  @Test
  public void testConstructor() {

    ExpressionParser parser = new SpelExpressionParser();
    Inventor inventor = parser.parseExpression("new  my.gov.dosm.aesictec.spel.Inventor('DOSM')").getValue(Inventor.class);

    System.out.println(inventor.getName());
  }

  @Test
  public void testVariable() {

    Inventor inventor = new Inventor("Jabatan Statistic");
    System.out.println(inventor.getName());


    StandardEvaluationContext context = new StandardEvaluationContext(inventor);
    context.setVariable("newName", "DOSM");

    ExpressionParser parser = new SpelExpressionParser();
    parser.parseExpression("Name = #newName").getValue(context);

    System.out.println(inventor.getName());
  }

}
