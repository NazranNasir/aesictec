package my.gov.dosm.aesictec.api;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import my.gov.dosm.aesictec.TestUtil;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.Rollback;

import static io.restassured.RestAssured.given;

public class SimpleApiTest extends AbstractApiTest {

  private static final Logger LOG = LoggerFactory.getLogger(SimpleApiTest.class);

  private final String API_PREFIX = "/api/management";

  @Test
  @Rollback
  public void saveInterview() throws Exception {

    Response response = given()
      .header("Authorization", "Bearer " + getAccessToken())
      .body(TestUtil.convertToJsonBytes(null))
      .contentType(ContentType.JSON)
      .when()
      .post(API_PREFIX + "/interviews")
      .then()
      .statusCode(HttpStatus.OK.value())
      .extract()
      .response();
  }
}
