package my.gov.dosm.aesictec;

import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.business.statemachine.EditSpecStateHandler;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecEvent;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class AesEditSpecStateMachineTest {

  private static final Logger LOG = LoggerFactory.getLogger(AesEditSpecStateMachineTest.class);

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private EditSpecStateHandler stateHandler;

  @Autowired
  private ValidationService validationService;

  @Before
  public void before() {
  }

  @After
  public void after() {
  }

  @Test
  @Rollback
  public void run() {
    loadDataEditSpec();
    stateHandler.registerListener(validationService);
    List<AesEditSpec> editSpecs = validationService.findEditSpecs();
    LOG.debug("Total data : {}", editSpecs.size());
    for (AesEditSpec editSpec : editSpecs) {
      LOG.debug("Processing : {}", editSpec.getId());
      switch (editSpec.getState()) {
        case JP_IMPORTED:
          stateHandler.handleEvent(
            MessageBuilder
              .withPayload(AesEditSpecEvent.JP_VALIDATE)
              .setHeader("editSpecId", editSpec.getId())
              .build(), editSpec.getState());
          break;
        case JP_VALIDATED:
          stateHandler.handleEvent(
            MessageBuilder
              .withPayload(AesEditSpecEvent.JP_APPROVE)
              .setHeader("editSpecId", editSpec.getId())
              .build(), editSpec.getState());
          break;
        case JP_APPROVED:
          stateHandler.handleEvent(
            MessageBuilder
              .withPayload(AesEditSpecEvent.SMD_APPROVE)
              .setHeader("editSpecId", editSpec.getId())
              .build(), editSpec.getState());
          break;
        case SMD_APPROVED:
          stateHandler.handleEvent(
            MessageBuilder
              .withPayload(AesEditSpecEvent.SMD_SCHEDULE)
              .setHeader("editSpecId", editSpec.getId())
              .build(), editSpec.getState());
          break;
        case SMD_SCHEDULED:
          stateHandler.handleEvent(
            MessageBuilder
              .withPayload(AesEditSpecEvent.SMD_FINALIZE)
              .setHeader("editSpecId", editSpec.getId())
              .build(), editSpec.getState());
          break;
        case SMD_FINALIZED:
          break;
      }
    }
  }

  private void loadDataEditSpec() {
    AesEditSpec es1 = new AesEditSpecImpl();
    es1.setCaseNo("310");
    es1.setState(AesEditSpecState.JP_IMPORTED);
    validationService.saveEditSpec(es1);

    AesEditSpec es2 = new AesEditSpecImpl();
    es2.setCaseNo("310");
    es2.setState(AesEditSpecState.JP_APPROVED);
    validationService.saveEditSpec(es2);

    AesEditSpec es3 = new AesEditSpecImpl();
    es3.setCaseNo("310");
    es3.setState(AesEditSpecState.SMD_APPROVED);
    validationService.saveEditSpec(es3);
  }
}
