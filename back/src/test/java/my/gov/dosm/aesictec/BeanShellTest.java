package my.gov.dosm.aesictec;

import bsh.EvalError;
import bsh.Interpreter;
import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class BeanShellTest {

  private static final Logger LOG = LoggerFactory.getLogger(BeanShellTest.class);

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private ValidationService validationService;

  @Before
  public void before() {
  }

  @After
  public void after() {
    // cleanup
  }

  @Test
  public void test1() throws EvalError {
    Interpreter interpreter = new Interpreter();
    interpreter.set("one", 1);
    interpreter.set("thow", 2);
    System.out.println(interpreter.eval("2 + 2"));
    System.out.println("test3");

    Object result = interpreter.eval("2 + 2");
    System.out.println(result);

    SimpleBeanShellTest.main(new String[0]);
    interpreter.eval("my.gov.dosm.aesictec.SimpleBeanShellTest.main(new String[0])");

    String script = "print (5)";
    interpreter.eval(script);

    System.out.println(entityManager.createQuery("select count(p) from CngPrincipal p").getSingleResult());

    interpreter.set("entityManager", entityManager);
    script = "entityManager.createQuery(\"select count(p) from CngPrincipal p\").getSingleResult()";
    System.out.println(interpreter.eval(script));

    script = "javax.persistence.Query query = entityManager.createQuery(\"select count(p) from CngPrincipal p\");\n" +
      "int i = ((Long)query.getSingleResult()).intValue();" +
      "String result = i > 0 ? \"Ada\" : \"Kosong\"";
    interpreter.eval(script);
    System.out.println(interpreter.get("result"));
  }

  @Test
  public void test99() throws EvalError {
    Interpreter interpreter = new Interpreter();
    interpreter.set("first_name", "Naz");
    interpreter.set("last_name", "Nasir");
    String script = "String fullname = first_name + last_name";
    interpreter.eval(script);
    System.out.println(interpreter.get("fullname"));

//    String script = "first_name + last_name";
//    String fullname = (String)interpreter.eval(script);
//    System.out.println(fullname);

  }

  @Test
  public void test98() throws EvalError {
	  Interpreter interpreter = new Interpreter();
	  interpreter.set("F1", 4);
	  interpreter.set("F2", 2);
	  interpreter.set("result", false);
	  String script = "if((F1 == 1 || F1 == 2 || F1 == 3 || F1 == 9) && (F2 == 1 || F2 == 2)) {result = true;} else {result = false;}";
//	  String script = "if(1 == 1) result = true;";
	  interpreter.eval(script);
	  boolean results = (boolean)interpreter.get("result");
	  System.out.println(results);
  }

  @Test
  public void test4007() throws EvalError {
	  Interpreter interpreter = new Interpreter();
	  interpreter.set("F310003", 1);
	  interpreter.set("F310004", 99);
	  String script = "Boolean result = (F310003 == 1) && (F310004 >= 1 && F310004 <= 100)";
	  interpreter.eval(script);
	  System.out.println(interpreter.get("result"));
  }

  @Test
  public void test2() throws EvalError {
    Interpreter interpreter = new Interpreter();
    interpreter.set("one", 1);
    interpreter.set("two", 2);

    System.out.println("Rasa sayang hey Rasa sayang sayang hey");
    System.out.println(interpreter.eval("10/2"));
    System.out.println(interpreter.eval("two + two"));

    String script = "print (110)";
    interpreter.eval(script);
  }

  @Test
  public void test3() throws EvalError {
    Interpreter interpreter = new Interpreter();
    interpreter.set("F030011", 0.1);
    interpreter.set("F020013", 0.3);
    String script = "( F030011 > 0.5 ) && ( F030013 > 0 )";

  }

  @Test
  public void testBeanshell() {
    List<AesEditSpecSchema> editSpecSchemas = validationService.findEditSpecSchemas();
    for (AesEditSpecSchema editSpecSchema : editSpecSchemas) {
      List<AesEditSpecSchemaItem> editSpecSchemaItems = validationService.findEditSpecSchemaItem(editSpecSchema);
      for (AesEditSpecSchemaItem editSpecSchemaItem : editSpecSchemaItems) {
        LOG.debug(editSpecSchemaItem.getRules());
      }
    }
  }

  @Test
  public void testValidate() {
    List<AesEditSpec> editSpecs = validationService.findEditSpecs();
    for (AesEditSpec editSpec : editSpecs) {
      validationService.runRules(editSpec);
    }
  }
}
