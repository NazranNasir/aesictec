package my.gov.dosm.aesictec.identity.dao;

import my.gov.dosm.aesictec.Application;
import my.gov.dosm.aesictec.identity.domain.dao.CngUserDao;
import my.gov.dosm.aesictec.identity.domain.model.CngUser;
import my.gov.dosm.aesictec.identity.domain.model.CngUserImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class CngUserDaoTest {

  private static final Logger LOG = LoggerFactory.getLogger(CngUserDaoTest.class);

  @Autowired
  private CngUserDao userDao;

  @Autowired
  private EntityManager entityManager;

  @Autowired
  AuthenticationManager authenticationManager;

  @Before
  public void before() {
  }

  @After
  public void after() {
  }

  @Test
  public void testLogin() {
    UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("johari", "password");
    Authentication authed = authenticationManager.authenticate(token);
    SecurityContextHolder.getContext().setAuthentication(authed);

  }

  @Test
  public void testCreaateUsers() {
    List<CngUser> users = userDao.find();
    int totalUserBefore = users.size();
    CngUser user = new CngUserImpl();
    
    user.setEmail("xxx");
    user.setName("xxx");
    user.setRealName("xxx");
    user.setPassword("xxx");
    entityManager.merge(user);

    users = userDao.find();
    int totalUserAfter = users.size();
    Assert.assertEquals(totalUserBefore + 1, totalUserAfter);
    LOG.debug("{} {} ", totalUserBefore, totalUserAfter);
  }

}
