package my.gov.dosm.aesictec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.persistence.EntityManager;

import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecItemImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchema;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItem;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecSchemaItemImpl;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpecState;


/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class AesEditSpecSchemaReaderTest {

  private static final Logger LOG = LoggerFactory.getLogger(AesEditSpecSchemaReaderTest.class);

  @Autowired
  private EntityManager entityManager;

  @Autowired
  private ValidationService validationService;

  @Before
  public void before() {
  }

  @After
  public void after() {
    // cleanup
  }

  @Test
  @Rollback(false)
  public void schemaReader310() {
    try {
      String caseNo = "310";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      AesEditSpecSchema specSchema = new AesEditSpecSchemaImpl();
      specSchema.setCaseNo("310");
      validationService.saveEditSpecSchema(specSchema);
      entityManager.refresh(specSchema);

      for (int i = 0; i < schemas.length; i++) {
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);

        AesEditSpecSchemaItem schemaItem = new AesEditSpecSchemaItemImpl();
        schemaItem.setQuestion(schemas[i]);
        schemaItem.setRules("");
        validationService.addEditSpecSchemaItem(specSchema, schemaItem);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  @Rollback(false)
  public void surveyReader310() {
    try {
      String caseNo = "310";
      String surveyFile = "./../docs/FieldName_KP" + caseNo + ".txt";
      String appended = trimLine(surveyFile);

      AesEditSpec editSpec = new AesEditSpecImpl();
      editSpec.setState(AesEditSpecState.JP_IMPORTED);
      editSpec.setCaseNo("310");
      editSpec.setEditSpecSchema(validationService.findEditSpecSchemaByCaseNo("310"));
      validationService.saveEditSpec(editSpec);
      entityManager.flush();

      entityManager.refresh(editSpec);

      // split into schema and response
      String[] schemaAndResponse = split(caseNo, appended);
      String schema = schemaAndResponse[0];
      String response = schemaAndResponse[1];

      String[] schemas = schema.split("\\|");
      String[] responses = response.split("\\|");

      for (int i = 0; i < schemas.length; i++) {
        if (null == schemas[i] || null == responses[i]) continue;
        if (schemas[i].isEmpty() || responses[i].isEmpty()) continue;
        LOG.debug(schemas[i]);
        LOG.debug(responses[i]);
        AesEditSpecItem item = new AesEditSpecItemImpl();
        item.setQuestion(schemas[i]);
        item.setResponse(responses[i]);
        item.setError("");
        validationService.addEditSpecItem(editSpec, item);
      }
      entityManager.flush();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  private String trimLine(String file) throws IOException {
    String line;
    StringBuffer append = new StringBuffer();

    BufferedReader bufferreader = new BufferedReader(new FileReader(file));
    while ((line = bufferreader.readLine()) != null) {
      append.append(line.trim());
    }
    return append.toString();
  }

  private String[] split(String caseNo, String appended) {
    String[] split = appended.split("SURVEY" + caseNo);
    String schema = null;
    String raw = split[0];
    String response = split[1];
    schema = raw.substring("SURVEY|#BatchNo|BATCHCODE|FID|DATETIME".length(), raw.length());
    return new String[]{schema, response};
  }
}
