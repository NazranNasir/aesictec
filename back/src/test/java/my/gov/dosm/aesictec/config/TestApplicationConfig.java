package my.gov.dosm.aesictec.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
@EnableScheduling
@EnableCaching
@ComponentScan(basePackages = {
        // internals
        "my.gov.dosm.aesictec.identity",
        "my.gov.dosm.aesictec.security",
        "my.gov.dosm.aesictec.system",
        "my.gov.dosm.aesictec.common",
        "my.gov.dosm.aesictec.core",

        // modules
        "my.gov.dosm.aesictec.helper",
})
@EntityScan(basePackages = {
        "my.gov.dosm.aesictec"
})

@Import({
        SecurityConfig.class,
        SwaggerConfig.class,
        AuthorizationServerConfig.class,
        ResourceServerConfig.class,
        CorsConfig.class,
        CacheConfig.class,
        WebConfig.class
})
@PropertySource("classpath:application.properties")
@EnableAutoConfiguration
public class TestApplicationConfig {


}
