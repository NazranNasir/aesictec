package my.gov.dosm.aesictec.spel;

public class Inventor {
  private String name;

  public Inventor(String name) {
    this.name = name;
  }

  public Inventor() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
