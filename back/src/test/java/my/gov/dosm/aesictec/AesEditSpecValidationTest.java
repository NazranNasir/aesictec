package my.gov.dosm.aesictec;

import com.deliveredtechnologies.rulebook.model.RuleBook;
import my.gov.dosm.aesictec.validation.business.rules.ValidationResult;
import my.gov.dosm.aesictec.validation.business.service.ValidationService;
import my.gov.dosm.aesictec.validation.domain.model.AesEditSpec;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author canang technologies
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@Transactional
public class AesEditSpecValidationTest {

  private static final Logger LOG = LoggerFactory.getLogger(AesEditSpecValidationTest.class);

  @Autowired
  private ValidationService validationService;

  @Autowired
  @Qualifier(value = "case411RuleBook")
  private RuleBook ruleBook;

  @Autowired
  @Qualifier(value = "case310RuleBook")
  private RuleBook case310RuleBook;

  @Before
  public void before() {
  }

  @After
  public void after() {
  }

  @Test
  @Rollback(false)
  public void run() {
    AesEditSpec editSpec = validationService.findEditSpecById(6L);
    ValidationResult result = validationService.runRules(editSpec, ruleBook);
    LOG.debug("result = " + result);
  }
}
